package com.imovil.casta.tiempodejuegofinal.adaptadores;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.imovil.casta.tiempodejuegofinal.modelo.Equipo;
import com.imovil.casta.tiempodejuegofinal.R;

import java.util.List;

/**
 * Created by casta on 23/3/17.
 */

public class AdaptadorEquipos extends BaseAdapter {


    static class ViewHolder {
        public TextView nombreEquipo;
        public TextView categorias;
        public ImageView escudoEquipo;
    }

    private final List<Equipo> listaEquipos;
    public LayoutInflater inflater;

    //Constructor del adatador
    public AdaptadorEquipos(Context context, List<Equipo> equipos) {

        if (context == null) {
            throw new IllegalArgumentException();
        }

        this.listaEquipos = equipos;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listaEquipos.size();
    }

    @Override
    public Object getItem(int position) {
        return listaEquipos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        ViewHolder viewHolder;
        if (rowView == null) {

            rowView = inflater.inflate(R.layout.lista_equipos,parent,false);
            viewHolder = new ViewHolder();
            viewHolder.nombreEquipo = (TextView) rowView.findViewById(R.id.tvEquipo);
            viewHolder.categorias = (TextView) rowView.findViewById(R.id.tvCategoria);
            viewHolder.escudoEquipo = (ImageView) rowView.findViewById(R.id.ivEscudo);
            rowView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) rowView.getTag();
        }

        Equipo equipo = (Equipo) getItem(position);
        viewHolder.nombreEquipo.setText(equipo.getNombre());
        viewHolder.categorias.setText(equipo.getCategoria());
        viewHolder.escudoEquipo.setImageBitmap(Bitmap.createScaledBitmap(equipo.getEscudo(),120,120,false));

        return rowView;

    }
}

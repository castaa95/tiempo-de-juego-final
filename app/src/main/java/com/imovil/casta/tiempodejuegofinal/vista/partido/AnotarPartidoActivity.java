package com.imovil.casta.tiempodejuegofinal.vista.partido;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.imovil.casta.tiempodejuegofinal.MainActivity;
import com.imovil.casta.tiempodejuegofinal.R;
import com.imovil.casta.tiempodejuegofinal.adaptadores.AdaptadorConvocatoria;
import com.imovil.casta.tiempodejuegofinal.adaptadores.AdaptadorDatosPartido;
import com.imovil.casta.tiempodejuegofinal.basedatos.DBAdapter;
import com.imovil.casta.tiempodejuegofinal.modelo.Jugador;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AnotarPartidoActivity extends AppCompatActivity {

    private RecyclerView listaJugadores;

    private List<Integer> posiciones = new ArrayList<Integer>();
    private String rivalP;
    private String campoP;
    private String fechaP;
    private DBAdapter dbAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anotar_partido);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getResources().getString(R.string.nodirecto));

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        listaJugadores = (RecyclerView) findViewById(R.id.lvPlantilla);


        //Cogemos valores de la actividad anterior
        Intent intt = getIntent();
        rivalP = intt.getStringExtra("rival");
        campoP = intt.getStringExtra("campo");
        fechaP = intt.getStringExtra("fecha");

        //Sacamos lista de jugadores
        final List<Jugador> jugadores = new ArrayList<Jugador>();
        dbAdapter = new DBAdapter(this);
        dbAdapter.open();
        Cursor c = null;
        c  = dbAdapter.getJugadorLista(MainActivity.id_equipo);
        if (c.moveToFirst()) {
            //Recorremos el cursor hasta que no haya más registros
            do {
                byte[] foto = c.getBlob(9);
                Bitmap bm = BitmapFactory.decodeByteArray(foto,0,foto.length);
                Jugador jug = new Jugador(c.getInt(0),c.getInt(1),c.getString(2),c.getString(3),c.getString(4),
                        c.getString(5),c.getInt(6),c.getInt(7),c.getInt(8),bm,c.getInt(10),c.getInt(11),c.getInt(12),c.getInt(13),c.getInt(14));
                jugadores.add(jug);
            } while (c.moveToNext());
        }


        AdaptadorDatosPartido adapter = new AdaptadorDatosPartido(this,jugadores);
        listaJugadores.setAdapter(adapter);
        adapter.restoreSelectedItems(posiciones);
        listaJugadores.setLayoutManager(new LinearLayoutManager(this));

        FloatingActionButton fab = (FloatingActionButton)findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < jugadores.size();i++){

                    //Pasamos los datos del list view
                    if (AdaptadorDatosPartido.mConvocado.get(i) == true){

                        jugadores.get(i).setTemp_tiempoC(Long.valueOf(AdaptadorDatosPartido.mEditTextJugado.get(i)));
                        jugadores.get(i).setTemp_tiempoB(Long.valueOf(AdaptadorDatosPartido.mEditTextBanquillo.get(i)));
                        jugadores.get(i).setTemp_golesPartido(Integer.valueOf(AdaptadorDatosPartido.mEditTextGoles.get(i)));

                        if(AdaptadorDatosPartido.mRoja.get(i) == true){
                            jugadores.get(i).setTemp_rojasPartido(1);
                        }

                        if(AdaptadorDatosPartido.m0.get(i) == true){
                            jugadores.get(i).setTemp_amarillasPartido(0);
                        }

                        if(AdaptadorDatosPartido.m1.get(i) == true){
                            jugadores.get(i).setTemp_amarillasPartido(1);
                        }

                        if(AdaptadorDatosPartido.m2.get(i) == true){
                            jugadores.get(i).setTemp_amarillasPartido(2);
                        }

                        jugadores.get(i).setPartidos_jugados(jugadores.get(i).getPartidos_jugados() +1);

                    }

                    else {

                        jugadores.get(i).setTemp_tiempoC(0);
                        jugadores.get(i).setTemp_tiempoB(0);
                        jugadores.get(i).setPartidos_desconvocado(jugadores.get(i).getPartidos_desconvocado() +1);

                        switch (MainActivity.categoria){
                            case "Futbol 8": jugadores.get(i).setTemp_tiempoD(80); break;
                            case "Fútbol 11": jugadores.get(i).setTemp_tiempoD(90); break;
                            case "Pista":jugadores.get(i).setTemp_tiempoD(50); break;
                        }



                    }

                }
                new CrearPartidoTask(jugadores).execute();
            }
        });



    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    public void onBackPressed(){
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(getResources().getString(R.string.tituloSalir))
                .setMessage(getResources().getString(R.string.mensajeSalir))
                .setPositiveButton(getResources().getString(R.string.si), new DialogInterface.OnClickListener()
                {

                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }

    private class CrearPartidoTask extends AsyncTask<Void,Void,Boolean>{

        //Cogemos las listas
        List<Jugador> jugadores;

        public CrearPartidoTask(List<Jugador>jugadores){

            this.jugadores = jugadores;

        }

        @Override
        public Boolean doInBackground(Void... voids) {


            //Cogemos lista de partidos de ese equipo contando los que hay
            int contador = 1;
            Cursor c = null;
            c = dbAdapter.getPartidosLista(MainActivity.id_equipo);
            if (c.moveToFirst()) {
                //Recorremos el cursor hasta que no haya más registros
                do {
                    contador++;
                } while (c.moveToNext());
            }


            //Creamos uno nuevo añadiendo un valor mas al contador
            dbAdapter.insertarPartido(MainActivity.id_equipo,contador,fechaP,rivalP,campoP);

            //Cogemos el id de ese nuevo partido
            c = dbAdapter.getPartido(MainActivity.id_equipo,contador);
            int id = 0;
            if (c.moveToFirst()) {
                //Recorremos el cursor hasta que no haya más registros
                do {
                    id = c.getInt(0);
                } while (c.moveToNext());
            }

            //Para cada jugador actualizamos sus datos
            for (int i = 0; i<jugadores.size(); i++){

                dbAdapter.insertarJugadorPartido(jugadores.get(i).getId(),id,jugadores.get(i).getMote(),jugadores.get(i).getTemp_golesPartido(),jugadores.get(i).getTemp_amarillasPartido(),
                        jugadores.get(i).getTemp_rojasPartido(),AdaptadorDatosPartido.mConvocado.get(i),jugadores.get(i).getTemp_tiempoC(),jugadores.get(i).getTemp_tiempoB(),jugadores.get(i).getTemp_tiempoD());

                dbAdapter.actualizarJugador(jugadores.get(i).getId(),jugadores.get(i).getPartidos_jugados(),jugadores.get(i).getPartidos_desconvocado(),jugadores.get(i).getGoles() + jugadores.get(i).getTemp_golesPartido(),
                        jugadores.get(i).getAmarillas() + jugadores.get(i).getTemp_amarillasPartido(),jugadores.get(i).getRojas() + jugadores.get(i).getTemp_rojasPartido(), jugadores.get(i).getMinutos_jugados_totales() + jugadores.get(i).getTemp_tiempoC(),
                        jugadores.get(i).getMinutos_banquillo_totales() + jugadores.get(i).getTemp_tiempoB(),jugadores.get(i).getMinutos_desconvocado_totales() + jugadores.get(i).getTemp_tiempoD());

            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean bool) {
            setResult(Activity.RESULT_OK);
            finish();
            /*Intent intent = new Intent(AnotarPartidoActivity.this,MainActivity.class);
            intent.putExtra("id",MainActivity.id_equipo);
            intent.putExtra("nombre",MainActivity.nombre);
            intent.putExtra("escudo",MainActivity.escudo);
            intent.putExtra("categoria",MainActivity.categoria);
            startActivity(intent);*/
        }

    }
}

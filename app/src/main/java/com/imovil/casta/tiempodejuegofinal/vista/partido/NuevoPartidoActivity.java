package com.imovil.casta.tiempodejuegofinal.vista.partido;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.view.DragEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.imovil.casta.tiempodejuegofinal.MainActivity;
import com.imovil.casta.tiempodejuegofinal.R;
import com.imovil.casta.tiempodejuegofinal.adaptadores.AdaptadorBanco;
import com.imovil.casta.tiempodejuegofinal.adaptadores.AdaptadorConvocatoria;
import com.imovil.casta.tiempodejuegofinal.adaptadores.AdaptadorCampo;
import com.imovil.casta.tiempodejuegofinal.adaptadores.AdaptadorDefensas;
import com.imovil.casta.tiempodejuegofinal.adaptadores.AdaptadorDelanteros;
import com.imovil.casta.tiempodejuegofinal.adaptadores.AdaptadorMedios;
import com.imovil.casta.tiempodejuegofinal.basedatos.DBAdapter;
import com.imovil.casta.tiempodejuegofinal.modelo.Jugador;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*
Actividad principal, nuevo partido, creamos convocatoria, titulares y banquillo
Empezamos el partido, podemos poner goles y hacer cambios
Al final del partido se nos guardan los detalles y las estadisticas
 */

public class NuevoPartidoActivity extends AppCompatActivity {

    DBAdapter dbAdapter;

    private GridView banquillo;
    private RecyclerView portero;
    private RecyclerView delanteros;
    private RecyclerView centrocampistas;
    private RecyclerView defensas;
    private RecyclerView suplentes;
    public static Chronometer tiempo;
    private Button empezar;
    private Button pausa;
    private Button guardar;
    long tiempoAlPausar = 0;
    public static int jugadores;
    private int jugBanquillo;

    public static List<Jugador> expulsados;

    private long tiempoPartido=0;
    private long tiempoC =0;
    private long tiempoB =0;

    static String titulo = "titulo";
    private String rival;
    private String fecha;
    private String campo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_partido);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getResources().getString(R.string.directo));

        //Preparamos layout
        banquillo = (GridView) findViewById(R.id.vBanquillo);
        delanteros = (RecyclerView) findViewById(R.id.vDelanteros);
        centrocampistas = (RecyclerView) findViewById(R.id.vCentrocampistas);
        defensas = (RecyclerView) findViewById(R.id.vDefensas);
        portero = (RecyclerView) findViewById(R.id.vPortero);
        tiempo = (Chronometer) findViewById(R.id.chronometer2);
        empezar = (Button) findViewById(R.id.bEmpezar);
        pausa = (Button) findViewById(R.id.bParar);
        guardar = (Button) findViewById(R.id.bSave);
        suplentes = (RecyclerView) findViewById(R.id.vSuplentes);
        LinearLayout layoutCampo = (LinearLayout)findViewById(R.id.layoutCampo);



        //Cambiamos de campo segun la categoria
        switch (MainActivity.categoria){
            case "Futbol 8": jugadores = 8; jugBanquillo = 6; tiempoPartido=80; break;
            case "Fútbol 11": jugadores = 11; jugBanquillo = 7; tiempoPartido=90; break;
            case "Pista": jugadores = 5; jugBanquillo = 5; tiempoPartido=25; layoutCampo.setBackgroundResource(R.drawable.pista);break;
        }

        expulsados = new ArrayList<Jugador>();

        Intent intt = getIntent();
        rival = intt.getStringExtra("rival");
        campo = intt.getStringExtra("campo");
        fecha = intt.getStringExtra("fecha");

        //Ponemos las listas de forma horizontal
        final List<Jugador> listaDelanteros = new ArrayList<Jugador>();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        delanteros.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(delanteros.getContext(),
                layoutManager.getOrientation());
        delanteros.addItemDecoration(dividerItemDecoration);
        final AdaptadorDelanteros adapterDelanteros = new AdaptadorDelanteros(this,listaDelanteros);
        delanteros.setAdapter(adapterDelanteros);

        final List<Jugador> listaCentrocampistas = new ArrayList<Jugador>();
        LinearLayoutManager layoutManager2 = new LinearLayoutManager(this);
        layoutManager2.setOrientation(LinearLayoutManager.HORIZONTAL);
        centrocampistas.setLayoutManager(layoutManager2);
        DividerItemDecoration dividerItemDecoration2 = new DividerItemDecoration(centrocampistas.getContext(),
                layoutManager.getOrientation());
        centrocampistas.addItemDecoration(dividerItemDecoration2);
        final AdaptadorMedios adapterCentrocampistas = new AdaptadorMedios(this,listaCentrocampistas);
        centrocampistas.setAdapter(adapterCentrocampistas);

        final List<Jugador> listaDefensas = new ArrayList<Jugador>();
        LinearLayoutManager layoutManager3 = new LinearLayoutManager(this);
        layoutManager3.setOrientation(LinearLayoutManager.HORIZONTAL);
        defensas.setLayoutManager(layoutManager3);
        DividerItemDecoration dividerItemDecoration3 = new DividerItemDecoration(defensas.getContext(),
                layoutManager.getOrientation());
        defensas.addItemDecoration(dividerItemDecoration3);
        final AdaptadorDefensas adapterDefensas = new AdaptadorDefensas(this,listaDefensas);
        defensas.setAdapter(adapterDefensas);

        final List<Jugador> listaPortero = new ArrayList<Jugador>();
        LinearLayoutManager layoutManager4 = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL, false);
        portero.setLayoutManager(layoutManager4);
        DividerItemDecoration dividerItemDecoration4 = new DividerItemDecoration(portero.getContext(),
                layoutManager.getOrientation());
        portero.addItemDecoration(dividerItemDecoration4);
        final AdaptadorCampo adapterPortero = new AdaptadorCampo(this,listaPortero);
        portero.setAdapter(adapterPortero);

        final List<Jugador> listaSuplentes = new ArrayList<Jugador>();
        LinearLayoutManager layoutManager5 = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL, false);
        suplentes.setLayoutManager(layoutManager5);
        DividerItemDecoration dividerItemDecoration5 = new DividerItemDecoration(suplentes.getContext(),
                layoutManager.getOrientation());
        suplentes.addItemDecoration(dividerItemDecoration5);
        final AdaptadorBanco adapterSuplentes = new AdaptadorBanco(this,listaSuplentes);
        suplentes.setAdapter(adapterSuplentes);

        final List<Jugador> convocatoria = new ArrayList<Jugador>();

        //Sacamos lista de jugadores
        dbAdapter = new DBAdapter(this);
        dbAdapter.open();
        Cursor c = null;
        c  = dbAdapter.getJugadorLista(MainActivity.id_equipo);
        if (c.moveToFirst()) {
            //Recorremos el cursor hasta que no haya más registros
            do {
                byte[] foto = c.getBlob(9);
                Bitmap bm = BitmapFactory.decodeByteArray(foto,0,foto.length);
                Jugador jug = new Jugador(c.getInt(0),c.getInt(1),c.getString(2),c.getString(3),c.getString(4),
                        c.getString(5),c.getInt(6),c.getInt(7),c.getInt(8),bm,c.getInt(10),c.getInt(11),c.getInt(12),c.getInt(13),c.getInt(14));
                convocatoria.add(jug);
            } while (c.moveToNext());
        }



        //Eventos de los botones
        empezar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pausa.setEnabled(true);
                tiempo.setBase(SystemClock.elapsedRealtime() + tiempoAlPausar);
                tiempo.start();
                empezar.setEnabled(false);
                empezar.setText(R.string.comenzar2);
                empezar.setVisibility(View.INVISIBLE);
                pausa.setVisibility(View.VISIBLE);
                banquillo.setEnabled(false);
                banquillo.setVisibility(View.INVISIBLE);
            }
        });

        pausa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Al finalizar partido
                if (pausa.getText().equals(getString(R.string.finalizar2))){
                    tiempoAlPausar = tiempo.getBase() - SystemClock.elapsedRealtime();
                    tiempo.stop();
                    pausa.setEnabled(false);
                    pausa.setBackgroundColor(getResources().getColor(R.color.banquillo));
                    guardar.setEnabled(true);
                    guardar.setVisibility(View.VISIBLE);
                    //Actualizamos tiempos de los jugadores
                    for (int i = 0; i< listaDelanteros.size();i++){
                        listaDelanteros.get(i).setTemp_tiempoC(((SystemClock.elapsedRealtime() - tiempo.getBase())/1000/60) - listaDelanteros.get(i).getTemp_tiempoB());
                        listaDelanteros.get(i).setTemp_tiempoB(((SystemClock.elapsedRealtime() - tiempo.getBase())/1000/60) - listaDelanteros.get(i).getTemp_tiempoC());
                    }

                    for (int i = 0; i< listaCentrocampistas.size();i++){
                        listaCentrocampistas.get(i).setTemp_tiempoC(((SystemClock.elapsedRealtime() - tiempo.getBase())/1000/60) - listaCentrocampistas.get(i).getTemp_tiempoB());
                        listaCentrocampistas.get(i).setTemp_tiempoB(((SystemClock.elapsedRealtime() - tiempo.getBase())/1000/60) - listaCentrocampistas.get(i).getTemp_tiempoC());
                    }

                    for (int i = 0; i< listaDefensas.size();i++){
                        listaDefensas.get(i).setTemp_tiempoC(((SystemClock.elapsedRealtime() - tiempo.getBase())/1000/60) - listaDefensas.get(i).getTemp_tiempoB());
                        listaDefensas.get(i).setTemp_tiempoB(((SystemClock.elapsedRealtime() - tiempo.getBase())/1000/60) - listaDefensas.get(i).getTemp_tiempoC());
                    }

                    for (int i = 0; i< listaPortero.size();i++){
                        listaPortero.get(i).setTemp_tiempoC(((SystemClock.elapsedRealtime() - tiempo.getBase())/1000/60) - listaPortero.get(i).getTemp_tiempoB());
                        listaPortero.get(i).setTemp_tiempoB(((SystemClock.elapsedRealtime() - tiempo.getBase())/1000/60) - listaPortero.get(i).getTemp_tiempoC());
                    }

                    for (int i = 0; i< listaSuplentes.size();i++){
                        listaSuplentes.get(i).setTemp_tiempoB(((SystemClock.elapsedRealtime() - tiempo.getBase())/1000/60) - listaSuplentes.get(i).getTemp_tiempoC());
                        listaSuplentes.get(i).setTemp_tiempoC(((SystemClock.elapsedRealtime() - tiempo.getBase())/1000/60) - listaSuplentes.get(i).getTemp_tiempoB());
                    }

                    for(int i =0; i< convocatoria.size();i++){
                        convocatoria.get(i).setTemp_tiempoD(((SystemClock.elapsedRealtime() - tiempo.getBase())/1000/60));
                    }

                }

                //Al descanso
                else{
                    empezar.setEnabled(true);
                    empezar.setVisibility(View.VISIBLE);
                    tiempoAlPausar = tiempo.getBase() - SystemClock.elapsedRealtime();
                    tiempo.stop();
                    pausa.setEnabled(false);
                    pausa.setVisibility(View.INVISIBLE);
                    pausa.setText(R.string.finalizar2);
                }

            }
        });

        //Evento de guardar
        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creamos el partido
                new ConsultasTask(convocatoria, listaSuplentes, listaDelanteros, listaCentrocampistas, listaDefensas, listaPortero,expulsados).execute();
            }
        });


        final AdaptadorConvocatoria adapter = new AdaptadorConvocatoria(this,convocatoria);
        banquillo.setAdapter(adapter);

        //Arrastre a porteria
        portero.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                int action = event.getAction();
                switch (event.getAction()) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        // do nothing
                        break;
                    case DragEvent.ACTION_DRAG_ENTERED:
                        break;
                    case DragEvent.ACTION_DRAG_EXITED:
                        break;
                    case DragEvent.ACTION_DROP:

                        try {

                            View view = (View) event.getLocalState();
                            //Obtenemos los datos

                            String origen = event.getClipData().getItemAt(10).getText().toString();
                            //Solo se permite un determinaod numero de jugadores convocados
                            // else {
                                int id = Integer.parseInt(event.getClipData().getItemAt(0).getText().toString());
                                int equipo = Integer.parseInt(event.getClipData().getItemAt(1).getText().toString());
                                String nombre = event.getClipData().getItemAt(2).getText().toString();
                                String apellidos = event.getClipData().getItemAt(3).getText().toString();
                                String mote = event.getClipData().getItemAt(4).getText().toString();
                                String puesto = event.getClipData().getItemAt(5).getText().toString();
                                int jugados = Integer.parseInt(event.getClipData().getItemAt(6).getText().toString());
                                int desconvocado = Integer.parseInt(event.getClipData().getItemAt(7).getText().toString());
                                int minutos = Integer.parseInt(event.getClipData().getItemAt(8).getText().toString());
                                int minbanquillo = Integer.parseInt(event.getClipData().getItemAt(9).getText().toString());
                                int goles = Integer.parseInt(event.getClipData().getItemAt(12).getText().toString());

                                Uri uriFoto = event.getClipData().getItemAt(13).getUri();
                                Bitmap foto = MediaStore.Images.Media.getBitmap(getContentResolver(), uriFoto);

                                String origen2 = event.getClipData().getItemAt(14).getText().toString();
                            int golesPartido = Integer.parseInt(event.getClipData().getItemAt(15).getText().toString());
                            int mindesconvocado = Integer.parseInt(event.getClipData().getItemAt(16).getText().toString());
                            int amarillas = Integer.parseInt(event.getClipData().getItemAt(17).getText().toString());
                            int rojas = Integer.parseInt(event.getClipData().getItemAt(18).getText().toString());
                            int amarillasPartido = Integer.parseInt(event.getClipData().getItemAt(19).getText().toString());
                            int rojasPartido = Integer.parseInt(event.getClipData().getItemAt(20).getText().toString());
                                Jugador portero = new Jugador(id, equipo, nombre, apellidos, mote, puesto, goles,amarillas,rojas, foto, jugados, desconvocado, minutos, minbanquillo, mindesconvocado);
                            portero.setTemp_golesPartido(golesPartido);
                            portero.setTemp_amarillasPartido(amarillasPartido);
                            portero.setTemp_rojasPartido(rojasPartido);

                                //Segun de donde venga el jugador se calcula el tiempo de determinada forma

                                if (origen.equals("campo")) {
                                    portero.setTemp_tiempoC(((SystemClock.elapsedRealtime() - tiempo.getBase()) / 1000 / 60) - portero.getTemp_tiempoB());
                                    int posicion = Integer.parseInt(event.getClipData().getItemAt(11).getText().toString());

                                    if(origen2.equals("delantero")){
                                        listaDelanteros.remove(posicion);
                                        adapterDelanteros.notifyDataSetChanged();
                                    }
                                    else if (origen2.equals("medio")){
                                        listaCentrocampistas.remove(posicion);
                                        adapterCentrocampistas.notifyDataSetChanged();
                                    }
                                    else if(origen2.equals("defensa")){
                                        listaDefensas.remove(posicion);
                                        adapterDefensas.notifyDataSetChanged();
                                    }
                                    else if(origen2.equals("portero")){
                                        listaPortero.remove(posicion);
                                        adapterPortero.notifyDataSetChanged();
                                    }

                                }

                            int contador = cuentaJugadoresCampo(listaDelanteros,listaCentrocampistas,listaDefensas,listaPortero);
                                if (contador >= jugadores) {
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.maxjugadores), Toast.LENGTH_SHORT).show();
                                    break;
                                }


                            if (origen.equals("convocatoria")) {
                                int posicion = Integer.parseInt(event.getClipData().getItemAt(11).getText().toString());
                                convocatoria.remove(posicion);
                                adapter.notifyDataSetChanged();
                            }

                            if (origen.equals("banco")) {
                                portero.setTemp_tiempoB(((SystemClock.elapsedRealtime() - tiempo.getBase()) / 1000 / 60) - portero.getTemp_tiempoC());
                                int posicion = Integer.parseInt(event.getClipData().getItemAt(11).getText().toString());
                                listaSuplentes.remove(posicion);
                                adapterSuplentes.notifyDataSetChanged();
                            }
                                listaPortero.add(portero);
                                adapterPortero.notifyDataSetChanged();
                                view.setVisibility(View.VISIBLE);
                                break;



                        }catch (IOException ex){
                            break;
                        }
                    case DragEvent.ACTION_DRAG_ENDED:
                    default:
                        break;
                }
                return true;
            }
        });

        //Arrastre a defensas
        defensas.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                int action = event.getAction();
                switch (event.getAction()) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        // do nothing
                        break;
                    case DragEvent.ACTION_DRAG_ENTERED:
                        break;
                    case DragEvent.ACTION_DRAG_EXITED:
                        break;
                    case DragEvent.ACTION_DROP:
                        View view = (View) event.getLocalState();
                        //Obtenemos los datos

                        String origen = event.getClipData().getItemAt(10).getText().toString();
                        //else {
                            try {
                                int id = Integer.parseInt(event.getClipData().getItemAt(0).getText().toString());
                                int equipo = Integer.parseInt(event.getClipData().getItemAt(1).getText().toString());
                                String nombre = event.getClipData().getItemAt(2).getText().toString();
                                String apellidos = event.getClipData().getItemAt(3).getText().toString();
                                String mote = event.getClipData().getItemAt(4).getText().toString();
                                String puesto = event.getClipData().getItemAt(5).getText().toString();
                                int jugados = Integer.parseInt(event.getClipData().getItemAt(6).getText().toString());
                                int desconvocado = Integer.parseInt(event.getClipData().getItemAt(7).getText().toString());
                                int minutos = Integer.parseInt(event.getClipData().getItemAt(8).getText().toString());
                                int minbanquillo = Integer.parseInt(event.getClipData().getItemAt(9).getText().toString());
                                int goles = Integer.parseInt(event.getClipData().getItemAt(12).getText().toString());

                                Uri uriFoto = event.getClipData().getItemAt(13).getUri();
                                Bitmap foto = MediaStore.Images.Media.getBitmap(getContentResolver(), uriFoto);

                                String origen2 = event.getClipData().getItemAt(14).getText().toString();
                                int golesPartido = Integer.parseInt(event.getClipData().getItemAt(15).getText().toString());
                                int mindesconvocado = Integer.parseInt(event.getClipData().getItemAt(16).getText().toString());
                                int amarillas = Integer.parseInt(event.getClipData().getItemAt(17).getText().toString());
                                int rojas = Integer.parseInt(event.getClipData().getItemAt(18).getText().toString());
                                int amarillasPartido = Integer.parseInt(event.getClipData().getItemAt(19).getText().toString());
                                int rojasPartido = Integer.parseInt(event.getClipData().getItemAt(20).getText().toString());
                                Jugador defensa = new Jugador(id, equipo, nombre, apellidos, mote, puesto, goles,amarillas,rojas, foto, jugados, desconvocado, minutos, minbanquillo,mindesconvocado);
                                defensa.setTemp_golesPartido(golesPartido);
                                defensa.setTemp_amarillasPartido(amarillasPartido);
                                defensa.setTemp_rojasPartido(rojasPartido);

                                if (origen.equals("campo")) {
                                    defensa.setTemp_tiempoC(((SystemClock.elapsedRealtime() - tiempo.getBase()) / 1000 / 60) - defensa.getTemp_tiempoB());
                                    int posicion = Integer.parseInt(event.getClipData().getItemAt(11).getText().toString());

                                    if(origen2.equals("delantero")){
                                        listaDelanteros.remove(posicion);
                                        adapterDelanteros.notifyDataSetChanged();
                                    }
                                    else if (origen2.equals("medio")){
                                        listaCentrocampistas.remove(posicion);
                                        adapterCentrocampistas.notifyDataSetChanged();
                                    }
                                    else if(origen2.equals("defensa")){
                                        listaDefensas.remove(posicion);
                                        adapterDefensas.notifyDataSetChanged();
                                    }
                                    else if(origen2.equals("portero")){
                                        listaPortero.remove(posicion);
                                        adapterPortero.notifyDataSetChanged();
                                    }
                                }

                                int contador = cuentaJugadoresCampo(listaDelanteros,listaCentrocampistas,listaDefensas,listaPortero);
                                if(contador >= jugadores){
                                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.maxjugadores),Toast.LENGTH_SHORT).show();
                                    break;
                                }

                                if (origen.equals("convocatoria")) {
                                    int posicion = Integer.parseInt(event.getClipData().getItemAt(11).getText().toString());
                                    convocatoria.remove(posicion);
                                    adapter.notifyDataSetChanged();
                                }

                                if (origen.equals("banco")) {
                                    defensa.setTemp_tiempoB(((SystemClock.elapsedRealtime() - tiempo.getBase()) / 1000 / 60) - defensa.getTemp_tiempoC());
                                    int posicion = Integer.parseInt(event.getClipData().getItemAt(11).getText().toString());
                                    listaSuplentes.remove(posicion);
                                    adapterSuplentes.notifyDataSetChanged();
                                }
                                listaDefensas.add(defensa);
                                adapterDefensas.notifyDataSetChanged();
                                view.setVisibility(View.VISIBLE);
                                break;
                            }catch (IOException ex){
                                break;
                            }
                       // }
                    case DragEvent.ACTION_DRAG_ENDED:
                    default:
                        break;
                }
                return true;
            }
        });

        //Arrastre a centrocampistas
        centrocampistas.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                int action = event.getAction();
                switch (event.getAction()) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        // do nothing
                        break;
                    case DragEvent.ACTION_DRAG_ENTERED:
                        break;
                    case DragEvent.ACTION_DRAG_EXITED:
                        break;
                    case DragEvent.ACTION_DROP:
                        View view = (View) event.getLocalState();
                        //Obtenemos los datos

                        String origen = event.getClipData().getItemAt(10).getText().toString();

//                        else {

                            try {

                                int id = Integer.parseInt(event.getClipData().getItemAt(0).getText().toString());
                                int equipo = Integer.parseInt(event.getClipData().getItemAt(1).getText().toString());
                                String nombre = event.getClipData().getItemAt(2).getText().toString();
                                String apellidos = event.getClipData().getItemAt(3).getText().toString();
                                String mote = event.getClipData().getItemAt(4).getText().toString();
                                String puesto = event.getClipData().getItemAt(5).getText().toString();
                                int jugados = Integer.parseInt(event.getClipData().getItemAt(6).getText().toString());
                                int desconvocado = Integer.parseInt(event.getClipData().getItemAt(7).getText().toString());
                                int minutos = Integer.parseInt(event.getClipData().getItemAt(8).getText().toString());
                                int minbanquillo = Integer.parseInt(event.getClipData().getItemAt(9).getText().toString());
                                int goles = Integer.parseInt(event.getClipData().getItemAt(12).getText().toString());

                                Uri uriFoto = event.getClipData().getItemAt(13).getUri();
                                Bitmap foto = MediaStore.Images.Media.getBitmap(getContentResolver(), uriFoto);

                                String origen2 = event.getClipData().getItemAt(14).getText().toString();
                                int golesPartido = Integer.parseInt(event.getClipData().getItemAt(15).getText().toString());
                                int mindesconvocado = Integer.parseInt(event.getClipData().getItemAt(16).getText().toString());
                                int amarillas = Integer.parseInt(event.getClipData().getItemAt(17).getText().toString());
                                int rojas = Integer.parseInt(event.getClipData().getItemAt(18).getText().toString());
                                int amarillasPartido = Integer.parseInt(event.getClipData().getItemAt(19).getText().toString());
                                int rojasPartido = Integer.parseInt(event.getClipData().getItemAt(20).getText().toString());
                                Jugador centrocampista = new Jugador(id, equipo, nombre, apellidos, mote, puesto, goles,amarillas,rojas, foto, jugados, desconvocado, minutos, minbanquillo,mindesconvocado);
                                centrocampista.setTemp_golesPartido(golesPartido);
                                centrocampista.setTemp_amarillasPartido(amarillasPartido);
                                centrocampista.setTemp_rojasPartido(rojasPartido);

                                if (origen.equals("campo")) {
                                    centrocampista.setTemp_tiempoC(((SystemClock.elapsedRealtime() - tiempo.getBase()) / 1000 / 60) - centrocampista.getTemp_tiempoB());
                                    int posicion = Integer.parseInt(event.getClipData().getItemAt(11).getText().toString());

                                    if(origen2.equals("delantero")){
                                        listaDelanteros.remove(posicion);
                                        adapterDelanteros.notifyDataSetChanged();
                                    }
                                    else if (origen2.equals("medio")){
                                        listaCentrocampistas.remove(posicion);
                                        adapterCentrocampistas.notifyDataSetChanged();
                                    }
                                    else if(origen2.equals("defensa")){
                                        listaDefensas.remove(posicion);
                                        adapterDefensas.notifyDataSetChanged();
                                    }
                                    else if(origen2.equals("portero")){
                                        listaPortero.remove(posicion);
                                        adapterPortero.notifyDataSetChanged();
                                    }
                                }

                                int contador = cuentaJugadoresCampo(listaDelanteros,listaCentrocampistas,listaDefensas,listaPortero);
                                if(contador >= jugadores){
                                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.maxjugadores),Toast.LENGTH_SHORT).show();
                                    break;
                                }

                                if (origen.equals("convocatoria")) {
                                    int posicion = Integer.parseInt(event.getClipData().getItemAt(11).getText().toString());
                                    convocatoria.remove(posicion);
                                    adapter.notifyDataSetChanged();
                                }

                                if (origen.equals("banco")) {
                                    centrocampista.setTemp_tiempoB(((SystemClock.elapsedRealtime() - tiempo.getBase()) / 1000 / 60) - centrocampista.getTemp_tiempoC());
                                    int posicion = Integer.parseInt(event.getClipData().getItemAt(11).getText().toString());
                                    listaSuplentes.remove(posicion);
                                    adapterSuplentes.notifyDataSetChanged();
                                }
                                listaCentrocampistas.add(centrocampista);
                                adapterCentrocampistas.notifyDataSetChanged();
                                view.setVisibility(View.VISIBLE);
                                break;

                            }catch (IOException ex){
                                break;
                            }

                    case DragEvent.ACTION_DRAG_ENDED:
                    default:
                        break;
                }
                return true;
            }
        });

        //Arrastre a delanteros
        delanteros.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                int action = event.getAction();
                switch (event.getAction()) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        // do nothing
                        break;
                    case DragEvent.ACTION_DRAG_ENTERED:
                        break;
                    case DragEvent.ACTION_DRAG_EXITED:
                        break;
                    case DragEvent.ACTION_DROP:
                        View view = (View) event.getLocalState();

                        String origen = event.getClipData().getItemAt(10).getText().toString();


                        //else {

                            try {
                                //Obtenemos los datos
                                int id = Integer.parseInt(event.getClipData().getItemAt(0).getText().toString());
                                int equipo = Integer.parseInt(event.getClipData().getItemAt(1).getText().toString());
                                String nombre = event.getClipData().getItemAt(2).getText().toString();
                                String apellidos = event.getClipData().getItemAt(3).getText().toString();
                                String mote = event.getClipData().getItemAt(4).getText().toString();
                                String puesto = event.getClipData().getItemAt(5).getText().toString();
                                int jugados = Integer.parseInt(event.getClipData().getItemAt(6).getText().toString());
                                int desconvocado = Integer.parseInt(event.getClipData().getItemAt(7).getText().toString());
                                int minutos = Integer.parseInt(event.getClipData().getItemAt(8).getText().toString());
                                int minbanquillo = Integer.parseInt(event.getClipData().getItemAt(9).getText().toString());
                                int goles = Integer.parseInt(event.getClipData().getItemAt(12).getText().toString());

                                Uri uriFoto = event.getClipData().getItemAt(13).getUri();
                                Bitmap foto = MediaStore.Images.Media.getBitmap(getContentResolver(), uriFoto);

                                String origen2 = event.getClipData().getItemAt(14).getText().toString();
                                int golesPartido = Integer.parseInt(event.getClipData().getItemAt(15).getText().toString());
                                int mindesconvocado = Integer.parseInt(event.getClipData().getItemAt(16).getText().toString());
                                int amarillas = Integer.parseInt(event.getClipData().getItemAt(17).getText().toString());
                                int rojas = Integer.parseInt(event.getClipData().getItemAt(18).getText().toString());
                                int amarillasPartido = Integer.parseInt(event.getClipData().getItemAt(19).getText().toString());
                                int rojasPartido = Integer.parseInt(event.getClipData().getItemAt(20).getText().toString());
                                Jugador delantero = new Jugador(id, equipo, nombre, apellidos, mote, puesto, goles,amarillas,rojas, foto, jugados, desconvocado, minutos, minbanquillo,mindesconvocado);
                                delantero.setTemp_golesPartido(golesPartido);
                                delantero.setTemp_amarillasPartido(amarillasPartido);
                                delantero.setTemp_rojasPartido(rojasPartido);

                                if (origen.equals("campo")) {
                                    delantero.setTemp_tiempoC(((SystemClock.elapsedRealtime() - tiempo.getBase()) / 1000 / 60) - delantero.getTemp_tiempoB());
                                    int posicion = Integer.parseInt(event.getClipData().getItemAt(11).getText().toString());

                                    if(origen2.equals("delantero")){
                                        listaDelanteros.remove(posicion);
                                        adapterDelanteros.notifyDataSetChanged();
                                    }
                                    else if (origen2.equals("medio")){
                                        listaCentrocampistas.remove(posicion);
                                        adapterCentrocampistas.notifyDataSetChanged();
                                    }
                                    else if(origen2.equals("defensa")){
                                        listaDefensas.remove(posicion);
                                        adapterDefensas.notifyDataSetChanged();
                                    }
                                    else if(origen2.equals("portero")){
                                        listaPortero.remove(posicion);
                                        adapterPortero.notifyDataSetChanged();
                                    }
                                }

                                int contador = cuentaJugadoresCampo(listaDelanteros,listaCentrocampistas,listaDefensas,listaPortero);
                                if(contador >= jugadores){
                                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.maxjugadores),Toast.LENGTH_SHORT).show();
                                    break;
                                }

                                if (origen.equals("convocatoria")) {
                                    int posicion = Integer.parseInt(event.getClipData().getItemAt(11).getText().toString());
                                    convocatoria.remove(posicion);
                                    adapter.notifyDataSetChanged();
                                }

                                if (origen.equals("banco")) {
                                    delantero.setTemp_tiempoB(((SystemClock.elapsedRealtime() - tiempo.getBase()) / 1000 / 60) - delantero.getTemp_tiempoC());
                                    int posicion = Integer.parseInt(event.getClipData().getItemAt(11).getText().toString());
                                    listaSuplentes.remove(posicion);
                                    adapterSuplentes.notifyDataSetChanged();
                                }

                                listaDelanteros.add(delantero);
                                adapterDelanteros.notifyDataSetChanged();
                                view.setVisibility(View.VISIBLE);
                                break;
                            }catch (IOException ex){
                                break;
                            }
                        //}

                    case DragEvent.ACTION_DRAG_ENDED:
                    default:
                        break;
                }
                return true;
            }
        });

        //Arrastre a suplentes
        suplentes.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                int action = event.getAction();
                switch (event.getAction()) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        // do nothing
                        break;
                    case DragEvent.ACTION_DRAG_ENTERED:
                        break;
                    case DragEvent.ACTION_DRAG_EXITED:
                        break;
                    case DragEvent.ACTION_DROP:
                        View view = (View) event.getLocalState();
                        //Obtenemos los datos
                        int contador = cuentaJugadoresBanquillo(listaSuplentes);
                        String origen = event.getClipData().getItemAt(10).getText().toString();
                        if(contador >= (jugBanquillo + 1)){
                            Toast.makeText(getApplicationContext(),getResources().getString(R.string.maxjugadores),Toast.LENGTH_SHORT).show();
                            break;
                        }
                        else {

                            try {

                                int id = Integer.parseInt(event.getClipData().getItemAt(0).getText().toString());
                                int equipo = Integer.parseInt(event.getClipData().getItemAt(1).getText().toString());
                                String nombre = event.getClipData().getItemAt(2).getText().toString();
                                String apellidos = event.getClipData().getItemAt(3).getText().toString();
                                String mote = event.getClipData().getItemAt(4).getText().toString();
                                String puesto = event.getClipData().getItemAt(5).getText().toString();
                                int jugados = Integer.parseInt(event.getClipData().getItemAt(6).getText().toString());
                                int desconvocado = Integer.parseInt(event.getClipData().getItemAt(7).getText().toString());
                                int minutos = Integer.parseInt(event.getClipData().getItemAt(8).getText().toString());
                                int minbanquillo = Integer.parseInt(event.getClipData().getItemAt(9).getText().toString());
                                int goles = Integer.parseInt(event.getClipData().getItemAt(12).getText().toString());

                                Uri uriFoto = event.getClipData().getItemAt(13).getUri();
                                Bitmap foto = MediaStore.Images.Media.getBitmap(getContentResolver(), uriFoto);
                                String origen2 = event.getClipData().getItemAt(14).getText().toString();
                                int golesPartido = Integer.parseInt(event.getClipData().getItemAt(15).getText().toString());
                                int mindesconvocado = Integer.parseInt(event.getClipData().getItemAt(16).getText().toString());
                                int amarillas = Integer.parseInt(event.getClipData().getItemAt(17).getText().toString());
                                int rojas = Integer.parseInt(event.getClipData().getItemAt(18).getText().toString());
                                int amarillasPartido = Integer.parseInt(event.getClipData().getItemAt(19).getText().toString());
                                int rojasPartido = Integer.parseInt(event.getClipData().getItemAt(20).getText().toString());
                                Jugador suplente = new Jugador(id, equipo, nombre, apellidos, mote, puesto, goles,amarillas,rojas, foto, jugados, desconvocado, minutos, minbanquillo,mindesconvocado);
                                suplente.setTemp_golesPartido(golesPartido);
                                suplente.setTemp_amarillasPartido(amarillasPartido);
                                suplente.setTemp_rojasPartido(rojasPartido);
                                if (origen.equals("convocatoria")) {
                                    int posicion = Integer.parseInt(event.getClipData().getItemAt(11).getText().toString());
                                    convocatoria.remove(posicion);
                                    adapter.notifyDataSetChanged();
                                }
                                if (origen.equals("campo")) {
                                    suplente.setTemp_tiempoC(((SystemClock.elapsedRealtime() - tiempo.getBase()) / 1000 / 60) - suplente.getTemp_tiempoB());
                                    int posicion = Integer.parseInt(event.getClipData().getItemAt(11).getText().toString());

                                    if(origen2.equals("delantero")){
                                        listaDelanteros.remove(posicion);
                                        adapterDelanteros.notifyDataSetChanged();
                                    }
                                    else if (origen2.equals("medio")){
                                        listaCentrocampistas.remove(posicion);
                                        adapterCentrocampistas.notifyDataSetChanged();
                                    }
                                    else if(origen2.equals("defensa")){
                                        listaDefensas.remove(posicion);
                                        adapterDefensas.notifyDataSetChanged();
                                    }
                                    else if(origen2.equals("portero")){
                                        listaPortero.remove(posicion);
                                        adapterPortero.notifyDataSetChanged();
                                    }
                                }
                                if (origen.equals("banco")) {
                                    suplente.setTemp_tiempoB(((SystemClock.elapsedRealtime() - tiempo.getBase()) / 1000 / 60) - suplente.getTemp_tiempoC());
                                }
                                listaSuplentes.add(suplente);
                                adapterSuplentes.notifyDataSetChanged();
                                view.setVisibility(View.VISIBLE);
                                break;

                            }catch (IOException ex){
                                break;
                            }
                        }
                    case DragEvent.ACTION_DRAG_ENDED:
                    default:
                        break;
                }
                return true;
            }
        });

        //Evento de arrastre desde banquillo
        banquillo.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id){
                //Jugador seleccionado
                try{

                Jugador jug = (Jugador)parent.getItemAtPosition(position);
                //Pasamos los datos del jugador
                ClipData.Item itemID = new ClipData.Item(String.valueOf(jug.getId()));
                ClipData.Item itemEquipo = new ClipData.Item(String.valueOf(jug.getId_equipo()));
                ClipData.Item itemNombre = new ClipData.Item(jug.getNombre());
                ClipData.Item itemApellidos = new ClipData.Item(jug.getApellidos());
                ClipData.Item itemMote = new ClipData.Item(jug.getMote());
                ClipData.Item itemDorsal = new ClipData.Item(jug.getPosicion());
                ClipData.Item itemPartidosJugados = new ClipData.Item(String.valueOf(jug.getPartidos_jugados()));
                ClipData.Item itemPartidosDesconvocado = new ClipData.Item(String.valueOf(jug.getPartidos_desconvocado()));
                ClipData.Item itemMinutos = new ClipData.Item(String.valueOf(jug.getMinutos_jugados_totales()));
                ClipData.Item itemBanquillo = new ClipData.Item(String.valueOf(jug.getMinutos_banquillo_totales()));
                //Enviamos además la posicion en lista y un identificador de desde donde arrastramos
                ClipData.Item posicion = new ClipData.Item(String.valueOf(position));
                ClipData.Item origen = new ClipData.Item("convocatoria");
                ClipData.Item goles = new ClipData.Item(String.valueOf(jug.getGoles()));
                    Uri uriFoto = convertToUri(jug.getFotografia());
                    ClipData.Item foto = new ClipData.Item(uriFoto);
                    ClipData.Item origen2 = new ClipData.Item("");
                    ClipData.Item golesPartido = new ClipData.Item(String.valueOf(jug.getTemp_golesPartido()));
                    ClipData.Item minutosDesconvocado = new ClipData.Item(String.valueOf(jug.getMinutos_desconvocado_totales()));
                    ClipData.Item amarillas = new ClipData.Item(String.valueOf(jug.getAmarillas()));
                    ClipData.Item rojas = new ClipData.Item(String.valueOf(jug.getRojas()));
                    ClipData.Item amarillasPartido = new ClipData.Item(String.valueOf(jug.getTemp_amarillasPartido()));
                    ClipData.Item rojasPartido = new ClipData.Item(String.valueOf(jug.getTemp_rojasPartido()));

                String[] mimeTypes = {ClipDescription.MIMETYPE_TEXT_PLAIN};
                ClipData dragData = new ClipData("",mimeTypes, itemID);
                dragData.addItem(itemEquipo);
                dragData.addItem(itemNombre);
                dragData.addItem(itemApellidos);
                dragData.addItem(itemMote);
                dragData.addItem(itemDorsal);
                dragData.addItem(itemPartidosJugados);
                dragData.addItem(itemPartidosDesconvocado);
                dragData.addItem(itemMinutos);
                dragData.addItem(itemBanquillo);
                dragData.addItem(origen);
                dragData.addItem(posicion);
                dragData.addItem(goles);

                    dragData.addItem(foto);
                    dragData.addItem(origen2);
                    dragData.addItem(golesPartido);
                    dragData.addItem(minutosDesconvocado);
                    dragData.addItem(amarillas);
                    dragData.addItem(rojas);
                    dragData.addItem(amarillasPartido);
                    dragData.addItem(rojasPartido);
                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
                    view.startDrag(dragData,shadowBuilder,view,0);
                    return true;
                }catch (IOException e){
                    e.printStackTrace();
                    return false;
                }
            }
        });

    }

    public int cuentaJugadoresCampo(List<Jugador>del,List<Jugador>cent, List<Jugador> def, List<Jugador> port){
        int contador = 0;
        contador = contador + del.size() + cent.size() + def.size() + port.size();
        return  contador;
    }

    public int cuentaJugadoresBanquillo(List<Jugador> sup){
        int contador = 0;
        contador = contador + sup.size();
        return  contador;
    }

    public Uri convertToUri(Bitmap image)throws IOException{
        //File tempDir= Environment.getExternalStorageDirectory();
        //tempDir=new File(tempDir.getAbsolutePath()+"/.tmp/");
        //tempDir.mkdirs();
        //File tempFile = File.createTempFile(titulo, ".jpg", tempDir);

        File tempDir = getApplicationContext().getCacheDir(); // context being the Activity pointer
        File tempFile = File.createTempFile(titulo, ".jpg", tempDir);

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        byte[] bitmapData = bytes.toByteArray();

        //write the bytes in file
        FileOutputStream fos = new FileOutputStream(tempFile);
        fos.write(bitmapData);
        fos.flush();
        fos.close();
        return Uri.fromFile(tempFile);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void onBackPressed(){
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(getResources().getString(R.string.tituloSalir))
                .setMessage(getResources().getString(R.string.mensajeSalir))
                .setPositiveButton(getResources().getString(R.string.si), new DialogInterface.OnClickListener()
                {

                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }

    private class ConsultasTask extends AsyncTask<Void, Void, Boolean>{

        //Cogemos las listas
        List<Jugador> convocados;
        List<Jugador> suplentes;
        List<Jugador> delanteros;
        List<Jugador> medios;
        List<Jugador> expulsados;
        List<Jugador> defensas;
        List<Jugador> portero;

        public ConsultasTask(List<Jugador> convocados, List<Jugador> suplentes, List<Jugador> delanteros, List<Jugador> medios, List<Jugador> defensas, List<Jugador> portero, List<Jugador> expulsados){
            this.convocados = convocados;
            this.suplentes = suplentes;
            this.defensas = defensas;
            this.delanteros = delanteros;
            this.medios = medios;
            this.portero = portero;
            this.expulsados = expulsados;
        }


        @Override
        public Boolean doInBackground(Void... voids) {

            //Cogemos lista de partidos de ese equipo contando los que hay
            int contador = 1;
            Cursor c = null;
            c = dbAdapter.getPartidosLista(MainActivity.id_equipo);
            if (c.moveToFirst()) {
                //Recorremos el cursor hasta que no haya más registros
                do {
                    contador++;
                } while (c.moveToNext());
            }

            //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            //String date = sdf.format(new Date());


            //Creamos uno nuevo añadiendo un valor mas al contador
            dbAdapter.insertarPartido(MainActivity.id_equipo,contador,fecha,rival,campo);

            //Cogemos el id de ese nuevo partido
            c = dbAdapter.getPartido(MainActivity.id_equipo,contador);
            int id = 0;
            if (c.moveToFirst()) {
                //Recorremos el cursor hasta que no haya más registros
                do {
                    id = c.getInt(0);
                } while (c.moveToNext());
            }

            //Para cada jugador actualizamos sus datos
            for (int i = 0; i< convocados.size(); i++){
                //Les ponemos en jugador_partido convocado=NO
                dbAdapter.insertarJugadorPartido(convocados.get(i).getId(),id,convocados.get(i).getMote(),convocados.get(i).getTemp_golesPartido(),convocados.get(i).getTemp_amarillasPartido(),convocados.get(i).getTemp_rojasPartido(),false,0,0,convocados.get(i).getTemp_tiempoD());
                dbAdapter.actualizarJugador(convocados.get(i).getId(),convocados.get(i).getPartidos_jugados(),convocados.get(i).getPartidos_desconvocado() + 1, convocados.get(i).getGoles() + convocados.get(i).getTemp_golesPartido(),convocados.get(i).getAmarillas() + convocados.get(i).getTemp_amarillasPartido(),convocados.get(i).getRojas()+convocados.get(i).getTemp_rojasPartido(),
                        convocados.get(i).getMinutos_jugados_totales(), convocados.get(i).getMinutos_banquillo_totales(),convocados.get(i).getMinutos_desconvocado_totales() + convocados.get(i).getTemp_tiempoD());
            }

            for (int i =0;i< delanteros.size(); i++){
                dbAdapter.insertarJugadorPartido(delanteros.get(i).getId(),id,delanteros.get(i).getMote(),delanteros.get(i).getTemp_golesPartido(),delanteros.get(i).getTemp_amarillasPartido(),delanteros.get(i).getTemp_rojasPartido(),true,delanteros.get(i).getTemp_tiempoC(),delanteros.get(i).getTemp_tiempoB(),0);
                dbAdapter.actualizarJugador(delanteros.get(i).getId(),delanteros.get(i).getPartidos_jugados() + 1,delanteros.get(i).getPartidos_desconvocado(),delanteros.get(i).getGoles() + delanteros.get(i).getTemp_golesPartido(),delanteros.get(i).getAmarillas() + delanteros.get(i).getTemp_amarillasPartido(),delanteros.get(i).getRojas()+delanteros.get(i).getTemp_rojasPartido(),
                        delanteros.get(i).getMinutos_jugados_totales() + delanteros.get(i).getTemp_tiempoC(), delanteros.get(i).getMinutos_banquillo_totales() + delanteros.get(i).getTemp_tiempoB(),delanteros.get(i).getMinutos_desconvocado_totales());
            }

            for (int i =0;i< medios.size(); i++){
                dbAdapter.insertarJugadorPartido(medios.get(i).getId(),id,medios.get(i).getMote(),medios.get(i).getTemp_golesPartido(),medios.get(i).getTemp_amarillasPartido(),medios.get(i).getTemp_rojasPartido(),true,medios.get(i).getTemp_tiempoC(),medios.get(i).getTemp_tiempoB(),0);
                dbAdapter.actualizarJugador(medios.get(i).getId(),medios.get(i).getPartidos_jugados() + 1,medios.get(i).getPartidos_desconvocado(),medios.get(i).getGoles() + medios.get(i).getTemp_golesPartido(),medios.get(i).getAmarillas() + medios.get(i).getTemp_amarillasPartido(),medios.get(i).getRojas()+medios.get(i).getTemp_rojasPartido(),
                        medios.get(i).getMinutos_jugados_totales() + medios.get(i).getTemp_tiempoC(), medios.get(i).getMinutos_banquillo_totales() + medios.get(i).getTemp_tiempoB(),medios.get(i).getMinutos_desconvocado_totales());
            }

            for (int i =0;i< defensas.size(); i++){
                dbAdapter.insertarJugadorPartido(defensas.get(i).getId(),id,defensas.get(i).getMote(),defensas.get(i).getTemp_golesPartido(),defensas.get(i).getTemp_amarillasPartido(),defensas.get(i).getTemp_rojasPartido(),true,defensas.get(i).getTemp_tiempoC(),defensas.get(i).getTemp_tiempoB(),0);
                dbAdapter.actualizarJugador(defensas.get(i).getId(),defensas.get(i).getPartidos_jugados() + 1,defensas.get(i).getPartidos_desconvocado(),defensas.get(i).getGoles() + defensas.get(i).getTemp_golesPartido(),defensas.get(i).getAmarillas() + defensas.get(i).getTemp_amarillasPartido(),defensas.get(i).getRojas()+defensas.get(i).getTemp_rojasPartido(),
                        defensas.get(i).getMinutos_jugados_totales() + defensas.get(i).getTemp_tiempoC(), defensas.get(i).getMinutos_banquillo_totales() + defensas.get(i).getTemp_tiempoB(),defensas.get(i).getMinutos_desconvocado_totales());
            }

            for (int i =0;i< portero.size(); i++){
                dbAdapter.insertarJugadorPartido(portero.get(i).getId(),id,portero.get(i).getMote(),portero.get(i).getTemp_golesPartido(),portero.get(i).getTemp_amarillasPartido(),portero.get(i).getTemp_rojasPartido(),true,portero.get(i).getTemp_tiempoC(),portero.get(i).getTemp_tiempoB(),0);
                dbAdapter.actualizarJugador(portero.get(i).getId(),portero.get(i).getPartidos_jugados() + 1,portero.get(i).getPartidos_desconvocado(),portero.get(i).getGoles() + portero.get(i).getTemp_golesPartido(),portero.get(i).getAmarillas() + portero.get(i).getTemp_amarillasPartido(),portero.get(i).getRojas()+portero.get(i).getTemp_rojasPartido(),
                        portero.get(i).getMinutos_jugados_totales() + portero.get(i).getTemp_tiempoC(), portero.get(i).getMinutos_banquillo_totales() + portero.get(i).getTemp_tiempoB(),portero.get(i).getMinutos_desconvocado_totales());
            }

            //Banquillo
            for (int i = 0; i<suplentes.size();i++){
                dbAdapter.insertarJugadorPartido(suplentes.get(i).getId(),id,suplentes.get(i).getMote(),suplentes.get(i).getTemp_golesPartido(),suplentes.get(i).getTemp_amarillasPartido(),suplentes.get(i).getTemp_rojasPartido(),true,suplentes.get(i).getTemp_tiempoC(),suplentes.get(i).getTemp_tiempoB(),0);
                dbAdapter.actualizarJugador(suplentes.get(i).getId(),suplentes.get(i).getPartidos_jugados() + 1,suplentes.get(i).getPartidos_desconvocado(),suplentes.get(i).getGoles() + suplentes.get(i).getTemp_golesPartido(),suplentes.get(i).getAmarillas() + suplentes.get(i).getTemp_amarillasPartido(),suplentes.get(i).getRojas()+suplentes.get(i).getTemp_rojasPartido(),
                        suplentes.get(i).getMinutos_jugados_totales() + suplentes.get(i).getTemp_tiempoC(), suplentes.get(i).getMinutos_banquillo_totales()+suplentes.get(i).getTemp_tiempoB(),suplentes.get(i).getMinutos_desconvocado_totales());
            }

            for (int i=0;i<expulsados.size();i++){
                dbAdapter.insertarJugadorPartido(expulsados.get(i).getId(),id,expulsados.get(i).getMote(),expulsados.get(i).getTemp_golesPartido(),expulsados.get(i).getTemp_amarillasPartido(),expulsados.get(i).getTemp_rojasPartido(),true,expulsados.get(i).getTemp_tiempoC(),expulsados.get(i).getTemp_tiempoB(),0);
                dbAdapter.actualizarJugador(expulsados.get(i).getId(),expulsados.get(i).getPartidos_jugados() + 1,expulsados.get(i).getPartidos_desconvocado(),expulsados.get(i).getGoles() + expulsados.get(i).getTemp_golesPartido(),expulsados.get(i).getAmarillas() + expulsados.get(i).getTemp_amarillasPartido(),expulsados.get(i).getRojas()+expulsados.get(i).getTemp_rojasPartido(),
                        expulsados.get(i).getMinutos_jugados_totales() + expulsados.get(i).getTemp_tiempoC(), expulsados.get(i).getMinutos_banquillo_totales()+expulsados.get(i).getTemp_tiempoB(),expulsados.get(i).getMinutos_desconvocado_totales());
            }


            return true;
        }

        protected void onPostExecute(Boolean bool) {
            setResult(Activity.RESULT_OK);
            finish();
            /*Intent intent = new Intent(NuevoPartidoActivity.this,MainActivity.class);
            intent.putExtra("id",MainActivity.id_equipo);
            intent.putExtra("nombre",MainActivity.nombre);
            intent.putExtra("escudo",MainActivity.escudo);
            intent.putExtra("categoria",MainActivity.categoria);
            startActivity(intent);*/
        }

    }


}

package com.imovil.casta.tiempodejuegofinal.modelo;

import android.graphics.Bitmap;

/**
 * Created by casta on 23/3/17.
 */

public class Equipo {

    //Atributos
    private int id;
    private String nombre;
    private Bitmap escudo;
    private String categoria;

    //Constructores
    public Equipo(int id, String nombre, Bitmap escudo,String categoria){

        this.id = id;
        this.nombre = nombre;
        this.escudo = escudo;
        this.categoria = categoria;
    }

    //Getters y Setters
    public int getId(){ return id;}

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Bitmap getEscudo() {
        return escudo;
    }

    public void setEscudo(Bitmap escudo) {
        this.escudo = escudo;
    }


    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
}

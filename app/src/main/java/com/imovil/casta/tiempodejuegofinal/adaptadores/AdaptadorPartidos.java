package com.imovil.casta.tiempodejuegofinal.adaptadores;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.imovil.casta.tiempodejuegofinal.MainActivity;
import com.imovil.casta.tiempodejuegofinal.R;
import com.imovil.casta.tiempodejuegofinal.modelo.Jugador;
import com.imovil.casta.tiempodejuegofinal.modelo.Partido;

import java.text.DateFormat;
import java.util.List;

/**
 * Created by casta on 22/5/17.
 */

public class AdaptadorPartidos extends BaseAdapter {

    static class ViewHolder {
        //public TextView partido;
        public TextView fechaPartido;
        public TextView rival;
        public ImageView campo;
    }

    private final List<Partido> listaPartidos;
    public LayoutInflater inflater;

    //Constructor del adatador
    public AdaptadorPartidos(Context context, List<Partido> listaPartidos) {

        if (context == null) {
            throw new IllegalArgumentException();
        }

        this.listaPartidos=listaPartidos;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listaPartidos.size();
    }

    @Override
    public Object getItem(int position) {
        return listaPartidos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        AdaptadorPartidos.ViewHolder viewHolder;
        if (rowView == null) {

            rowView = inflater.inflate(R.layout.lista_partidos,parent,false);
            viewHolder = new AdaptadorPartidos.ViewHolder();
            //viewHolder.partido = (TextView) rowView.findViewById(R.id.tvPartido);
            viewHolder.fechaPartido = (TextView) rowView.findViewById(R.id.tvFecha);
            viewHolder.rival = (TextView) rowView.findViewById(R.id.tvRival);
            viewHolder.campo = (ImageView) rowView.findViewById(R.id.ivCampo);
            rowView.setTag(viewHolder);

        } else {
            viewHolder = (AdaptadorPartidos.ViewHolder) rowView.getTag();
        }

        Partido partido= (Partido) getItem(position);
        //viewHolder.partido.setText(viewHolder.partido.getContext().getResources().getString(R.string.partido) + " " + String.valueOf(partido.getPartidoEquipo()));

        viewHolder.rival.setText(partido.getFecha());
        if (partido.getCampo().equals("Local")){
            viewHolder.fechaPartido.setText(MainActivity.nombre + " vs " + partido.getRival());
            viewHolder.campo.setImageBitmap(BitmapFactory.decodeResource(viewHolder.campo.getResources(), R.mipmap.im_local));
        }

        else {
            viewHolder.fechaPartido.setText(partido.getRival() + " vs " + MainActivity.nombre);
            viewHolder.campo.setImageBitmap(BitmapFactory.decodeResource(viewHolder.campo.getResources(),R.mipmap.im_visitante));
        }


        return rowView;

    }
}

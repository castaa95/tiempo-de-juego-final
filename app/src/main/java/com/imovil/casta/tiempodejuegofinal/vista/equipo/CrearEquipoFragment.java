package com.imovil.casta.tiempodejuegofinal.vista.equipo;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;


import com.imovil.casta.tiempodejuegofinal.MainActivity;
import com.imovil.casta.tiempodejuegofinal.R;
import com.imovil.casta.tiempodejuegofinal.adaptadores.ImagePicker;
import com.imovil.casta.tiempodejuegofinal.basedatos.DBAdapter;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/*
Crear nuevos equipos
 */
public class CrearEquipoFragment extends Fragment {

    static final int PHOTO_SELECTED = 1;


    EditText textNombre;
    ImageView eligeEscudo;
    Spinner spinnerCategorias;
    String categoria = "";
    DBAdapter dbAdapter;

    private Bitmap imagen;
    private String keyImagen = "imagen";

    public CrearEquipoFragment() {
        // Required empty public constructor
    }


    public static CrearEquipoFragment newInstance() {
        CrearEquipoFragment fragment = new CrearEquipoFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        //Cogemos el escudo anterior por si giramos la pantalla
        if (savedInstanceState != null){
            imagen = savedInstanceState.getParcelable(keyImagen);
        }
        else{
            imagen = BitmapFactory.decodeResource(getResources(),R.mipmap.escudo_defecto);
        }
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_crear_equipo, container, false);

        FloatingActionButton creaEquipo;
        textNombre = (EditText) root.findViewById(R.id.etNombre);
        //Conecta con BBDD
        dbAdapter = new DBAdapter(getActivity().getApplicationContext());
        dbAdapter.open();

        //Preparamos el spinner
        spinnerCategorias = (Spinner) root.findViewById(R.id.spCategoria);
        final String [] categorias = getResources().getStringArray(R.array.array_categorias);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, categorias);
        spinnerCategorias.setAdapter(adapter);
        spinnerCategorias.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Object elegido = parent.getItemAtPosition(position);
                switch (position){
                    case 0: categoria = "Pista"; break;
                    case 1: categoria ="Fútbol 8"; break;
                    case 2: categoria = "Fútbol 11" ;break;
                }
                Log.i("Jugadores",categoria);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        //Elige escudo de la galeria
        eligeEscudo = (ImageView) root.findViewById(R.id.ivEscudo);
        eligeEscudo.setImageBitmap(imagen);
        eligeEscudo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seleccionaEscudo();
            }
        });

        //Evento de boton que mete equipo en BBDD y pasa a crear jugadores
        creaEquipo = (FloatingActionButton) root.findViewById(R.id.bCrearPlantilla);
        creaEquipo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nuevoEquipo();
            }
        });

        return root;
    }

            @Override
            public void onActivityResult(int requestCode, int resultCode, Intent imagenEscogida) {

                switch(requestCode) {
                    case PHOTO_SELECTED:
                        Bitmap bitmap = ImagePicker.getImageFromResult(this.getContext(), resultCode, imagenEscogida);
                        imagen = bitmap;
                        if (bitmap == null){
                            bitmap = BitmapFactory.decodeResource(getResources(),R.mipmap.escudo_defecto);
                            imagen = bitmap;
                        }
                        eligeEscudo.setImageBitmap(bitmap);
                        break;
                    default:
                        super.onActivityResult(requestCode, resultCode, imagenEscogida);
                        break;
                }


            }

            //Metodo para seleccionar el escudo
            public void seleccionaEscudo(){
                Intent chooseImageIntent = ImagePicker.getPickEscudoIntent(this.getContext());
                startActivityForResult(chooseImageIntent, PHOTO_SELECTED);
            }

            //Metodo para pasar Bitmap a byte para poder meter la imagen en BBDD
            public static byte[] getBitmapAsByteArray(Bitmap bitmap) {
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 0, outputStream);
                return outputStream.toByteArray();
            }

            //Metodo que recoge los valores y añade un equipo a la BBDD
            private void nuevoEquipo(){
                //Obtenemos los campos del equipo
                boolean error = false;
                String nombre = textNombre.getText().toString();
                if (TextUtils.isEmpty(nombre)){
                    textNombre.setError(getResources().getText(R.string.campoVacio));
                    error = true;
                }
                //Pasamos la imagen a byte
                byte[] imagen = getBitmapAsByteArray(((BitmapDrawable)eligeEscudo.getDrawable()).getBitmap());

                if (categoria == ""){
                    error = true;
                }

                if (error) return;

                Log.i("categoria", categoria);
                //Ahora metemos los valores en BBDD

                new CrearEquipoTask(nombre,imagen,categoria).execute();

            }

    //Recoge el estado del escudo
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(keyImagen,imagen);
        super.onSaveInstanceState(outState);
    }

    private class CrearEquipoTask extends AsyncTask<Void, Void, Boolean>{

        String nombre;
        byte[] imagen;
        String categoria;

        public CrearEquipoTask(String nombre,byte[] imagen,String categoria){
            this.nombre = nombre;
            this.imagen = imagen;
            this.categoria=categoria;
        }

        @Override
        public Boolean doInBackground(Void... voids) {

            return dbAdapter.insertarEquipo(nombre,imagen,categoria);

        }

        @Override
        protected void onPostExecute(Boolean bool) {
            getActivity().setResult(Activity.RESULT_OK);
            getActivity().finish();
            Intent intent = new Intent(getActivity(),MainActivity.class);
            intent.putExtra("id",MainActivity.id_equipo);
            intent.putExtra("nombre",MainActivity.nombre);
            intent.putExtra("escudo",MainActivity.escudo);
            intent.putExtra("categoria",MainActivity.categoria);
            startActivity(intent);

        }

    }
}

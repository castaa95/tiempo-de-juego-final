package com.imovil.casta.tiempodejuegofinal.vista.plantilla;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.imovil.casta.tiempodejuegofinal.R;
import com.imovil.casta.tiempodejuegofinal.adaptadores.AdaptadorPlantilla;
import com.imovil.casta.tiempodejuegofinal.basedatos.DBAdapter;
import com.imovil.casta.tiempodejuegofinal.modelo.Jugador;

import java.util.ArrayList;
import java.util.List;

public class PlantillaFragment extends Fragment {

    DBAdapter dbAdapter;

    public static final String ARGS_EQUIPO = "id_equipo";
    private boolean mTwoPanes = false;

    List<String> nombres = new ArrayList<String>();
    List<String> apellidos = new ArrayList<String>();
    List<String> motes = new ArrayList<String>();
    List<Bitmap> fotografias = new ArrayList<Bitmap>();
    List<String> posiciones = new ArrayList<String>();
    List<Integer> goles = new ArrayList<Integer>();
    List<Integer> amarillas = new ArrayList<Integer>();
    List<Integer> rojas = new ArrayList<Integer>();
    List<Integer> partidosJugados = new ArrayList<Integer>();
    List<Integer> partidosDesconvocado = new ArrayList<Integer>();
    List<Long> minutosTotales = new ArrayList<Long>();
    List<Long> minutosBanquillo = new ArrayList<Long>();
    List<Long> minutosDesconvocado = new ArrayList<Long>();
    List<Integer> id = new ArrayList<Integer>();
    List<Integer> id_equipo = new ArrayList<Integer>();

    private ListView listaJugadores;
    private TextView listaVacia;
    private FloatingActionButton fabJugadores;

    public PlantillaFragment() {
        // Required empty public constructor
    }

    public static PlantillaFragment newInstance(int id_equipo) {
        PlantillaFragment fragment = new PlantillaFragment();
        Bundle args = new Bundle();
        args.putInt(ARGS_EQUIPO, id_equipo);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_plantilla, container, false);

        listaJugadores = (ListView)root.findViewById(R.id.lvJugadores);
        fabJugadores = (FloatingActionButton) root.findViewById(R.id.fabJugador);
        listaVacia = (TextView) root.findViewById(R.id.noJugadores);

        //Rellenamos el listView
        Bundle args = getArguments();
        int equipo = args.getInt(ARGS_EQUIPO, 0);
        cargarJugadores(equipo);

        //Creamos el adapter
        final AdaptadorPlantilla adapter = new AdaptadorPlantilla(getContext(),crearListaJugadores(nombres,apellidos,motes,fotografias,posiciones,goles,amarillas,rojas,partidosJugados, partidosDesconvocado, minutosTotales,minutosBanquillo,minutosDesconvocado,id,id_equipo));
        listaJugadores.setAdapter(adapter);

        //Manejador del floating button
        fabJugadores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                crearJugador();
            }
        });

        //Manejador de seleccion de jugador
        listaJugadores.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Cogemos datos del jugador
                Jugador seleccionado = (Jugador) adapter.getItem(position);
                //Vamos a la nueva actividad pasandole los datos
                Intent intent = new Intent(getActivity(), DatosJugadorActivity.class);
                intent.putExtra("idJugador",seleccionado.getId());
                intent.putExtra("nombreJugador",seleccionado.getNombre());
                intent.putExtra("apellidosJugador",seleccionado.getApellidos());
                intent.putExtra("moteJugador",seleccionado.getMote());
                intent.putExtra("posicionJugador", seleccionado.getPosicion());
                intent.putExtra("minJugados",seleccionado.getMinutos_jugados_totales());
                intent.putExtra("minBanquillo",seleccionado.getMinutos_banquillo_totales());
                intent.putExtra("minDesconvocado",seleccionado.getMinutos_desconvocado_totales());
                intent.putExtra("golesJugador",seleccionado.getGoles());
                intent.putExtra("amarillasJugador",seleccionado.getAmarillas());
                intent.putExtra("rojasJugador",seleccionado.getRojas());
                intent.putExtra("partidosJugador", seleccionado.getPartidos_jugados());
                intent.putExtra("desconvocadosJugador", seleccionado.getPartidos_desconvocado());

                startActivity(intent);
            }
        });

        return root;
    }

    private List<Jugador> crearListaJugadores(List<String> nombres, List<String> apellidos, List<String> motes, List<Bitmap> fotografias, List<String> posiciones, List<Integer> goles,List<Integer> amarillas,List<Integer> rojas, List<Integer> partidosJugados,
     List<Integer> partidosDesconvocado,List<Long> minutosTotales, List<Long> minutosBanquillo,List<Long> minutosDesconvocado, List<Integer>id, List<Integer>id_equipo) {

        if (nombres.size() != apellidos.size()) {
            throw new IllegalStateException();
        }

        ArrayList<Jugador> listaJugadores = new ArrayList<Jugador>(nombres.size());
        for (int i = 0; i < nombres.size(); i++) {
            listaJugadores.add(new Jugador(id.get(i),id_equipo.get(i),nombres.get(i), apellidos.get(i), motes.get(i), posiciones.get(i), goles.get(i),amarillas.get(i),rojas.get(i), fotografias.get(i), partidosJugados.get(i),partidosDesconvocado.get(i),minutosTotales.get(i),minutosBanquillo.get(i),minutosDesconvocado.get(i) ));
        }
        return listaJugadores;
    }

    private void crearJugador(){
        Intent intent = new Intent(getActivity(), CrearJugadorActivity.class);
        startActivity(intent);
    }

    private void cargarJugadores(int equipo){
        dbAdapter = new DBAdapter(getContext());
        dbAdapter.open();

        nombres = new ArrayList<String>();
        apellidos = new ArrayList<String>();
        motes = new ArrayList<String>();
        fotografias = new ArrayList<Bitmap>();
        posiciones = new ArrayList<String>();
        goles = new ArrayList<Integer>();
        amarillas = new ArrayList<Integer>();
        rojas = new ArrayList<Integer>();
        partidosJugados = new ArrayList<Integer>();
        partidosDesconvocado = new ArrayList<Integer>();
        minutosTotales = new ArrayList<Long>();
        minutosBanquillo = new ArrayList<Long>();
        minutosDesconvocado = new ArrayList<Long>();
        id = new ArrayList<Integer>();
        id_equipo = new ArrayList<Integer>();


        Cursor c = null;
        c  = dbAdapter.getJugadorLista(equipo);

        //Sacamos los campos
        if (c.moveToFirst()) {
            //Recorremos el cursor hasta que no haya más registros
            do {
                id.add(c.getInt(0));
                id_equipo.add(c.getInt(1));
                nombres.add(c.getString(2));
                apellidos.add(c.getString(3));
                motes.add(c.getString(4));
                posiciones.add(c.getString(5));
                goles.add(c.getInt(6));
                amarillas.add(c.getInt(7));
                rojas.add(c.getInt(8));
                byte[] foto = (c.getBlob(9));
                partidosJugados.add(c.getInt(10));
                partidosDesconvocado.add(c.getInt(11));
                minutosTotales.add(c.getLong(12));
                minutosBanquillo.add(c.getLong(13));
                minutosDesconvocado.add(c.getLong(14));
                //Pasamos el escudo a Bitmap
                Bitmap bm = BitmapFactory.decodeByteArray(foto, 0 ,foto.length);
                fotografias.add(bm);
            } while (c.moveToNext());
        }

        if (nombres.size() == 0){
            listaVacia.setVisibility(View.VISIBLE);
        }
        else {
            listaVacia.setVisibility(View.INVISIBLE);
        }
    }

}

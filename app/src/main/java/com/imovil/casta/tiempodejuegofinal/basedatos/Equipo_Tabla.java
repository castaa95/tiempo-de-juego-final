package com.imovil.casta.tiempodejuegofinal.basedatos;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

/**
 *
 * Esta clase sera la encargada de gestionar la tabla de Equipo
 *
 * Created by casta on 28/3/17.
 */

public class Equipo_Tabla {

    //Nombre de la tabla
    private final static String NOMBRE_TABLA = "equipo_tabla";

    //Objeto para acceder a la BBDD
    private SQLiteDatabase sqlDB;

    //Constructor
    public Equipo_Tabla(SQLiteDatabase sqlDB){
        this.sqlDB = sqlDB;
    }

    //Clase interna con los campos de la tabla
    public class ColumnsEquipo implements BaseColumns {
        public final static String NOMBRE_TABLA = "equipo_tabla";
       // public final static String ID_EQUIPO = "id_equipo";
        public final static String NOMBRE = "nombre";
        public final static String ESCUDO = "escudo";
        public final static String CATEGORIA = "categoria";
    }

    //Sentencia para crear tabla
    public final static String CREATE_TABLE = "create table if not exists "
            + NOMBRE_TABLA + " (" + ColumnsEquipo._ID + " integer primary key autoincrement, "
            + ColumnsEquipo.NOMBRE + " varchar(30) not null, " + ColumnsEquipo.ESCUDO
            + " blob, " + ColumnsEquipo.CATEGORIA + " varchar(30) not null)";

    public static final String DELETE_TABLE =
            "DROP TABLE IF EXISTS " + NOMBRE_TABLA;

    //Insert
    public boolean crearEquipo(String nombre, byte[] escudo, String categoria){
        ContentValues values = new ContentValues();
        values.put(ColumnsEquipo.NOMBRE, nombre);
        values.put(ColumnsEquipo.ESCUDO, escudo);
        values.put(ColumnsEquipo.CATEGORIA,categoria);

        return sqlDB.insert(NOMBRE_TABLA,null,values)>0;
    }

    //Coger datos del equipo
    public Cursor datosEquipo(int id){
        String[] columns = {ColumnsEquipo._ID,ColumnsEquipo.NOMBRE,ColumnsEquipo.ESCUDO,ColumnsEquipo.CATEGORIA};
        String where = ColumnsEquipo._ID + "=?";
        String[] args = {String.valueOf(id)};
        return sqlDB.query(NOMBRE_TABLA,columns,where,args,null,null,null);
    }

    //Sentencia para coger nombres de equipos y escudos
    public Cursor listaEquipo(){
        String[] columns = {ColumnsEquipo._ID, ColumnsEquipo.NOMBRE, ColumnsEquipo.ESCUDO, ColumnsEquipo.CATEGORIA};
        return sqlDB.query(NOMBRE_TABLA,columns,null,null,null,null,null);
    }

    //Delete team
    public int deleteTeam(int id){
        String where = ColumnsEquipo._ID + "=?";
        String[] args = {String.valueOf(id)};
        return sqlDB.delete(NOMBRE_TABLA,where,args);
    }
}

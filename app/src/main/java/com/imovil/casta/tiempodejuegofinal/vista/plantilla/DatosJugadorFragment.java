package com.imovil.casta.tiempodejuegofinal.vista.plantilla;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.imovil.casta.tiempodejuegofinal.MainActivity;
import com.imovil.casta.tiempodejuegofinal.R;
import com.imovil.casta.tiempodejuegofinal.basedatos.DBAdapter;

import java.text.DecimalFormat;

public class DatosJugadorFragment extends Fragment {

    private static final String ARG_ID = "idJugador";
    private static final String ARG_NOMBRE = "nombreJugador";
    private static final String ARG_APELLIDOS = "apellidosJugador";
    private static final String ARG_MOTE = "moteJugador";
    private static final String ARG_POSICION = "posicionJugador";
    private static final String ARG_GOLES = "golesJugador";
    private static final String ARG_AMARILLAS = "amarillasJugador";
    private static final String ARG_ROJAS = "rojasJugador";
    private static final String ARG_PARTIDOS = "partidosJugador";
    private static final String ARG_DESCONVOCADO = "desconvocadoJugador";
    private static final String ARG_JUGADOS = "jugadosJugador";
    private static final String ARG_BANQUILLO = "banquilloJugador";
    private static final String ARG_MINDESCONVODADO = "mindesconvocadoJugador";

    private int midJugador;
    private String mnombreJugador;
    private String mapellidosJugador;
    private String mmoteJugador;
    private String mposicionJugador;
    private int mgolesJugador;
    private int mamarillasJugador;
    private int mrojasJugador;
    private int mpartidosJugador;
    private int mdesconvocadoJugador;
    private long mjugadosJugador;
    private long mbanquilloJugador;
    private long mmindesconvocadoJugador;

    private TextView mNombre;
    private TextView mApellidos;
    private TextView mMote;
    private TextView mDorsal;
    private TextView mGoles;
    private TextView mAmarillas;
    private TextView mRojas;
    private TextView mPartidos;
    private TextView mDesconvocado;
    private TextView mMinutosJugados;
    private TextView mMinutosBanquillo;
    private TextView mMinutosDesconvocado;
    private TextView mMediaJugado;
    private TextView mMediaBanquillo;
    private TextView mMediaDesconvocado;

    private DBAdapter dbAdapter;
    private String campoDorsal;
    private int idJugador;

    public DatosJugadorFragment() {
        // Required empty public constructor
    }

    public static DatosJugadorFragment newInstance(int id, String nombre, String apellidos, String mote, String posicion,int goles,int amarillas,int rojas, int partidos, int desconvocado, long jugados, long banquillo, long min_desconvocado) {
        //Datos del jugador pasados como argumento
        //Nos ahorramos la consulta
        DatosJugadorFragment fragment = new DatosJugadorFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_ID,id);
        args.putString(ARG_NOMBRE, nombre);
        args.putString(ARG_APELLIDOS, apellidos);
        args.putString(ARG_MOTE,mote);
        args.putString(ARG_POSICION,posicion);
        args.putInt(ARG_GOLES,goles);
        args.putInt(ARG_AMARILLAS,amarillas);
        args.putInt(ARG_ROJAS,rojas);
        args.putInt(ARG_PARTIDOS,partidos);
        args.putInt(ARG_DESCONVOCADO,desconvocado);
        args.putLong(ARG_JUGADOS,jugados);
        args.putLong(ARG_BANQUILLO,banquillo);
        args.putLong(ARG_MINDESCONVODADO,min_desconvocado);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            midJugador = getArguments().getInt(ARG_ID);
            mnombreJugador = getArguments().getString(ARG_NOMBRE);
            mapellidosJugador = getArguments().getString(ARG_APELLIDOS);
            mmoteJugador = getArguments().getString(ARG_MOTE);
            mposicionJugador = getArguments().getString(ARG_POSICION);
            mgolesJugador = getArguments().getInt(ARG_GOLES);
            mamarillasJugador = getArguments().getInt(ARG_AMARILLAS);
            mrojasJugador = getArguments().getInt(ARG_ROJAS);
            mpartidosJugador = getArguments().getInt(ARG_PARTIDOS);
            mdesconvocadoJugador = getArguments().getInt(ARG_DESCONVOCADO);
            mjugadosJugador = getArguments().getLong(ARG_JUGADOS);
            mbanquilloJugador = getArguments().getLong(ARG_BANQUILLO);
            mmindesconvocadoJugador = getArguments().getLong(ARG_MINDESCONVODADO);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_datos_jugador, container, false);

        //Preparamos layout
        mNombre = (TextView)root.findViewById(R.id.tvDatosNombre);
        mApellidos = (TextView)root.findViewById(R.id.tvDatosApellidos);
        mMote = (TextView)root.findViewById(R.id.tvDatosMote);
        mPartidos = (TextView)root.findViewById(R.id.tvDatosPartidosJugados);
        mDesconvocado = (TextView)root.findViewById(R.id.tvDatosPartidosDesconvocado);
        mMediaJugado = (TextView)root.findViewById(R.id.tvDatosMediaJugados);
        mMediaBanquillo =(TextView)root.findViewById(R.id.tvDatosMediaBanquillo);
        mMediaDesconvocado = (TextView)root.findViewById(R.id.tvDatosMediaDesconvocado);
        mDorsal = (TextView)root.findViewById(R.id.tvDatosDorsal);
        mGoles = (TextView) root.findViewById(R.id.tvGoles);
        mAmarillas = (TextView)root.findViewById(R.id.tvAmarillas);
        mRojas = (TextView)root.findViewById(R.id.tvRojas);
        mMinutosJugados = (TextView)root.findViewById(R.id.tvDatosMinutosJugados);
        mMinutosBanquillo = (TextView)root.findViewById(R.id.tvDatosMinutosBanquillo);
        mMinutosDesconvocado = (TextView)root.findViewById(R.id.tvMinutosDesconvocado);

        //Rellenamos los datos
        mNombre.setText(mnombreJugador);
        mApellidos.setText(mapellidosJugador);
        mMote.setText(mmoteJugador);
        mDorsal.setText(mposicionJugador);
        mGoles.setText(String.valueOf(mgolesJugador));
        mAmarillas.setText(String.valueOf(mamarillasJugador));
        mRojas.setText(String.valueOf(mrojasJugador));
        mPartidos.setText(String.valueOf(mpartidosJugador));
        mDesconvocado.setText(String.valueOf(mdesconvocadoJugador));
        mMinutosJugados.setText(String.valueOf(mjugadosJugador));
        mMinutosBanquillo.setText(String.valueOf(mbanquilloJugador));
        mMinutosDesconvocado.setText(String.valueOf(mmindesconvocadoJugador));
        DecimalFormat df = new DecimalFormat("0.00");

        if( mjugadosJugador + mbanquilloJugador + mmindesconvocadoJugador == 0){

           mMediaJugado.setText("-");
            mMediaBanquillo.setText("-");
            mMediaDesconvocado.setText("-");

        }

       else {

            mMediaJugado.setText((String.valueOf(df.format((double) mjugadosJugador / (mjugadosJugador + mbanquilloJugador + mmindesconvocadoJugador) * 100))) + " %");
            mMediaBanquillo.setText((String.valueOf(df.format((double) mbanquilloJugador / (mjugadosJugador + mbanquilloJugador + mmindesconvocadoJugador) * 100))) + " %");
            mMediaDesconvocado.setText((String.valueOf(df.format((double) mmindesconvocadoJugador / (mjugadosJugador + mbanquilloJugador + mmindesconvocadoJugador) * 100))) + " %");
        }


        return root;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_delete){
            new EliminarJugadorTask().execute();
        }
        return super.onOptionsItemSelected(item);
    }

    //Eliminamos un jugador
    private class EliminarJugadorTask extends AsyncTask<Void, Void, Integer> {

        @Override
        protected Integer doInBackground(Void... voids) {
            //Abre BBDD
            dbAdapter = new DBAdapter(getActivity().getApplicationContext());
            dbAdapter.open();

            return dbAdapter.deleteJugador(midJugador);

        }

        @Override
        protected void onPostExecute(Integer integer) {
            getActivity().setResult(Activity.RESULT_OK);
            getActivity().finish();
            Intent intent = new Intent(getActivity(),MainActivity.class);
            intent.putExtra("id",MainActivity.id_equipo);
            intent.putExtra("nombre",MainActivity.nombre);
            intent.putExtra("escudo",MainActivity.escudo);
            intent.putExtra("categoria",MainActivity.categoria);
            startActivity(intent);

        }

    }

}
package com.imovil.casta.tiempodejuegofinal.vista.equipo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.imovil.casta.tiempodejuegofinal.R;

public class CrearEquipoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_equipo);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.crearEquipo);

       CrearEquipoFragment nuevoEquipoFragment = (CrearEquipoFragment)
                getSupportFragmentManager().findFragmentById(R.id.nuevo_equipo_container);
        if (nuevoEquipoFragment == null) {
            nuevoEquipoFragment = CrearEquipoFragment.newInstance();
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.nuevo_equipo_container, nuevoEquipoFragment)
                    .commit();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}

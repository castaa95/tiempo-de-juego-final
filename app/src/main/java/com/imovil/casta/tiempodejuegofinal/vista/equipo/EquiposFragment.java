package com.imovil.casta.tiempodejuegofinal.vista.equipo;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.imovil.casta.tiempodejuegofinal.MainActivity;
import com.imovil.casta.tiempodejuegofinal.R;
import com.imovil.casta.tiempodejuegofinal.adaptadores.AdaptadorEquipos;
import com.imovil.casta.tiempodejuegofinal.basedatos.DBAdapter;
import com.imovil.casta.tiempodejuegofinal.modelo.Equipo;
import com.imovil.casta.tiempodejuegofinal.vista.plantilla.PlantillaFragment;

import java.util.ArrayList;
import java.util.List;

/*
Clase que muestra el listado de equipos
Permite crear nuevos equipos
Pulsando sobreel equipo nos lleva a sus detalles
 */

public class EquiposFragment extends Fragment {

    private ListView listaEquipos;
    private FloatingActionButton nuevoEquipo;
    private TextView sinEquipos;

    List<Integer> id = new ArrayList<Integer>();
    List<String> equipos = new ArrayList<String>();
    List<Bitmap> escudos = new ArrayList<Bitmap>();
    List<String> categoria = new ArrayList<String>();

    DBAdapter dbAdapter;
    private AdaptadorEquipos adapter = null;

    public EquiposFragment() {
        // Required empty public constructor
    }

    public static EquiposFragment newInstance() {
        EquiposFragment fragment = new EquiposFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_equipos, container, false);

        //Inicializamos el layout
        listaEquipos = (ListView) root.findViewById(R.id.lvEquipos);
        nuevoEquipo = (FloatingActionButton)root.findViewById(R.id.fab);
        sinEquipos = (TextView)root.findViewById(R.id.noEquipos);

        //Rellena el listView
        cargaLista();

        //Crea el adaptador
        adapter = new AdaptadorEquipos(getContext(),crearListaEquipos(id,equipos,escudos,categoria));

        listaEquipos.setAdapter(adapter);

        //Manejador de seleccion de equipo
        listaEquipos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Cogemos datos del equipo seleccionado
                Equipo equipoSeleccionado = (Equipo) adapter.getItem(position);
                //Vamos a nueva actividad
                Intent intent = new Intent(getActivity(), MainActivity.class);
                //Pasamos esos datos a la nueva actividad
                //intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                intent.putExtra("id",equipoSeleccionado.getId());
                intent.putExtra("nombre",equipoSeleccionado.getNombre());
                intent.putExtra("escudo",scaleDownBitmap(equipoSeleccionado.getEscudo(),100,getContext()));
                intent.putExtra("categoria",equipoSeleccionado.getCategoria());
                getActivity().finish();
                getActivity().overridePendingTransition(0,0);
                startActivity(intent);

            }
        });


        //Manejador del fab
        nuevoEquipo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CrearEquipoActivity.class);
                startActivity(intent);
            }
        });

        return root;
    }

    //Crea lista de equipo para el adaptador
    private List<Equipo> crearListaEquipos(List<Integer> id, List<String> equipos, List<Bitmap> escudos, List<String>categoria) {

        if (equipos.size() != escudos.size()) {
            throw new IllegalStateException();
        }

        ArrayList<Equipo> listaEquipos = new ArrayList<Equipo>(equipos.size());
        for (int i = 0; i < equipos.size(); i++) {
            listaEquipos.add(new Equipo(id.get(i),equipos.get(i), escudos.get(i), categoria.get(i)));
        }
        return listaEquipos;
    }

    //Carga la lista mediante una consulta
    private void cargaLista(){

        dbAdapter = new DBAdapter(getContext());
        dbAdapter.open();

        id = new ArrayList<Integer>();
        equipos = new ArrayList<String>();
        escudos = new ArrayList<Bitmap>();
        categoria = new ArrayList<String>();

        Cursor c = null;
        c  = dbAdapter.getNombreEscudo();

        //Sacamos los campos
        if (c.moveToFirst()) {
            //Recorremos el cursor hasta que no haya más registros
            do {
                id.add(c.getInt(0));
                equipos.add(c.getString(1));
                byte[] escudo = (c.getBlob(2));
                categoria.add(c.getString(3));
                //Pasamos el escudo a Bitmap
                Bitmap bm = BitmapFactory.decodeByteArray(escudo, 0 ,escudo.length);
                escudos.add(bm);
            } while (c.moveToNext());
        }
        if(equipos.size()== 0){
            sinEquipos.setVisibility(TextView.VISIBLE);
        }
        else{
            sinEquipos.setVisibility(TextView.INVISIBLE);
        }
    }

    public static Bitmap scaleDownBitmap(Bitmap photo, int newHeight, Context context) {

        final float densityMultiplier = context.getResources().getDisplayMetrics().density;

        int h= (int) (newHeight*densityMultiplier);
        int w= (int) (h * photo.getWidth()/((double) photo.getHeight()));

        photo=Bitmap.createScaledBitmap(photo, w, h, true);

        return photo;
    }

}

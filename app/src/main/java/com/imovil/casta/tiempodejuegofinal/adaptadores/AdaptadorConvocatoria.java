package com.imovil.casta.tiempodejuegofinal.adaptadores;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.imovil.casta.tiempodejuegofinal.R;
import com.imovil.casta.tiempodejuegofinal.modelo.Jugador;

import java.util.List;

/**
 * Created by casta on 22/5/17.
 */

public class AdaptadorConvocatoria extends BaseAdapter{

    static class ViewHolder {

        public TextView moteJugador;
        public TextView dorsalJugador;
        public LinearLayout layout;

    }

    private final List<Jugador> listaJugadores;
    public LayoutInflater inflater;


    //Constructor del adatador
    public AdaptadorConvocatoria(Context context, List<Jugador> jugadores) {

        if (context == null) {
            throw new IllegalArgumentException();
        }

        this.listaJugadores=jugadores;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listaJugadores.size();
    }

    @Override
    public Object getItem(int position) {
        return listaJugadores.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        AdaptadorConvocatoria.ViewHolder viewHolder;
        if (rowView == null) {

            rowView = inflater.inflate(R.layout.lista_convocados,parent,false);
            viewHolder = new AdaptadorConvocatoria.ViewHolder();
            viewHolder.moteJugador = (TextView) rowView.findViewById(R.id.mote);
            viewHolder.dorsalJugador = (TextView) rowView.findViewById(R.id.numero);
            viewHolder.layout = (LinearLayout) rowView.findViewById(R.id.lCampo);
            rowView.setTag(viewHolder);

        } else {
            viewHolder = (AdaptadorConvocatoria.ViewHolder) rowView.getTag();
        }

        Jugador jugador = (Jugador) getItem(position);
        viewHolder.moteJugador.setText(jugador.getMote());
        viewHolder.dorsalJugador.setText(jugador.getPosicion());
        Drawable d = new BitmapDrawable(viewHolder.dorsalJugador.getResources(), jugador.getFotografia());
        viewHolder.layout.setBackground(d);

        return rowView;

    }
}

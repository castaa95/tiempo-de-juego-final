package com.imovil.casta.tiempodejuegofinal.vista.partido;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.imovil.casta.tiempodejuegofinal.MainActivity;
import com.imovil.casta.tiempodejuegofinal.R;
import com.imovil.casta.tiempodejuegofinal.adaptadores.TablaEstadisticas;
import com.imovil.casta.tiempodejuegofinal.basedatos.DBAdapter;
import com.imovil.casta.tiempodejuegofinal.modelo.Jugador;
import com.imovil.casta.tiempodejuegofinal.modelo.Jugador_Partido;
import com.imovil.casta.tiempodejuegofinal.vista.plantilla.DatosJugadorFragment;

import java.util.ArrayList;
import java.util.List;

import static com.github.mikephil.charting.utils.ColorTemplate.COLORFUL_COLORS;


public class DatosPartidoFragment extends Fragment {

    private static final String ARG_EQUIPO = "equipo";
    private static final String ARG_PARTIDO = "partido";

    public static String grafica = "";
    private String tagGrafica = "grafica";

    private int mequipo;
    private int mpartido;

    List<Jugador_Partido> listaDetalles = new ArrayList<Jugador_Partido>();
    List<Jugador> listaJugadores = new ArrayList<Jugador>();
    List<String> listaNombres = new ArrayList<String>();
    TablaEstadisticas tabla;

    private TextView tvTitulo;
    private DBAdapter dbAdapter;

    public DatosPartidoFragment() {
        // Required empty public constructor
    }


    public static DatosPartidoFragment newInstance(int equipo, int partido) {
        DatosPartidoFragment fragment = new DatosPartidoFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_EQUIPO, equipo);
        args.putInt(ARG_PARTIDO, partido);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if( savedInstanceState != null){
            grafica = savedInstanceState.getString(tagGrafica);
        }
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mequipo = getArguments().getInt(ARG_EQUIPO);
            mpartido = getArguments().getInt(ARG_PARTIDO);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_datos_partido, container, false);

        tvTitulo = (TextView)root.findViewById(R.id.tvTitulo);
        final ScrollView sv = (ScrollView)root.findViewById(R.id.scrollvertical);
tvTitulo.setText(getResources().getString(R.string.datos_generales));
        //Cogemos parametro
        Bundle args = getArguments();
        int equipo = args.getInt(ARG_EQUIPO, 0);
        int partido = args.getInt(ARG_PARTIDO,0);

        //Creamos la tabla
        tabla = new TablaEstadisticas(getActivity(),(TableLayout)root.findViewById(R.id.tablaPartido));
        //Agregamos cabecera
        tabla.agregarCabecera(R.array.cabecera_datosPartido);

        //Rellenamos la tabla
        dbAdapter = new DBAdapter(getContext());
        dbAdapter.open();
        Cursor c = null;
        //Primero cogemos el listado de jugadores de ese equipo
        c  = dbAdapter.getJugadorLista(equipo);
        if (c.moveToFirst()) {
            //Recorremos el cursor hasta que no haya más registros
            do {

                byte[] foto = c.getBlob(9);
                Bitmap bm = BitmapFactory.decodeByteArray(foto,0,foto.length);
                Jugador jug = new Jugador(c.getInt(0),c.getInt(1),c.getString(2),c.getString(3),c.getString(4),
                        c.getString(5),c.getInt(6),c.getInt(7),c.getInt(8),bm,c.getInt(10),c.getInt(11),c.getInt(12),c.getInt(13),c.getInt(14));
                listaJugadores.add(jug);

                //String nombreJugador = c.getString(4);

                //int id_jugador = c.getInt(0);
                //Cursor k = dbAdapter.getDetallespartido(id_jugador, partido);
                //if (k.moveToFirst()) {
                    //Jugador_Partido det = new Jugador_Partido(id_jugador, k.getInt(1),k.getInt(2),k.getInt(3),k.getInt(4), k.getInt(5) != 0, k.getInt(6), k.getInt(7), k.getInt(8));
                    //listaNombres.add(nombreJugador);
                    //listaDetalles.add(det);
                //}
            } while (c.moveToNext());
        }

        Cursor k = dbAdapter.getDetallesP(partido);
        if(k.moveToFirst()){
            do{
               Jugador_Partido det = new Jugador_Partido(k.getInt(0), k.getInt(1),k.getString(2),k.getInt(3),k.getInt(4),k.getInt(5), k.getInt(6) != 0, k.getInt(7), k.getInt(8), k.getInt(9));
                listaDetalles.add(det);
            } while (k.moveToNext());
        }

        final List<String> jugadores = new ArrayList<String>();
        List<BarEntry>entryJugados = new ArrayList<BarEntry>();
        List<BarEntry>entryBanquillo = new ArrayList<BarEntry>();
        List<BarEntry>entryConvcoados = new ArrayList<BarEntry>();
        List<PieEntry>entryGoles = new ArrayList<PieEntry>();
        ArrayList<String>goleadores = new ArrayList<String>();

        //Rellenamos la tabla
        for (int i = 0; i<listaDetalles.size(); i++){
            ArrayList<String> elementos = new ArrayList<String>();
            jugadores.add(listaDetalles.get(i).getNombre());
            elementos.add(listaDetalles.get(i).getNombre());
            if(String.valueOf(listaDetalles.get(i).isConvocado()).equals("true")){
                String valor = getString(R.string.si);
                elementos.add(valor);
            }
            else{
                elementos.add("No");
            }
            elementos.add(String.valueOf(listaDetalles.get(i).getGoles()));
            elementos.add(String.valueOf(listaDetalles.get(i).getAmarillas()));
            elementos.add(String.valueOf(listaDetalles.get(i).getRojas()));
            elementos.add(String.valueOf(listaDetalles.get(i).getMinutos_jugados()));
            elementos.add(String.valueOf(listaDetalles.get(i).getMinutos_banquillo()));
            elementos.add(String.valueOf(listaDetalles.get(i).getMinutos_desconvocado()));

            float suma = listaDetalles.get(i).getMinutos_jugados() + listaDetalles.get(i).getMinutos_banquillo() + listaDetalles.get(i).getMinutos_desconvocado();
            entryJugados.add(new BarEntry( i , (float)listaDetalles.get(i).getMinutos_jugados() / suma *100));
            entryBanquillo.add(new BarEntry( i , (float)listaDetalles.get(i).getMinutos_banquillo() / suma *100));
            entryConvcoados.add(new BarEntry( i , (float)listaDetalles.get(i).getMinutos_desconvocado() / suma *100));

            if (listaDetalles.get(i).getGoles() >0){
                entryGoles.add(new PieEntry(listaDetalles.get(i).getGoles(),listaDetalles.get(i).getNombre()));
            }



            tabla.agregarFilaTabla(elementos);

        }

        //Grafica Goles
        final PieChart charGoles = (PieChart)root.findViewById(R.id.chartGolesTarjetas);

        PieDataSet set = new PieDataSet(entryGoles, getResources().getString(R.string.goleadores));
        PieData dataa = new PieData(set);
        set.setColors(COLORFUL_COLORS);
        charGoles.setDescription(null);
        charGoles.setData(dataa);
        charGoles.invalidate(); // refresh

        //Grafica de tiempo
        final BarChart charGrafica = (BarChart)root.findViewById(R.id.chartMinPartido);

        //Preparamos el dataset
        XAxis xAxis = charGrafica.getXAxis();
        xAxis.setGranularity(1f);
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return String.valueOf(jugadores.get((int) value));
            }
        });
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        BarDataSet setJugados = new BarDataSet(entryJugados, getResources().getString(R.string.media_jugado));
        setJugados.setColor(Color.BLUE);
        BarDataSet setBanquillo = new BarDataSet(entryBanquillo, getResources().getString(R.string.media_banquillo));
        setBanquillo.setColor(Color.RED);
        BarDataSet setDesconvocado = new BarDataSet(entryConvcoados,getResources().getString(R.string.minutosDesconvocado));
        setDesconvocado.setColor(Color.YELLOW);

        final float groupSpace = 0.07f;
        float barSpace = 0.00f;
        float barWidth = 0.31f;

        ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
        dataSets.add(setJugados);
        dataSets.add(setBanquillo);
        dataSets.add(setDesconvocado);

        BarData data = new BarData(dataSets);

        data.setBarWidth(barWidth);

        charGrafica.setData(data);
        charGrafica.groupBars(-0.5f,groupSpace,barSpace);
        charGrafica.invalidate();
        charGrafica.setDescription(null);

        charGrafica.setDrawBarShadow(false);
        charGrafica.setDrawValueAboveBar(true);
        charGrafica.setMaxVisibleValueCount(50);
        charGrafica.setPinchZoom(false);
        charGrafica.setDrawGridBackground(false);

        //Eventos botones
        final RelativeLayout layoutTabla = (RelativeLayout)root.findViewById(R.id.lTabla);
        final RelativeLayout layoutMins = (RelativeLayout)root.findViewById(R.id.lMins);
        final RelativeLayout layoutGoles = (RelativeLayout)root.findViewById(R.id.lGols);
        Button botonGeneral = (Button)root.findViewById(R.id.bTabla);
        Button botonMinutos = (Button)root.findViewById(R.id.bMins);
        Button botonGoles = (Button)root.findViewById(R.id.bGoles);

        if (grafica.equals("minutos")){
            tvTitulo.setText(getResources().getString(R.string.datos_tiempo));
            layoutTabla.setVisibility(View.INVISIBLE);
            layoutMins.setVisibility(View.VISIBLE);
            layoutGoles.setVisibility(View.INVISIBLE);
        }

        if (grafica.equals("goles")){
            tvTitulo.setText(getResources().getString(R.string.goleadores));
            layoutTabla.setVisibility(View.INVISIBLE);
            layoutMins.setVisibility(View.INVISIBLE);
            layoutGoles.setVisibility(View.VISIBLE);
        }

        else {
            tvTitulo.setText(getResources().getString(R.string.datos_generales));
        }


        botonMinutos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                grafica = "minutos";
                layoutTabla.setVisibility(View.INVISIBLE);
                layoutMins.setVisibility(View.VISIBLE);
                layoutGoles.setVisibility(View.INVISIBLE);
                tvTitulo.setText(R.string.datos_tiempo);
                //charGrafica.setVisibility(View.VISIBLE);
                //charGoles.setVisibility(View.INVISIBLE);
                //sv.setVisibility(View.INVISIBLE);
            }
        });

        botonGeneral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                grafica = "grafica";
                tvTitulo.setText(R.string.datos_generales);
                layoutTabla.setVisibility(View.VISIBLE);
                layoutMins.setVisibility(View.INVISIBLE);
                layoutGoles.setVisibility(View.INVISIBLE);
            }
        });

        botonGoles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                grafica = "goles";
tvTitulo.setText(R.string.goleadores);
                layoutTabla.setVisibility(View.INVISIBLE);
                layoutMins.setVisibility(View.INVISIBLE);
                layoutGoles.setVisibility(View.VISIBLE);
            }
        });

        return root;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_delete){
            new DatosPartidoFragment.EliminarPartidoTask(listaJugadores,listaDetalles).execute();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(tagGrafica,grafica);
        super.onSaveInstanceState(outState);
    }

    //Eliminamos un partido
    private class EliminarPartidoTask extends AsyncTask<Void, Void, Integer> {

        List<Jugador> jugadores;
        List<Jugador_Partido> detalles;

        public EliminarPartidoTask(List<Jugador> jugadores,List<Jugador_Partido>detalles){
            this.jugadores = jugadores;
            this.detalles = detalles;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            //Abre BBDD
            dbAdapter = new DBAdapter(getActivity().getApplicationContext());
            dbAdapter.open();

            for (int i = 0; i<listaDetalles.size(); i++){

                for(int j = 0; j<listaJugadores.size(); j ++){

                    if(listaDetalles.get(i).getId_jugador() == listaJugadores.get(j).getId()){

                        if(listaDetalles.get(i).isConvocado()){
                            dbAdapter.actualizarJugador(listaJugadores.get(j).getId(), listaJugadores.get(j).getPartidos_jugados() - 1 , listaJugadores.get(j).getPartidos_desconvocado(),
                                    listaJugadores.get(j).getGoles() - listaDetalles.get(i).getGoles(), listaJugadores.get(j).getAmarillas() - listaDetalles.get(i).getAmarillas(),
                                    listaJugadores.get(j).getRojas() - listaDetalles.get(i).getRojas(), listaJugadores.get(j).getMinutos_jugados_totales() - listaDetalles.get(i).getMinutos_jugados(),
                                    listaJugadores.get(j).getMinutos_banquillo_totales() - listaDetalles.get(i).getMinutos_banquillo(), listaJugadores.get(j).getMinutos_desconvocado_totales());

                        }
                        else{
                            dbAdapter.actualizarJugador(listaJugadores.get(j).getId(), listaJugadores.get(j).getPartidos_jugados(), listaJugadores.get(j).getPartidos_desconvocado() - 1,
                                    listaJugadores.get(j).getGoles(), listaJugadores.get(j).getAmarillas(), listaJugadores.get(j).getRojas(), listaJugadores.get(j).getMinutos_jugados_totales(),
                                    listaJugadores.get(j).getMinutos_banquillo_totales(), listaJugadores.get(j).getMinutos_desconvocado_totales() - listaDetalles.get(i).getMinutos_desconvocado());
                        }

                    }

                }


            }

            dbAdapter.deleteDetalles(mpartido);
            return dbAdapter.deletePartido(mpartido);

        }

        @Override
        protected void onPostExecute(Integer integer) {
            getActivity().setResult(Activity.RESULT_OK);
            getActivity().finish();
            /*Intent intent = new Intent(getActivity(),MainActivity.class);
            intent.putExtra("id",MainActivity.id_equipo);
            intent.putExtra("nombre",MainActivity.nombre);
            intent.putExtra("escudo",MainActivity.escudo);
            intent.putExtra("categoria",MainActivity.categoria);
            startActivity(intent);*/

        }

    }

}

package com.imovil.casta.tiempodejuegofinal.vista.estadisticas;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.imovil.casta.tiempodejuegofinal.R;
import com.imovil.casta.tiempodejuegofinal.adaptadores.TablaEstadisticas;
import com.imovil.casta.tiempodejuegofinal.basedatos.DBAdapter;
import com.imovil.casta.tiempodejuegofinal.modelo.Jugador;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/*
Muestra las estadisricas globales de los jugadores de un equipo
 */

public class EstadisticasFragment extends Fragment {

    DBAdapter dbAdapter;
    public static final String ARGS_EQUIPO = "id_equipo";
    private BarChart charMinutos;
    List<Jugador> listaJugadores = new ArrayList<Jugador>();
    TablaEstadisticas tabla;

    public EstadisticasFragment() {
        // Required empty public constructor
    }


    public static EstadisticasFragment newInstance(int id_equipo) {
        EstadisticasFragment fragment = new EstadisticasFragment();
        Bundle args = new Bundle();
        args.putInt(ARGS_EQUIPO, id_equipo);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_estadisticas, container, false);

        //Cogemos parametro
        Bundle args = getArguments();
        int equipo = args.getInt(ARGS_EQUIPO, 0);

        //Creamos la tabla
        tabla = new TablaEstadisticas(getActivity(),(TableLayout)root.findViewById(R.id.tabla));
        //Agregamos cabecera
        tabla.agregarCabecera(R.array.cabecera_tabla);

       new EstadisticasLoadTask().execute(equipo);

        //Rellenamos la tabla
        for (int i = 0; i<listaJugadores.size(); i++){
            ArrayList<String> elementos = new ArrayList<String>();
            elementos.add(listaJugadores.get(i).getMote());
            elementos.add(String.valueOf(listaJugadores.get(i).getPartidos_jugados()));
            elementos.add(String.valueOf(listaJugadores.get(i).getPartidos_desconvocado()));
            elementos.add(String.valueOf(listaJugadores.get(i).getGoles()));
            elementos.add(String.valueOf(listaJugadores.get(i).getAmarillas()));
            elementos.add(String.valueOf(listaJugadores.get(i).getRojas()));
            elementos.add(String.valueOf(listaJugadores.get(i).getMinutos_jugados_totales()));
            elementos.add(String.valueOf(listaJugadores.get(i).getMinutos_banquillo_totales()));
            elementos.add(String.valueOf(listaJugadores.get(i).getMinutos_desconvocado_totales()));

            if(listaJugadores.get(i).getMinutos_jugados_totales() + listaJugadores.get(i).getMinutos_banquillo_totales() + listaJugadores.get(i).getMinutos_desconvocado_totales() == 0){

                elementos.add("-");
                elementos.add("-");
                elementos.add("-");

            }

            else{

                elementos.add(String.valueOf(((double)listaJugadores.get(i).getMinutos_jugados_totales()/(listaJugadores.get(i).getMinutos_jugados_totales() + listaJugadores.get(i).getMinutos_banquillo_totales() + listaJugadores.get(i).getMinutos_desconvocado_totales())*100)+ " %"));
                elementos.add(String.valueOf(((double)listaJugadores.get(i).getMinutos_banquillo_totales()/(listaJugadores.get(i).getMinutos_jugados_totales() + listaJugadores.get(i).getMinutos_banquillo_totales()+listaJugadores.get(i).getMinutos_desconvocado_totales())*100))+ " %");
                elementos.add(String.valueOf(((double)listaJugadores.get(i).getMinutos_desconvocado_totales()/(listaJugadores.get(i).getMinutos_jugados_totales() + listaJugadores.get(i).getMinutos_banquillo_totales()+listaJugadores.get(i).getMinutos_desconvocado_totales())*100))+ " %");


            }

            tabla.agregarFilaTabla(elementos);

        }

        //charMinutos = (BarChart)root.findViewById(R.id.chart_minutos);

        return root;
    }

    private class EstadisticasLoadTask extends AsyncTask<Integer,Void,Cursor> {

        @Override
        protected Cursor doInBackground(Integer... params){
            dbAdapter = new DBAdapter(getContext());
            dbAdapter.open();
            Cursor c = null;
            c  = dbAdapter.getJugadorLista(params[0]);

            return c;

        }

        @Override
        protected void onPostExecute(Cursor c){
            if (c.moveToFirst()) {
                //Recorremos el cursor hasta que no haya más registros
                do {
                    byte[] foto = c.getBlob(9);
                    Bitmap bm = BitmapFactory.decodeByteArray(foto,0,foto.length);
                    Jugador jug = new Jugador(c.getInt(0),c.getInt(1),c.getString(2),c.getString(3),c.getString(4),
                            c.getString(5),c.getInt(6),c.getInt(7),c.getInt(8),bm,c.getInt(10),c.getInt(11),c.getInt(12),c.getInt(13),c.getInt(14));
                    listaJugadores.add(jug);
                } while (c.moveToNext());
            }


            //Agregamos elementos
            for (int i = 0; i<listaJugadores.size(); i++){
                ArrayList<String> elementos = new ArrayList<String>();
                //jugadores.add(listaJugadores.get(i).getMote());
                DecimalFormat df = new DecimalFormat("0.00");
                elementos.add(listaJugadores.get(i).getMote());
                elementos.add(String.valueOf(listaJugadores.get(i).getPartidos_jugados()));
                elementos.add(String.valueOf(listaJugadores.get(i).getPartidos_desconvocado()));
                elementos.add(String.valueOf(listaJugadores.get(i).getGoles()));
                elementos.add(String.valueOf(listaJugadores.get(i).getAmarillas()));
                elementos.add(String.valueOf(listaJugadores.get(i).getRojas()));
                elementos.add(String.valueOf(listaJugadores.get(i).getMinutos_jugados_totales()));
                elementos.add(String.valueOf(listaJugadores.get(i).getMinutos_banquillo_totales()));
                elementos.add(String.valueOf(listaJugadores.get(i).getMinutos_desconvocado_totales()));

                if(listaJugadores.get(i).getMinutos_jugados_totales() + listaJugadores.get(i).getMinutos_banquillo_totales() + listaJugadores.get(i).getMinutos_desconvocado_totales() == 0){

                    elementos.add("-");
                    elementos.add("-");
                    elementos.add("-");

                }

                else{

                    elementos.add(String.valueOf(df.format((double)listaJugadores.get(i).getMinutos_jugados_totales()/(listaJugadores.get(i).getMinutos_jugados_totales() + listaJugadores.get(i).getMinutos_banquillo_totales() + listaJugadores.get(i).getMinutos_desconvocado_totales())*100)+ " %"));
                    elementos.add(String.valueOf(df.format((double)listaJugadores.get(i).getMinutos_banquillo_totales()/(listaJugadores.get(i).getMinutos_jugados_totales() + listaJugadores.get(i).getMinutos_banquillo_totales()+listaJugadores.get(i).getMinutos_desconvocado_totales())*100))+ " %");
                    elementos.add(String.valueOf(df.format((double)listaJugadores.get(i).getMinutos_desconvocado_totales()/(listaJugadores.get(i).getMinutos_jugados_totales() + listaJugadores.get(i).getMinutos_banquillo_totales()+listaJugadores.get(i).getMinutos_desconvocado_totales())*100))+ " %");


                }

                tabla.agregarFilaTabla(elementos);

            }

        }

    }


}

package com.imovil.casta.tiempodejuegofinal.modelo;


import android.graphics.Bitmap;



/**
 * Created by casta on 23/3/17.
 */

public class Jugador {

    //Atributos
    private int id;
    private int id_equipo;
    private String nombre;
    private String apellidos;
    private String mote;
    private String posicion;
    private int goles;
    private int amarillas;
    private  int rojas;
    private Bitmap fotografia;
    private int partidos_jugados;
    private int partidos_desconvocado;
    private long minutos_jugados_totales;
    private long minutos_banquillo_totales;
    private long minutos_desconvocado_totales;
    private long temp_tiempoC;
    private long temp_tiempoB;
    private long temp_tiempoD;
    private int temp_golesPartido;
    private int temp_amarillasPartido;
    private int temp_rojasPartido;

    //Constructor
    public Jugador(int id, int id_equipo, String nombre, String apellidos, String mote, String posicion, int goles, int amarillas, int rojas, Bitmap fotografia, int partidos_jugados,
                   int partidos_desconvocado, long minutos_jugados_totales, long minutos_banquillo_totales, long minutos_desconvocado_totales){

        this.id = id;
        this.id_equipo = id_equipo;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.mote = mote;
        this.posicion = posicion;
        this.goles = goles;
        this.amarillas = amarillas;
        this.rojas = rojas;
        this.fotografia = fotografia;
        this.partidos_jugados = partidos_jugados;
        this.partidos_desconvocado = partidos_desconvocado;
        this.minutos_jugados_totales = minutos_jugados_totales;
        this.minutos_banquillo_totales = minutos_banquillo_totales;
        this.minutos_desconvocado_totales = minutos_desconvocado_totales;
        this.temp_tiempoB = 0;
        this.temp_tiempoC = 0;
        this.temp_tiempoD = 0;
        this.temp_golesPartido =0;
        this.temp_amarillasPartido =0;
        this.temp_rojasPartido =0;
    }


    //Getters y Setter

    public int getId() {
        return id;
    }

    public int getId_equipo() {
        return id_equipo;
    }

    public String getMote() {
        return mote;
    }

    public int getGoles() {
        return goles;
    }

    public void setGoles(int goles) {
        this.goles = goles;
    }

    public void setMote(String mote) {
        this.mote = mote;
    }

    public int getPartidos_jugados() {
        return partidos_jugados;
    }

    public void setPartidos_jugados(int partidos_jugados) {
        this.partidos_jugados = partidos_jugados;
    }

    public int getPartidos_desconvocado() {
        return partidos_desconvocado;
    }

    public void setPartidos_desconvocado(int partidos_desconvocado) {
        this.partidos_desconvocado = partidos_desconvocado;
    }

    public long getMinutos_jugados_totales() {
        return minutos_jugados_totales;
    }

    public void setMinutos_jugados_totales(long minutos_jugados_totales) {
        this.minutos_jugados_totales = minutos_jugados_totales;
    }

    public long getMinutos_banquillo_totales() {
        return minutos_banquillo_totales;
    }

    public void setMinutos_banquillo_totales(long minutos_banquillo_totales) {
        this.minutos_banquillo_totales = minutos_banquillo_totales;
    }

    public long getTemp_tiempoC() {
        return temp_tiempoC;
    }

    public void setTemp_tiempoC(long temp_tiempoC) {
        this.temp_tiempoC = temp_tiempoC;
    }

    public long getTemp_tiempoB() {
        return temp_tiempoB;
    }

    public void setTemp_tiempoB(long temp_tiempoB) {
        this.temp_tiempoB = temp_tiempoB;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public int getAmarillas() {
        return amarillas;
    }

    public void setAmarillas(int amarillas) {
        this.amarillas = amarillas;
    }

    public int getRojas() {
        return rojas;
    }

    public void setRojas(int rojas) {
        this.rojas = rojas;
    }

    public int getTemp_amarillasPartido() {
        return temp_amarillasPartido;
    }

    public void setTemp_amarillasPartido(int temp_amarillasPartido) {
        this.temp_amarillasPartido = temp_amarillasPartido;
    }

    public long getMinutos_desconvocado_totales() {
        return minutos_desconvocado_totales;
    }

    public void setMinutos_desconvocado_totales(long minutos_desconvocado_totales) {
        this.minutos_desconvocado_totales = minutos_desconvocado_totales;
    }

    public long getTemp_tiempoD() {
        return temp_tiempoD;
    }

    public void setTemp_tiempoD(long temp_tiempoD) {
        this.temp_tiempoD = temp_tiempoD;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getPosicion() {
        return posicion;
    }

    public int getTemp_rojasPartido() {
        return temp_rojasPartido;
    }

    public void setTemp_rojasPartido(int temp_rojasPartido) {
        this.temp_rojasPartido = temp_rojasPartido;
    }

    public int getTemp_golesPartido() {
        return temp_golesPartido;
    }

    public void setTemp_golesPartido(int temp_golesPartido) {
        this.temp_golesPartido = temp_golesPartido;
    }

    public void setPosicion(String posicion) {
        this.posicion = posicion;
    }

    public Bitmap getFotografia() {
        return fotografia;
    }

    public void setFotografia(Bitmap fotografia) {
        this.fotografia = fotografia;
    }

}


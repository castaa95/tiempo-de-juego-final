package com.imovil.casta.tiempodejuegofinal.adaptadores;

import android.content.Context;

import android.graphics.Bitmap;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseBooleanArray;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.imovil.casta.tiempodejuegofinal.R;
import com.imovil.casta.tiempodejuegofinal.modelo.Jugador;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by casta on 22/6/17.
 */

public class AdaptadorDatosPartido extends RecyclerView.Adapter<AdaptadorDatosPartido.ViewHolder> {

    private static List<Jugador> listaJugadores;
    public LayoutInflater inflater = null;
    static int amarillas = 0;
    private SparseBooleanArray mCheckedItems = new SparseBooleanArray();

    static final String [] tarjetas = {"0","1","2"};

    public static List<String> mEditTextJugado = new ArrayList<>();
    public static List<String> mEditTextBanquillo = new ArrayList<>();
    public static List<String> mEditTextGoles = new ArrayList<>();
    public static List<Boolean> mRoja = new ArrayList<>();
    public static List<Boolean> mConvocado = new ArrayList<>();

    public static List<Boolean> m1 = new ArrayList<>();
    public static List<Boolean> m2 = new ArrayList<>();
    public static List<Boolean> m0 = new ArrayList<>();



    Context context;

    static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nombreJugador;
        public TextView tvamarillas;
        public TextView apellidosJugador;
        public TextView jugadosJ;
        public TextView banquilloJ;
        public TextView golesJ;
        public ImageView imagenJugador;
        public CheckBox convocadoJugador;
        public CheckBox rojaJugador;

        public TextInputEditText jugadosJugador;
        public TextInputEditText banquilloJugador;
        public EditText golesJugador;

        public RadioButton rb1;
        public RadioButton rb2;
        public  RadioButton rb0;


        public ViewHolder(View rowView) {
            super(rowView);

            nombreJugador = (TextView) rowView.findViewById(R.id.tvJugador);
            apellidosJugador = (TextView) rowView.findViewById(R.id.tvApellidos);
            imagenJugador = (ImageView) rowView.findViewById(R.id.ivFotografia);
            convocadoJugador = (CheckBox) rowView.findViewById(R.id.cbConvocado);
            jugadosJ = (TextView)rowView.findViewById(R.id.tvJugado);
            banquilloJ = (TextView)rowView.findViewById(R.id.tvBanquillo);
            golesJ = (TextView)rowView.findViewById(R.id.tvGoles);
            rojaJugador = (CheckBox) rowView.findViewById(R.id.cbRoja);
            tvamarillas = (TextView) rowView.findViewById(R.id.tvAmarillas);
            //amarillasJugador = (Spinner) rowView.findViewById(R.id.spAmarillas);
            jugadosJugador = (TextInputEditText) rowView.findViewById(R.id.etJugado);
            banquilloJugador = (TextInputEditText) rowView.findViewById(R.id.etBanquillo);
            golesJugador = (EditText) rowView.findViewById(R.id.etGoles);

            rb1 = (RadioButton)rowView.findViewById(R.id.rb1);
            rb2 = (RadioButton)rowView.findViewById(R.id.rb2);
            rb0 = (RadioButton) rowView.findViewById(R.id.rb0) ;

            rojaJugador.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (rojaJugador.isChecked()){
                        mRoja.set((int)rojaJugador.getTag(),true);
                    }

                    else mRoja.set((int)rojaJugador.getTag(),false);

                }
            });


            rb0.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(rb0.isChecked()){
                        m0.set((int)rb0.getTag(),true);
                        m2.set((int)rb2.getTag(),false);
                        m1.set((int)rb1.getTag(),false);
                    }

                    else{
                        m0.set((int)rb0.getTag(),false);
                    }
                }
            });

            rb1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(rb1.isChecked()){
                        m1.set((int)rb1.getTag(),true);
                        m2.set((int)rb2.getTag(),false);
                        m0.set((int)rb0.getTag(),false);
                    }

                    else  m1.set((int)rb1.getTag(),false);
                }
            });


            rb2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(rb2.isChecked()){
                        m2.set((int)rb2.getTag(),true);
                        m1.set((int)rb1.getTag(),false);
                        m0.set((int)rb0.getTag(),false);

                    }

                    else  m2.set((int)rb2.getTag(),false);
                }
            });

            convocadoJugador.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();

                    if(convocadoJugador.isChecked() == true){

                        mConvocado.set((int)convocadoJugador.getTag(),true);
                        banquilloJugador.setEnabled(true);
                        jugadosJugador.setEnabled(true);
                        golesJugador.setEnabled(true);
                        tvamarillas.setEnabled(true);
                        jugadosJ.setEnabled(true);
                        banquilloJ.setEnabled(true);
                        golesJ.setEnabled(true);
                        rb1.setEnabled(true);
                        rb0.setEnabled(true);
                        rb2.setEnabled(true);
                        //amarillasJugador.setEnabled(true);
                        rojaJugador.setEnabled(true);


                    }
                    else{

                        mConvocado.set((int)convocadoJugador.getTag(),false);
                        banquilloJugador.setEnabled(false);
                        jugadosJugador.setEnabled(false);
                        golesJugador.setEnabled(false);
                        tvamarillas.setEnabled(false);
                        jugadosJ.setEnabled(false);
                        banquilloJ.setEnabled(false);
                        golesJ.setEnabled(false);
                        rb1.setEnabled(false);
                        rb0.setEnabled(false);
                        rb2.setEnabled(false);
                        //amarillasJugador.setEnabled(false);
                        rojaJugador.setEnabled(false);

                        golesJugador.setText("0");
                        jugadosJugador.setText("0");
                        banquilloJugador.setText("0");

                    }


                }

            });

            jugadosJugador.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if(jugadosJugador.getTag()!= null){
                            mEditTextJugado.set((int)jugadosJugador.getTag(),s.toString());

                        }
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });



            banquilloJugador.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if(banquilloJugador.getTag()!= null){
                        mEditTextBanquillo.set((int)banquilloJugador.getTag(),s.toString());
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });


            golesJugador.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if(golesJugador.getTag()!= null){
                        mEditTextGoles.set((int)golesJugador.getTag(),s.toString());
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

        }



    }


    public AdaptadorDatosPartido(Context context, List<Jugador> jugadores) {

        if (context == null) {
            throw new IllegalArgumentException();
        }

        this.context = context;
        this.listaJugadores = jugadores;

        for (int i =0;i<listaJugadores.size();i++){
            mEditTextGoles.add("0");
            mEditTextJugado.add("0");
            mEditTextBanquillo.add("0");
            mConvocado.add(false);
            mRoja.add(false);
            m0.add(true);
            m1.add(false);
            m2.add(false);
        }

        this.inflater = LayoutInflater.from(context);
    }


    @Override
    public int getItemCount() {
        return listaJugadores.size();
    }

    @Override
    public AdaptadorDatosPartido.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lista_anotarpartido, parent, false);
        return new AdaptadorDatosPartido.ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final AdaptadorDatosPartido.ViewHolder holder, final int position) {

        final Jugador jugador = listaJugadores.get(position);

        holder.nombreJugador.setText(jugador.getNombre());
        holder.apellidosJugador.setText(jugador.getApellidos());
        holder.imagenJugador.setImageBitmap(Bitmap.createScaledBitmap(jugador.getFotografia(), 80, 80, false));

        holder.jugadosJugador.setTag(position);
        holder.jugadosJugador.setText(mEditTextJugado.get(position));
        jugador.setTemp_tiempoC(Long.valueOf(mEditTextJugado.get(position)));

        holder.banquilloJugador.setTag(position);
        holder.banquilloJugador.setText(mEditTextBanquillo.get(position));

        holder.golesJugador.setTag(position);
        holder.golesJugador.setText(mEditTextGoles.get(position));

        holder.rb0.setTag(position);
        if(m0.get(position) == true){
            holder.rb0.setChecked(true);
        }

        holder.rb1.setTag(position);
        if(m1.get(position) == true){
            holder.rb1.setChecked(true);
        }

        holder.rb2.setTag(position);
        if(m2.get(position) == true){
            holder.rb2.setChecked(true);
        }

        //holder.amarillasJugador.setTag(position);
        //holder.amarillasJugador.getItemAtPosition(mAmarillas.get(position));

        holder.convocadoJugador.setTag(position);
        if(mConvocado.get(position) == true){
            holder.convocadoJugador.setChecked(true);
        }
        else holder.convocadoJugador.setChecked(false);

        holder.rojaJugador.setTag(position);
        if(mRoja.get(position)==true){
            holder.rojaJugador.setChecked(true);
        }
        else holder.rojaJugador.setChecked(false);


        if (holder.convocadoJugador.isChecked()){

            holder.banquilloJugador.setEnabled(true);
            holder.jugadosJugador.setEnabled(true);
            holder.golesJugador.setEnabled(true);
            holder.tvamarillas.setEnabled(true);
            holder.jugadosJ.setEnabled(true);
            holder.banquilloJ.setEnabled(true);
            holder.rb1.setEnabled(true);
            holder.rb0.setEnabled(true);
            holder.rb2.setEnabled(true);
            holder.golesJ.setEnabled(true);
            //holder.amarillasJugador.setEnabled(true);
            holder.rojaJugador.setEnabled(true);

        }

        else{

            holder.banquilloJugador.setEnabled(false);
            holder.jugadosJugador.setEnabled(false);
            holder.golesJugador.setEnabled(false);
            holder.tvamarillas.setEnabled(false);
            holder.jugadosJ.setEnabled(false);
            holder.banquilloJ.setEnabled(false);
            holder.rb1.setEnabled(false);
            holder.rb0.setEnabled(false);
            holder.rb2.setEnabled(false);
            holder.golesJ.setEnabled(false);
            //holder.amarillasJugador.setEnabled(false);
            holder.rojaJugador.setEnabled(false);

            holder.golesJugador.setText("0");
            holder.jugadosJugador.setText("0");
            holder.banquilloJugador.setText("0");

        }

    }
        public List<Integer> getSelectedItemPositions() {
            List<Integer> selected = new ArrayList<>();
            for (int i = 0; i < mCheckedItems.size(); i++) {
                final boolean checked = mCheckedItems.valueAt(i);
                if (checked) {
                    selected.add(mCheckedItems.keyAt(i));
                }
            }
            return selected;
        }

        public void restoreSelectedItems(List<Integer> positions){
            for (Integer position : positions) {
                mCheckedItems.put(position, true);
            }
        }

        @Override
         public long getItemId(int position) {
        return 0;
    }
}

package com.imovil.casta.tiempodejuegofinal.vista.partido;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import com.imovil.casta.tiempodejuegofinal.R;

public class DatosPartidoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos_partido);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Cogemos datos de la otra actividad
        int equipo = getIntent().getIntExtra("equipo",0);
        int partido = getIntent().getIntExtra("partido",0);

        //Llamamos al fragmento
        setTitle(getResources().getString(R.string.datos_partido));
        DatosPartidoFragment fragment = (DatosPartidoFragment) getSupportFragmentManager().findFragmentById(R.id.datos_partido_container);
        if (fragment == null) {
            fragment = DatosPartidoFragment.newInstance(equipo,partido);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.datos_partido_container, fragment)
                    .commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_datos_jugador, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}

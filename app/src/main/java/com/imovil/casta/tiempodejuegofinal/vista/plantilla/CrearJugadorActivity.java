package com.imovil.casta.tiempodejuegofinal.vista.plantilla;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.imovil.casta.tiempodejuegofinal.R;

public class CrearJugadorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_jugador);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.tituloCreaJugador);

        CrearJugadorFragment nuevoJugadorFragment = (CrearJugadorFragment)
                getSupportFragmentManager().findFragmentById(R.id.nuevo_jugador_container);
        if (nuevoJugadorFragment == null) {
            nuevoJugadorFragment = CrearJugadorFragment.newInstance();
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.nuevo_jugador_container, nuevoJugadorFragment)
                    .commit();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}

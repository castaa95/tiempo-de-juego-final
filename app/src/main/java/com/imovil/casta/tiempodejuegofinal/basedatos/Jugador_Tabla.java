package com.imovil.casta.tiempodejuegofinal.basedatos;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

/**
 *
 * Clase encargada de gestionar la tabla jugador
 *
 * Created by casta on 28/3/17.
 */

public class Jugador_Tabla {

    //Nombre de la tabla
    private final static String NOMBRE_TABLA = "jugador_tabla";

    //Objeto para acceder a la BBDD
    private SQLiteDatabase sqlDB;

    //Constructor
    public Jugador_Tabla(SQLiteDatabase sqlDB){
        this.sqlDB = sqlDB;
    }

    //Clase interna con los campos de la tabla
    public class ColumnsJugador implements BaseColumns {

        public final static String NOMBRE_TABLA = "jugador_tabla";
        public final static String ID_EQUIPO = "equipo";
        public final static String NOMBRE = "nombre";
        public final static String APELLIDOS = "apellidos";
        public final static String MOTE = "mote";
        public final static String POSICION ="posicion";
        public final static String GOLES = "goles";
        public final static String AMARILLAS = "amarillas";
        public final static String ROJAS ="rojas";
        public final static String FOTOGRAFIA = "fotografia";
        public final static String PARTIDOS_JUGADOS = "partidos";
        public final static String PARTIDOS_DESCONVOCADO = "desconvocado";
        public final static String MINUTOS_JUGADOS_TOTALES = "min_jugados";
        public final static String MINUTOS_BANQUILLO_TOTALES = "min_banquillo";
        public final static String MINUTOS_DESCONVOCADO_TOTALES = "min_desconvocado";

    }

    //Sentencia para crear tabla
    public final static String CREATE_TABLE = "create table if not exists "
            + NOMBRE_TABLA + " ("
            + Jugador_Tabla.ColumnsJugador._ID + " integer primary key autoincrement, "
            + Jugador_Tabla.ColumnsJugador.NOMBRE + " varchar(30) not null, "
            + Jugador_Tabla.ColumnsJugador.APELLIDOS + " varchar(30) not null, "
            + ColumnsJugador.MOTE + " varchar(30) not null, "
            + ColumnsJugador.POSICION + " varchar(30) not null, "
            + ColumnsJugador.GOLES + " integer not null, "
            + ColumnsJugador.AMARILLAS + " integer not null, "
            + ColumnsJugador.ROJAS + " integer not null, "
            + Jugador_Tabla.ColumnsJugador.FOTOGRAFIA + " blob, "
            + ColumnsJugador.PARTIDOS_JUGADOS + " integer not null, "
            + ColumnsJugador.MINUTOS_JUGADOS_TOTALES + " long not null, "
            + ColumnsJugador.PARTIDOS_DESCONVOCADO + " integer not null, "
            + ColumnsJugador.MINUTOS_BANQUILLO_TOTALES + " long not null, "
            + ColumnsJugador.MINUTOS_DESCONVOCADO_TOTALES + " long not null, "
            + ColumnsJugador.ID_EQUIPO + " integer not null, "
            + "FOREIGN KEY(" + ColumnsJugador.ID_EQUIPO+ ") REFERENCES "
            + Equipo_Tabla.ColumnsEquipo.NOMBRE_TABLA + "(" + Equipo_Tabla.ColumnsEquipo._ID + ") )";

    //Borrar tabla
    public static final String DELETE_TABLE =
            "DROP TABLE IF EXISTS " + NOMBRE_TABLA;


    //Añadir jugador
    public boolean crearJugador(String nombreJugador, String apellidos, String mote, String posicion, byte[] fotografia, int id_equipo){
        ContentValues values = new ContentValues();
        values.put(ColumnsJugador.NOMBRE, nombreJugador);
        values.put(ColumnsJugador.APELLIDOS,apellidos);
        values.put(ColumnsJugador.MOTE,mote);
        values.put(ColumnsJugador.POSICION, posicion);
        values.put(ColumnsJugador.GOLES, 0);
        values.put(ColumnsJugador.AMARILLAS,0);
        values.put(ColumnsJugador.ROJAS,0);
        values.put(ColumnsJugador.FOTOGRAFIA, fotografia);
        values.put(ColumnsJugador.PARTIDOS_JUGADOS, 0);
        values.put(ColumnsJugador.PARTIDOS_DESCONVOCADO, 0);
        values.put(ColumnsJugador.MINUTOS_JUGADOS_TOTALES, 0);
        values.put(ColumnsJugador.MINUTOS_BANQUILLO_TOTALES, 0);
        values.put(ColumnsJugador.MINUTOS_DESCONVOCADO_TOTALES, 0);
        values.put(ColumnsJugador.ID_EQUIPO, id_equipo);

        return sqlDB.insert(NOMBRE_TABLA,null,values)>0;
    }

    //Listar jugadores
   public Cursor listaJugadores(int equipo){
        String[] columns = {ColumnsJugador._ID,ColumnsJugador.ID_EQUIPO,ColumnsJugador.NOMBRE, ColumnsJugador.APELLIDOS,
                ColumnsJugador.MOTE, ColumnsJugador.POSICION, ColumnsJugador.GOLES, ColumnsJugador.AMARILLAS, ColumnsJugador.ROJAS, ColumnsJugador.FOTOGRAFIA,ColumnsJugador.PARTIDOS_JUGADOS, ColumnsJugador.PARTIDOS_DESCONVOCADO, ColumnsJugador.MINUTOS_JUGADOS_TOTALES, ColumnsJugador.MINUTOS_BANQUILLO_TOTALES, ColumnsJugador.MINUTOS_DESCONVOCADO_TOTALES};
        String[] args ={String.valueOf(equipo)};
        String where = ColumnsJugador.ID_EQUIPO + "=?";
        return sqlDB.query(NOMBRE_TABLA,columns,where,args,null,null,ColumnsJugador.POSICION + " ASC");
    }

    //Borrar un jugador
    public int deleteJugador(int id){
        return sqlDB.delete(NOMBRE_TABLA, ColumnsJugador._ID + " LIKE ?",new String[]{String.valueOf(id)});
    }

    //Modificar datos del jugador
    public int actualizarJugador(int id, int jugados, int desconvocado, int goles, int amarillas, int rojas, long minutos, long banquillo, long min_desconvocado){
        ContentValues contentValues = new ContentValues();
        contentValues.put(ColumnsJugador.GOLES,goles);
        contentValues.put(ColumnsJugador.AMARILLAS,amarillas);
        contentValues.put(ColumnsJugador.ROJAS,rojas);
        contentValues.put(ColumnsJugador.PARTIDOS_JUGADOS,jugados);
        contentValues.put(ColumnsJugador.MINUTOS_JUGADOS_TOTALES,minutos);
        contentValues.put(ColumnsJugador.PARTIDOS_DESCONVOCADO,desconvocado);
        contentValues.put(ColumnsJugador.MINUTOS_BANQUILLO_TOTALES,banquillo);
        contentValues.put(ColumnsJugador.MINUTOS_DESCONVOCADO_TOTALES,min_desconvocado);
        String[] args = {String.valueOf(id)};
        String where = ColumnsJugador._ID + "=?";
        return sqlDB.update(NOMBRE_TABLA,contentValues,where,args);
    }

}

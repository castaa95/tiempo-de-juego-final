package com.imovil.casta.tiempodejuegofinal.basedatos;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.imovil.casta.tiempodejuegofinal.modelo.Partido;

import java.util.Date;

/**
 * Created by casta on 21/5/17.
 */

public class Partido_Tabla {

    //Nombre de la tabla
    private final static String NOMBRE_TABLA = "partido_tabla";

    //Objeto para acceder a la BBDD
    private SQLiteDatabase sqlDB;

    //Constructor
    public Partido_Tabla(SQLiteDatabase sqlDB){
        this.sqlDB = sqlDB;
    }

    //Clase interna con los campos de la tabla
    public class ColumnsPartido implements BaseColumns {

        public final static String NOMBRE_TABLA = "partido_tabla";
        public final static String ID_EQUIPO = "equipo";
        public final static String PARTIDO_EQUIPO = "numPartido";
        public final static String FECHA_PARTIDO = "fechaPartido";
        public final static String RIVAL = "rival";
        public final static String CAMPO ="campo";
    }

    //Sentencia para crear tabla
    public final static String CREATE_TABLE = "create table if not exists "
            + NOMBRE_TABLA + " ("
            + Partido_Tabla.ColumnsPartido._ID + " integer primary key autoincrement, "
            + Partido_Tabla.ColumnsPartido.ID_EQUIPO + " integer not null, "
            + ColumnsPartido.PARTIDO_EQUIPO + " integer not null, "
            + ColumnsPartido.FECHA_PARTIDO + " date, "
            + ColumnsPartido.RIVAL + " varchar(30), "
            + ColumnsPartido.CAMPO + " varchar(30), "
            + "FOREIGN KEY(" + Partido_Tabla.ColumnsPartido.ID_EQUIPO+ ") REFERENCES "
            + Equipo_Tabla.ColumnsEquipo.NOMBRE_TABLA + "(" + Equipo_Tabla.ColumnsEquipo._ID + ") )";

    public static final String DELETE_TABLE =
            "DROP TABLE IF EXISTS " + NOMBRE_TABLA;

    //Insert
    public boolean crearPartido(int id_equipo, int numPartido, String date, String rival, String campo){
        ContentValues values = new ContentValues();
        values.put(ColumnsPartido.ID_EQUIPO, id_equipo);
        values.put(ColumnsPartido.PARTIDO_EQUIPO, numPartido);
        values.put(ColumnsPartido.FECHA_PARTIDO, date);
        values.put(ColumnsPartido.RIVAL,rival);
        values.put(ColumnsPartido.CAMPO,campo);

        return sqlDB.insert(NOMBRE_TABLA,null,values)>0;
    }

    //Cogemos el id de un determinado partido
    public Cursor getPartido(int equipo, int numero){
        String[] columns = {ColumnsPartido._ID};
        String[] args = {String.valueOf(equipo),String.valueOf(numero)};
        String where = ColumnsPartido.ID_EQUIPO + "=? AND " + ColumnsPartido.PARTIDO_EQUIPO + "=?";
        return sqlDB.query(NOMBRE_TABLA,columns,where,args,null,null,null);

    }

    //Borrar un partido
    public int deletePartido(int id){
        return sqlDB.delete(NOMBRE_TABLA, Partido_Tabla.ColumnsPartido._ID + " LIKE ?",new String[]{String.valueOf(id)});
    }

    //Cogemos los partidos
    public Cursor listaPartidos(int equipo){
        String[] columns = {ColumnsPartido._ID,ColumnsPartido.ID_EQUIPO,ColumnsPartido.PARTIDO_EQUIPO,ColumnsPartido.FECHA_PARTIDO,ColumnsPartido.RIVAL,ColumnsPartido.CAMPO};
        String[] args ={String.valueOf(equipo)};
        String where = Partido_Tabla.ColumnsPartido.ID_EQUIPO + "=?";
        return sqlDB.query(NOMBRE_TABLA,columns,where,args,null,null, null);
    }


}

package com.imovil.casta.tiempodejuegofinal.vista.plantilla;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import com.imovil.casta.tiempodejuegofinal.R;

public class DatosJugadorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos_jugador);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Cogemos datos de la otra actividad
        int idJugador = getIntent().getIntExtra("idJugador",0);
        String nombreJugador = getIntent().getStringExtra("nombreJugador");
        String apellidosJugador = getIntent().getStringExtra("apellidosJugador");
        String moteJugador = getIntent().getStringExtra("moteJugador");
        String posicionJugador = getIntent().getStringExtra("posicionJugador");
        int golesJugador = getIntent().getIntExtra("golesJugador", 0);
        int amarillasJugador = getIntent().getIntExtra("amarillasJugador",0);
        int rojasJugador = getIntent().getIntExtra("rojasJugador",0);
        int partidosJugador = getIntent().getIntExtra("partidosJugador",0);
        int partidosDesconvocado = getIntent().getIntExtra("desconvocadosJugador",0);
        long jugadosJugador = getIntent().getLongExtra("minJugados",0);
        long banquilloJugador = getIntent().getLongExtra("minBanquillo",0);
        long desconvocadoJugador = getIntent().getLongExtra("minDesconvocado",0);

        setTitle(nombreJugador + " " + apellidosJugador);
        //llamamos al fragmento
        DatosJugadorFragment fragment = (DatosJugadorFragment) getSupportFragmentManager().findFragmentById(R.id.datos_jugador_container);
        if (fragment == null) {
            fragment = DatosJugadorFragment.newInstance(idJugador,nombreJugador,apellidosJugador,moteJugador,posicionJugador,golesJugador,amarillasJugador,rojasJugador,partidosJugador,partidosDesconvocado,jugadosJugador,banquilloJugador,desconvocadoJugador);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.datos_jugador_container, fragment)
                    .commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_datos_jugador, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}

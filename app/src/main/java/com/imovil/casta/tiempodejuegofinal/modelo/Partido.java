package com.imovil.casta.tiempodejuegofinal.modelo;

import java.util.Date;

/**
 * Created by casta on 21/5/17.
 */

public class Partido {

    //Atributos
    private int id;
    private int id_equipo;
    private int partidoEquipo;
    private String fecha;
    private String rival;
    private String campo;

    //Constructores
    public Partido(int id, int id_equipo, int partidoEquipo, String fecha, String rival, String campo){
        this.id = id;
        this.id_equipo = id_equipo;
        this.partidoEquipo = partidoEquipo;
        this.fecha = fecha;
        this.rival = rival;
        this.campo = campo;
    }

    //Getters y Setters

    public int getId() {
        return id;
    }

    public int getId_equipo() {
        return id_equipo;
    }

    public String getFecha() {
        return fecha;
    }

    public String getCampo() {
        return campo;
    }

    public void setCampo(String campo) {
        this.campo = campo;
    }

    public String getRival() {
        return rival;

    }

    public void setRival(String rival) {
        this.rival = rival;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getPartidoEquipo() {
        return partidoEquipo;
    }

    public void setPartidoEquipo(int partidoEquipo) {
        this.partidoEquipo = partidoEquipo;
    }
}

package com.imovil.casta.tiempodejuegofinal.adaptadores;

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.imovil.casta.tiempodejuegofinal.R;
import com.imovil.casta.tiempodejuegofinal.modelo.Jugador;
import com.imovil.casta.tiempodejuegofinal.vista.partido.NuevoPartidoActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by casta on 25/5/17.
 */

public class AdaptadorBanco extends RecyclerView.Adapter<AdaptadorBanco.ViewHolder> {

    private final List<Jugador> listaSuplentes;
    public LayoutInflater inflater;
    String titulo = "titulo";
    private Context context;

    static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView mote;
        public LinearLayout layout;
        public TextView dorsal;

        public ViewHolder(View itemView) {
            super(itemView);
            //Inicializar los dos TextView anteriores
            mote = (TextView) itemView.findViewById(R.id.mote);
            dorsal = (TextView) itemView.findViewById(R.id.numero);
            layout = (LinearLayout) itemView.findViewById(R.id.lCampo);

        }
    }

    //Constructor del adatador
    public AdaptadorBanco(Context context, List<Jugador> listaSuplentes) {

        if (context == null || listaSuplentes == null ) {
            throw new IllegalArgumentException();
        }

        this.context = context;
        this.listaSuplentes=listaSuplentes;
        this.inflater = LayoutInflater.from(context);

    }


    @Override
    public int getItemCount() {
        return listaSuplentes.size();
    }

    @Override
    public AdaptadorBanco.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lista_convocados,parent,false);
        return new AdaptadorBanco.ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(AdaptadorBanco.ViewHolder holder, final int position) {

        Jugador jugador = listaSuplentes.get(position);
        holder.mote.setText(listaSuplentes.get(position).getMote());
        holder.dorsal.setText(String.valueOf(listaSuplentes.get(position).getPosicion()));
        final Drawable d = new BitmapDrawable(holder.mote.getResources(), listaSuplentes.get(position).getFotografia());
        holder.layout.setBackground(d);

        //Evento de pulsacion larga
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                try {

                    Jugador jug = listaSuplentes.get(position);
                    //Pasamos todos los datos
                    ClipData.Item itemID = new ClipData.Item(String.valueOf(jug.getId()));
                    ClipData.Item itemEquipo = new ClipData.Item(String.valueOf(jug.getId_equipo()));
                    ClipData.Item itemNombre = new ClipData.Item(jug.getNombre());
                    ClipData.Item itemApellidos = new ClipData.Item(jug.getApellidos());
                    ClipData.Item itemMote = new ClipData.Item(jug.getMote());
                    ClipData.Item itemDorsal = new ClipData.Item(jug.getPosicion());
                    ClipData.Item itemPartidosJugados = new ClipData.Item(String.valueOf(jug.getPartidos_jugados()));
                    ClipData.Item itemPartidosDesconvocado = new ClipData.Item(String.valueOf(jug.getPartidos_desconvocado()));
                    ClipData.Item itemMinutos = new ClipData.Item(String.valueOf(jug.getMinutos_jugados_totales()));
                    ClipData.Item itemBanquillo = new ClipData.Item(String.valueOf(jug.getMinutos_banquillo_totales()));
                    ClipData.Item posicion = new ClipData.Item(String.valueOf(position));
                    //Indicamos que viene del banquillo
                    ClipData.Item origen = new ClipData.Item("banco");
                    ClipData.Item goles = new ClipData.Item(String.valueOf(jug.getGoles()));
                    Uri uriFoto = convertToUri(jug.getFotografia());
                    ClipData.Item foto = new ClipData.Item(uriFoto);
                    ClipData.Item origen2= new ClipData.Item("");
                    ClipData.Item golesPartido = new ClipData.Item(String.valueOf(jug.getTemp_golesPartido()));
                    ClipData.Item minutosDesconvocado = new ClipData.Item(String.valueOf(jug.getMinutos_desconvocado_totales()));
                    ClipData.Item amarillas = new ClipData.Item(String.valueOf(jug.getAmarillas()));
                    ClipData.Item rojas = new ClipData.Item(String.valueOf(jug.getRojas()));
                    ClipData.Item amarillasPartido = new ClipData.Item(String.valueOf(jug.getTemp_amarillasPartido()));
                    ClipData.Item rojasPartido = new ClipData.Item(String.valueOf(jug.getTemp_rojasPartido()));
                    String[] mimeTypes = {ClipDescription.MIMETYPE_TEXT_PLAIN};
                    ClipData dragData = new ClipData("", mimeTypes, itemID);
                    dragData.addItem(itemEquipo);
                    dragData.addItem(itemNombre);
                    dragData.addItem(itemApellidos);
                    dragData.addItem(itemMote);
                    dragData.addItem(itemDorsal);
                    dragData.addItem(itemPartidosJugados);
                    dragData.addItem(itemPartidosDesconvocado);
                    dragData.addItem(itemMinutos);
                    dragData.addItem(itemBanquillo);
                    dragData.addItem(origen);
                    dragData.addItem(posicion);
                    dragData.addItem(goles);
                    dragData.addItem(foto);
                    dragData.addItem(origen2);
                    dragData.addItem(golesPartido);
                    dragData.addItem(minutosDesconvocado);
                    dragData.addItem(amarillas);
                    dragData.addItem(rojas);
                    dragData.addItem(amarillasPartido);
                    dragData.addItem(rojasPartido);
                    View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
                    view.startDrag(dragData, shadowBuilder, view, 0);


                    return true;
                }catch (IOException ex){
                    return false;
                }
            }
        });

    }
    public Uri convertToUri(Bitmap image)throws IOException{
        //File tempDir= Environment.getExternalStorageDirectory();
        //tempDir=new File(tempDir.getAbsolutePath()+"/.temp/");
        //tempDir.mkdir();

        //File tempFile = File.createTempFile(titulo, ".jpg", tempDir);

        File tempDir = context.getCacheDir(); // context being the Activity pointer
        File tempFile = File.createTempFile(titulo, ".jpg", tempDir);

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        byte[] bitmapData = bytes.toByteArray();

        //write the bytes in file
        FileOutputStream fos = new FileOutputStream(tempFile);
        fos.write(bitmapData);
        fos.flush();
        fos.close();
        return Uri.fromFile(tempFile);

    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}


package com.imovil.casta.tiempodejuegofinal.modelo;

/**
 * Created by casta on 21/5/17.
 */

public class Jugador_Partido {

    //Atributos
    private int id_jugador;
    private int id_partido;
    private String nombre;
    private int goles;
    private int amarillas;
    private int rojas;
    private boolean convocado;
    private long minutos_jugados;
    private long minutos_banquillo;
    private long minutos_desconvocado;

    //Constructor
    public Jugador_Partido(int id_jugador, int id_partido, String nombre,int goles, int amarillas, int rojas, boolean convocado, long minutos_jugados, long minutos_banquillo, long minutos_desconvocado){
        this.id_jugador = id_jugador;
        this.id_partido = id_partido;
        this.nombre = nombre;
        this.goles = goles;
        this.amarillas = amarillas;
        this.rojas = rojas;
        this.convocado = convocado;
        this.minutos_jugados = minutos_jugados;
        this.minutos_banquillo = minutos_banquillo;
        this.minutos_desconvocado = minutos_desconvocado;
    }

    public int getId_jugador() {
        return id_jugador;
    }

    public int getId_partido() {
        return id_partido;
    }

    public int getGoles() {
        return goles;
    }

    public void setGoles(int goles) {
        this.goles = goles;
    }

    public boolean isConvocado() {
        return convocado;
    }

    public int getAmarillas() {
        return amarillas;
    }

    public void setAmarillas(int amarillas) {
        this.amarillas = amarillas;
    }

    public String getNombre() {return  nombre; }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getRojas() {
        return rojas;
    }

    public void setRojas(int rojas) {
        this.rojas = rojas;
    }

    public long getMinutos_desconvocado() {
        return minutos_desconvocado;
    }

    public void setMinutos_desconvocado(long minutos_desconvocado) {
        this.minutos_desconvocado = minutos_desconvocado;
    }

    public void setConvocado(boolean convocado) {
        this.convocado = convocado;
    }

    public long getMinutos_jugados() {
        return minutos_jugados;
    }

    public void setMinutos_jugados(long minutos_jugados) {
        this.minutos_jugados = minutos_jugados;
    }

    public long getMinutos_banquillo() {
        return minutos_banquillo;
    }

    public void setMinutos_banquillo(long minutos_banquillo) {
        this.minutos_banquillo = minutos_banquillo;
    }
}

package com.imovil.casta.tiempodejuegofinal.vista.estadisticas;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.LargeValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.imovil.casta.tiempodejuegofinal.R;
import com.imovil.casta.tiempodejuegofinal.basedatos.DBAdapter;
import com.imovil.casta.tiempodejuegofinal.modelo.Jugador;

import java.util.ArrayList;
import java.util.List;


public class GraficasFragment extends Fragment {

    DBAdapter dbAdapter;
    public static final String ARGS_EQUIPO = "id_equipo";
    private BarChart charMinutos;
    List<Jugador> listaJugadores = new ArrayList<Jugador>();

    public GraficasFragment() {
        // Required empty public constructor
    }

    public static GraficasFragment newInstance(int id_equipo) {
        GraficasFragment fragment = new GraficasFragment();
        Bundle args = new Bundle();
        args.putInt(ARGS_EQUIPO,id_equipo);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_grafica, container, false);

        //Layout
        BarChart charGrafica = (BarChart)root.findViewById(R.id.chartMinutos);

        //Cogemos parametro
        Bundle args = getArguments();
        int equipo = args.getInt(ARGS_EQUIPO, 0);

        dbAdapter = new DBAdapter(getContext());
        dbAdapter.open();
        Cursor c = null;
        c  = dbAdapter.getJugadorLista(equipo);

        //Listado de jugadores
        if (c.moveToFirst()) {
            //Recorremos el cursor hasta que no haya más registros
            do {
                byte[] foto = c.getBlob(9);
                Bitmap bm = BitmapFactory.decodeByteArray(foto,0,foto.length);
                Jugador jug = new Jugador(c.getInt(0),c.getInt(1),c.getString(2),c.getString(3),c.getString(4),
                        c.getString(5),c.getInt(6),c.getInt(7),c.getInt(8),bm,c.getInt(10),c.getInt(11),c.getInt(12),c.getInt(13),c.getInt(14));
                listaJugadores.add(jug);
            } while (c.moveToNext());
        }

        final List<String> jugadores = new ArrayList<String>();
        List<BarEntry>entryJugados = new ArrayList<BarEntry>();
        List<BarEntry>entryBanquillo = new ArrayList<BarEntry>();
        List<BarEntry>entryConvcoados = new ArrayList<BarEntry>();

        float mediaJugado = 0;
        float mediaBanquillo = 0;

        for (int i = 0; i<listaJugadores.size(); i++){

            float suma = listaJugadores.get(i).getMinutos_jugados_totales() + listaJugadores.get(i).getMinutos_banquillo_totales() + listaJugadores.get(i).getMinutos_desconvocado_totales();

            mediaJugado = mediaJugado + ((float)listaJugadores.get(i).getMinutos_jugados_totales() / suma *100);
            mediaBanquillo = mediaBanquillo + ((float)listaJugadores.get(i).getMinutos_banquillo_totales() / suma *100);

            jugadores.add(listaJugadores.get(i).getMote());
            entryJugados.add(new BarEntry( i , (float)listaJugadores.get(i).getMinutos_jugados_totales() / suma *100));
            entryBanquillo.add(new BarEntry( i , (float)listaJugadores.get(i).getMinutos_banquillo_totales() / suma *100));
            entryConvcoados.add(new BarEntry( i , (float)listaJugadores.get(i).getMinutos_desconvocado_totales() / suma *100));
        }

        mediaJugado = mediaJugado / listaJugadores.size();
        mediaBanquillo = mediaBanquillo / listaJugadores.size();

        //Preparamos el dataset
        XAxis xAxis = charGrafica.getXAxis();
        xAxis.setGranularity(1f);
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return String.valueOf(jugadores.get((int) value));
            }
        });
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        BarDataSet setJugados = new BarDataSet(entryJugados, getResources().getString(R.string.media_jugado));
        setJugados.setColor(Color.BLUE);
        BarDataSet setBanquillo = new BarDataSet(entryBanquillo, getResources().getString(R.string.media_banquillo));
        setBanquillo.setColor(Color.RED);
        BarDataSet setDesconvocado = new BarDataSet(entryConvcoados,getResources().getString(R.string.minutosDesconvocado));
        setDesconvocado.setColor(Color.YELLOW);

        float groupSpace = 0.07f;
        float barSpace = 0.00f;
        float barWidth = 0.31f;

        ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
        dataSets.add(setJugados);
        dataSets.add(setBanquillo);
        dataSets.add(setDesconvocado);

        BarData data = new BarData(dataSets);

        data.setBarWidth(barWidth);

        YAxis leftAxis = charGrafica.getAxisLeft();
        LimitLine ljugado = new LimitLine(mediaJugado, "Media % Tiempo Jugado");
        ljugado.setLineColor(Color.BLUE);
        ljugado.setLineWidth(2f);
        ljugado.setTextColor(Color.BLACK);
        ljugado.setTextSize(8f);
        leftAxis.addLimitLine(ljugado);

        LimitLine lbanquillo = new LimitLine(mediaBanquillo, "Media % Tiempo Banquillo");
        lbanquillo.setLineColor(Color.RED);
        lbanquillo.setLineWidth(2f);
        lbanquillo.setTextColor(Color.BLACK);
        lbanquillo.setTextSize(8f);
        leftAxis.addLimitLine(lbanquillo);


        charGrafica.setData(data);
        charGrafica.groupBars(-0.5f,groupSpace,barSpace);
        charGrafica.invalidate();
        charGrafica.setDescription(null);

        charGrafica.setDrawBarShadow(false);
        charGrafica.setDrawValueAboveBar(true);
        charGrafica.setMaxVisibleValueCount(50);
        charGrafica.setPinchZoom(false);
        charGrafica.setDrawGridBackground(false);
        return root;
    }
}

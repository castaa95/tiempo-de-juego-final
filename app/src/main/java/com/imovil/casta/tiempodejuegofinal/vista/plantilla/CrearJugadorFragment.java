package com.imovil.casta.tiempodejuegofinal.vista.plantilla;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.imovil.casta.tiempodejuegofinal.adaptadores.ImagePicker;
import com.imovil.casta.tiempodejuegofinal.MainActivity;
import com.imovil.casta.tiempodejuegofinal.R;
import com.imovil.casta.tiempodejuegofinal.basedatos.DBAdapter;

import java.io.ByteArrayOutputStream;

public class CrearJugadorFragment extends Fragment {

    static final int PHOTO_SELECTED = 2;
    private ImageView fotografia;
    private TextInputEditText inputNombre;
    private TextInputEditText inputApellidos;
    private Spinner spinnerPosicion;
    private TextInputEditText inputMote;

    private Bitmap imagen;
    String posicion = "";
    private String keyImagen = "imagen";


    DBAdapter dbAdapter;

    public CrearJugadorFragment() {
        // Required empty public constructor
    }

    public static CrearJugadorFragment newInstance() {
        CrearJugadorFragment fragment = new CrearJugadorFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        if (savedInstanceState != null){
            imagen = savedInstanceState.getParcelable(keyImagen);
        }
        else{
            imagen = BitmapFactory.decodeResource(getResources(),R.mipmap.default_avatar);
        }
        super.onCreate(savedInstanceState);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_crear_jugador, container, false);

        //Elementos del layout
        inputNombre = (TextInputEditText)root.findViewById(R.id.et_nombreJugador);
        inputApellidos = (TextInputEditText)root.findViewById(R.id.et_apellidoJugador);
        spinnerPosicion = (Spinner)root.findViewById(R.id.spPosicion);
        inputMote = (TextInputEditText)root.findViewById(R.id.et_moteJugador);
        FloatingActionButton fab = (FloatingActionButton)root.findViewById(R.id.fabAddJugador);
        fotografia = (ImageView) root.findViewById(R.id.ivFotografia);
        fotografia.setImageBitmap(imagen);

        //Elegir imagen de la galeria
        fotografia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seleccionaFotografia();
            }
        });

        //Spineer
        final String [] posiciones = getResources().getStringArray(R.array.posiciones);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, posiciones);
        spinnerPosicion.setAdapter(adapter);
        spinnerPosicion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Object elegido = parent.getItemAtPosition(position);
                switch (position){
                    case 0: posicion = "DL"; break;
                    case 1: posicion ="MC"; break;
                    case 2: posicion = "DF" ;break;
                    case 3: posicion = "PT";break;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        //Conectar con la BBDD
        dbAdapter = new DBAdapter(getActivity().getApplicationContext());
        dbAdapter.open();

        //Evento del boton que guarda jugador en BBDD y vuelve a la lista de jugadores
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jugadorAñadido();
            }
        });

        return root;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imagenEscogida) {
        switch(requestCode) {
            case PHOTO_SELECTED:
                Bitmap bitmap = ImagePicker.getImageFromResult(this.getContext(), resultCode, imagenEscogida);
                imagen = bitmap;
                if (bitmap == null){
                    bitmap = BitmapFactory.decodeResource(getResources(),R.mipmap.default_avatar);
                    imagen = bitmap;
                }
                fotografia.setImageBitmap(bitmap);
                break;
            default:
                super.onActivityResult(requestCode, resultCode, imagenEscogida);
                break;
        }



    }

    //Metodo para seleccionar la foto de la galeria
    public void seleccionaFotografia(){
        Intent chooseImageIntent = ImagePicker.getPickImageIntent(this.getContext());
        startActivityForResult(chooseImageIntent, PHOTO_SELECTED);

    }

    //Metodo para pasar Bitmap a byte para poder meter la imagen en BBDD
    public static byte[] getBitmapAsByteArray(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, outputStream);
        return outputStream.toByteArray();
    }



    private void jugadorAñadido(){

        //Obtenemos los campos y validamos
        boolean error = false;
        int dorsal = 0;
        String nombreJugador = inputNombre.getText().toString();
        if(TextUtils.isEmpty(nombreJugador)){
            inputNombre.setError(getResources().getText(R.string.campoVacio));
            error = true;
        }

        String apellidos = inputApellidos.getText().toString();
        if (TextUtils.isEmpty(apellidos)){
            inputApellidos.setError(getResources().getText(R.string.campoVacio));
            error = true;
        }

        String mote = inputMote.getText().toString();
        if (TextUtils.isEmpty(mote)){
            inputMote.setError(getResources().getText(R.string.campoVacio));
        }

        if (posicion == ""){
            error = true;
        }

        if (error)
            return;

        //Pasamos imagen a byte
        byte[] imagen = getBitmapAsByteArray(((BitmapDrawable)fotografia.getDrawable()).getBitmap());

        //BBDD
        new CrearJugadorTask(nombreJugador,apellidos,mote,posicion,imagen,MainActivity.id_equipo).execute();

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(keyImagen,imagen);
        super.onSaveInstanceState(outState);
    }

    private class CrearJugadorTask extends AsyncTask<Void, Void, Boolean>{

        String nombreJugador;
        String apellidos;
        String mote;
        String posicion;
        byte[] imagen;
        int id_equipo;

        public CrearJugadorTask(String nombreJugador, String apellidos, String mote, String posicion, byte[] imagen, int id_equipo){

            this.nombreJugador = nombreJugador;
            this.apellidos = apellidos;
            this.mote = mote;
            this.posicion = posicion;
            this.imagen = imagen;
            this.id_equipo = id_equipo;

        }

        @Override
        public Boolean doInBackground(Void... voids) {

            return dbAdapter.insertarJugador(nombreJugador,apellidos,mote,posicion,imagen, id_equipo);

        }

        @Override
        protected void onPostExecute(Boolean bool) {
            getActivity().setResult(Activity.RESULT_OK);
            getActivity().finish();
            Intent intent = new Intent(getActivity(),MainActivity.class);
            intent.putExtra("id",MainActivity.id_equipo);
            intent.putExtra("nombre",MainActivity.nombre);
            intent.putExtra("escudo",MainActivity.escudo);
            intent.putExtra("categoria",MainActivity.categoria);
            startActivity(intent);

        }

    }

}

package com.imovil.casta.tiempodejuegofinal.adaptadores;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.Gravity;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.imovil.casta.tiempodejuegofinal.R;

import java.util.ArrayList;

/**
 * Created by casta on 22/5/17.
 */

public class TablaEstadisticas {

    private TableLayout tabla; // Layout donde se pintará la tabla
    private ArrayList<TableRow> filas; // Array de las filas de la tabla
    private Activity actividad;
    private Resources rs;
    private int FILAS, COLUMNAS;

    //Constructor
    public TablaEstadisticas(Activity actividad, TableLayout tabla)
    {
        this.actividad = actividad;
        this.tabla = tabla;
        rs = this.actividad.getResources();
        FILAS = COLUMNAS = 0;
        filas = new ArrayList<TableRow>();
    }

    //Metodo para agregar cabecera
    public void agregarCabecera(int recursocabecera)
    {
        TableRow.LayoutParams layoutCelda;
        TableRow fila = new TableRow(actividad);
        TableRow.LayoutParams layoutFila = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, 72);
        fila.setLayoutParams(layoutFila);

        String[] arraycabecera = rs.getStringArray(recursocabecera);
        COLUMNAS = arraycabecera.length;

        for(int i = 0; i < arraycabecera.length; i++)
        {
            TextView texto = new TextView(actividad);
            layoutCelda = new TableRow.LayoutParams(obtenerAnchoPixelesTexto(arraycabecera[i]), 72);
            texto.setText(arraycabecera[i]);
            texto.setGravity(Gravity.CENTER_HORIZONTAL);
            //texto.setBackgroundColor(rs.getColor(R.color.letra));
            texto.setLayoutParams(layoutCelda);
            fila.setBackgroundColor(rs.getColor(R.color.cabecera));
            fila.addView(texto);
        }

        tabla.addView(fila);
        //fila.setBackgroundColor(rs.getColor(R.color.cabecera));
        filas.add(fila);
        filas.get(0).setBackgroundColor(rs.getColor(R.color.cabecera));


        FILAS++;
    }

    //Metodo para agregar filas
    public void agregarFilaTabla(ArrayList<String> elementos)
    {
        TableRow.LayoutParams layoutCelda;
        TableRow.LayoutParams layoutFila = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, 72);
        TableRow fila = new TableRow(actividad);
        fila.setLayoutParams(layoutFila);

        for(int i = 0; i< elementos.size(); i++)
        {
            TextView texto = new TextView(actividad);
            texto.setText(String.valueOf(elementos.get(i)));
            texto.setGravity(Gravity.CENTER_HORIZONTAL);
            texto.setTextAppearance(actividad, R.style.TextAppearance_AppCompat_Body1);
            //texto.setTextColor(rs.getColor(R.color.letra));
            layoutCelda = new TableRow.LayoutParams(obtenerAnchoPixelesTexto(texto.getText().toString()), 72);
            texto.setLayoutParams(layoutCelda);

            fila.addView(texto);
        }

        tabla.addView(fila);
        fila.setBackgroundColor(rs.getColor(R.color.cuerpo));
        filas.add(fila);

        FILAS++;
    }

    //Calcular el ancho de las filas
    private int obtenerAnchoPixelesTexto(String texto)
    {
        Paint p = new Paint();
        Rect bounds = new Rect();
        p.setTextSize(35);

        p.getTextBounds(texto, 0, texto.length(), bounds);
        return bounds.width();
    }
}

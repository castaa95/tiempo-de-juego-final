package com.imovil.casta.tiempodejuegofinal.vista.partido;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.imovil.casta.tiempodejuegofinal.R;
import com.imovil.casta.tiempodejuegofinal.adaptadores.AdaptadorPartidos;
import com.imovil.casta.tiempodejuegofinal.basedatos.DBAdapter;
import com.imovil.casta.tiempodejuegofinal.modelo.Partido;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/*
Permite crear nuevos partidos o ver los datos de un partido determinado
 */

public class PartidoFragment extends Fragment {

    DBAdapter dbAdapter;
    public static final String ARGS_EQUIPO = "id_equipo";

    private ListView listaPartidos;
    private TextView listaVacia;
    private FloatingActionButton fabPartidos;
    private static EditText etFecha;
    private static EditText textRival;


    private String campo = "";



    public PartidoFragment() {
        // Required empty public constructor
    }

    public static PartidoFragment newInstance(int id_equipo) {
        PartidoFragment fragment = new PartidoFragment();
        Bundle args = new Bundle();
        args.putInt(ARGS_EQUIPO, id_equipo);
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_partido, container, false);

        //Preparamos el layout

        listaPartidos = (ListView)root.findViewById(R.id.lvPartidos);
        listaVacia = (TextView)root.findViewById(R.id.noPartidos);
        fabPartidos = (FloatingActionButton)root.findViewById(R.id.fabPartido);


        //Rellenamos el listView
        Bundle args = getArguments();
        int equipo = args.getInt(ARGS_EQUIPO, 0);

        //Cargamos los partidos
        List<Partido> listapartidos = new ArrayList<Partido>();
        dbAdapter = new DBAdapter(getContext());
        dbAdapter.open();
        Cursor c = null;
        c  = dbAdapter.getPartidosLista(equipo);
        //Sacamos los campos
        if (c.moveToFirst()) {
            //Recorremos el cursor hasta que no haya más registros
            do {
                String fecha = c.getString(3);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date fecha2 = sdf.parse(fecha);
                }catch (ParseException p){

                }
                Partido part= new Partido(c.getInt(0),c.getInt(1),c.getInt(2),c.getString(3),c.getString(4),c.getString(5));
                listapartidos.add(part);
            } while (c.moveToNext());
        }

        //Mensaje de no hay partidos
        if (listapartidos.size() == 0){
            listaVacia.setVisibility(View.VISIBLE);
        }
        else {
            listaVacia.setVisibility(View.INVISIBLE);
        }

        //Creamos el adapter
        final AdaptadorPartidos adapter = new AdaptadorPartidos(getContext(),listapartidos);
        listaPartidos.setAdapter(adapter);

        //Manejador del floating button
        fabPartidos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                crearPartido();
            }
        });

        //Manejador de seleccion de partido
        listaPartidos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Partido seleccionado = (Partido)adapter.getItem(position);
                Intent intent = new Intent(getActivity(),DatosPartidoActivity.class);
                intent.putExtra("equipo", seleccionado.getId_equipo());
                intent.putExtra("partido",seleccionado.getId());
                startActivity(intent);
            }
        });

        return root;
    }


    private void crearPartido(){

        final Dialog customDialog = new Dialog(getContext());
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.layout_dialogo_rival);

        textRival = (EditText) customDialog.findViewById(R.id.dRival);
        final RadioButton rbCasa = (RadioButton) customDialog.findViewById(R.id.rbLocal);
        final RadioButton rbFuera = (RadioButton) customDialog.findViewById(R.id.rbVisitante);
        final RadioButton rbDirecto = (RadioButton) customDialog.findViewById(R.id.rbDirecto);
        final RadioButton rbFinalizado = (RadioButton) customDialog.findViewById(R.id.rbAnotar);
        final Button bFecha = (Button) customDialog.findViewById(R.id.bFecha);
        etFecha =(EditText) customDialog.findViewById(R.id.etFecha);

        bFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment();
                Bundle args = new Bundle();
                int vista = v.getBottom(); //.getId();
                args.putLong("vista",vista);
                newFragment.setArguments(args);
                newFragment.show(getFragmentManager(), "datePicker");
            }
        });

        //Eventos de los botones
        ((Button) customDialog.findViewById(R.id.dAceptar)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                //customDialog.dismiss();
                Intent intent = null;

                String fecha = etFecha.getText().toString();

                if (TextUtils.isEmpty(fecha)){
                    etFecha.setError(getResources().getText(R.string.campoVacio));
                  //  customDialog.dismiss();
                }

                else {

                    if (rbDirecto.isChecked()) {

                        intent = new Intent(getActivity(), NuevoPartidoActivity.class);


                    } else {

                        intent = new Intent(getActivity(), AnotarPartidoActivity.class);

                    }

                    intent.putExtra("rival", textRival.getText().toString());
                    intent.putExtra("fecha", etFecha.getText().toString());
                    if (rbCasa.isChecked()) {
                        campo = "Local";
                        intent.putExtra("campo", campo);
                    } else {
                        campo = "Visitante";
                        intent.putExtra("campo", campo);
                    }
                    customDialog.dismiss();
                    startActivity(intent);
                }

            }
        });

        ((Button) customDialog.findViewById(R.id.dCancelar)).setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view){
                customDialog.dismiss();
            }
        });

        customDialog.show();
    }

    //Clase para nuestro selector de fecha
    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            long vista = getArguments().getLong("vista");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

            Date fecha = new Date(year-1900,month,day);
            String date = sdf.format(fecha);
            etFecha.setText(date);

        }
    }


}

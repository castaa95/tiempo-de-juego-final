package com.imovil.casta.tiempodejuegofinal.basedatos;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.imovil.casta.tiempodejuegofinal.modelo.Jugador_Partido;

/**
 * Created by casta on 21/5/17.
 */

public class Jugador_Partido_Tabla {

    //Nombre de la tabla
    private final static String NOMBRE_TABLA = "jugador_partido_tabla";

    //Objeto para acceder a la BBDD
    private SQLiteDatabase sqlDB;

    //Constructor
    public Jugador_Partido_Tabla(SQLiteDatabase sqlDB){
        this.sqlDB = sqlDB;
    }

    //Clase interna con los campos de la tabla
    public class ColumnsJugadorPartido implements BaseColumns {

        public final static String ID_JUGADOR = "jugador";
        public final static String ID_PARTIDO = "partido";
        public final static String NOMBRE_JUGADOR = "nomjug";
        public final static String GOLES = "goles";
        public final static String AMARILLAS = "amarillas";
        public final static String ROJAS ="rojas";
        public final static String CONVOCADO = "convocado";
        public final static String MINUTOS_JUGADOS = "min_jugados";
        public final static String MINUTOS_BANQUILLO = "min_banquillo";
        public final static String MINUTOS_DESCONVOCADO = "min_desconvocado";

    }

    //Sentencia para crear tabla
    public final static String CREATE_TABLE = "create table if not exists "
            + NOMBRE_TABLA + " ("
            + ColumnsJugadorPartido.ID_JUGADOR + " integer , "
            + ColumnsJugadorPartido.ID_PARTIDO + " integer, "
            + ColumnsJugadorPartido.NOMBRE_JUGADOR + " varchar (30) not null, "
            + ColumnsJugadorPartido.GOLES + " integer not null, "
            + ColumnsJugadorPartido.AMARILLAS + " integer not null, "
            + ColumnsJugadorPartido.ROJAS + " integer not null, "
            + ColumnsJugadorPartido.CONVOCADO + " boolean not null, "
            + ColumnsJugadorPartido.MINUTOS_JUGADOS + " long not null, "
            + ColumnsJugadorPartido.MINUTOS_BANQUILLO + " long not null, "
            + ColumnsJugadorPartido.MINUTOS_DESCONVOCADO + " long not null, "
            + "PRIMARY KEY(" + ColumnsJugadorPartido.ID_JUGADOR + ", " + ColumnsJugadorPartido.ID_PARTIDO + "), "
            + "FOREIGN KEY(" + ColumnsJugadorPartido.ID_PARTIDO+ ") REFERENCES "
            + Partido_Tabla.ColumnsPartido.NOMBRE_TABLA + "(" + Partido_Tabla.ColumnsPartido._ID + ") "
            + "FOREIGN KEY(" + ColumnsJugadorPartido.ID_JUGADOR+ ") REFERENCES "
            + Jugador_Tabla.ColumnsJugador.NOMBRE_TABLA + "(" + Jugador_Tabla.ColumnsJugador._ID + ") )";

    //Borrar tabla
    public static final String DELETE_TABLE =
            "DROP TABLE IF EXISTS " + NOMBRE_TABLA;


    //Añadir jugador
    public boolean crearJugadorPartido(int id_jugador, int id_partido, String nombre, int goles, int amarillas, int rojas, boolean convocado, long minutos_jugados, long minutos_banquillo, long minutos_desconvocado){
        ContentValues values = new ContentValues();
        values.put(ColumnsJugadorPartido.ID_JUGADOR, id_jugador);
        values.put(ColumnsJugadorPartido.ID_PARTIDO, id_partido);
        values.put(ColumnsJugadorPartido.NOMBRE_JUGADOR,nombre);
        values.put(ColumnsJugadorPartido.GOLES,goles);
        values.put(ColumnsJugadorPartido.AMARILLAS,amarillas);
        values.put(ColumnsJugadorPartido.ROJAS,rojas);
        values.put(ColumnsJugadorPartido.CONVOCADO,convocado);
        values.put(ColumnsJugadorPartido.MINUTOS_JUGADOS,minutos_jugados);
        values.put(ColumnsJugadorPartido.MINUTOS_BANQUILLO,minutos_banquillo);
        values.put(ColumnsJugadorPartido.MINUTOS_DESCONVOCADO, minutos_desconvocado);

        return sqlDB.insert(NOMBRE_TABLA,null,values)>0;
    }

    //Elimianr detalles
    public int deleteDetallesPartido(int id){
        return sqlDB.delete(NOMBRE_TABLA, ColumnsJugadorPartido.ID_PARTIDO + " LIKE ?",new String[]{String.valueOf(id)});
    }

    //Datos del partido
    public Cursor getDetallesP(int id_partido){
        String[] columns = {ColumnsJugadorPartido.ID_JUGADOR, ColumnsJugadorPartido.ID_PARTIDO, ColumnsJugadorPartido.NOMBRE_JUGADOR, ColumnsJugadorPartido.GOLES, ColumnsJugadorPartido.AMARILLAS, ColumnsJugadorPartido.ROJAS, ColumnsJugadorPartido.CONVOCADO, ColumnsJugadorPartido.MINUTOS_JUGADOS, ColumnsJugadorPartido.MINUTOS_BANQUILLO, ColumnsJugadorPartido.MINUTOS_DESCONVOCADO};
        String[] args ={String.valueOf(id_partido)};
        String where = ColumnsJugadorPartido.ID_PARTIDO + "=?";
        return sqlDB.query(NOMBRE_TABLA,columns,where,args,null,null,null);
    }

    //Datos del partido
    public Cursor getDetalles(int id_jugador, int id_partido){
        String[] columns = {ColumnsJugadorPartido.ID_JUGADOR, ColumnsJugadorPartido.ID_PARTIDO, ColumnsJugadorPartido.NOMBRE_JUGADOR, ColumnsJugadorPartido.GOLES, ColumnsJugadorPartido.AMARILLAS, ColumnsJugadorPartido.ROJAS, ColumnsJugadorPartido.CONVOCADO, ColumnsJugadorPartido.MINUTOS_JUGADOS, ColumnsJugadorPartido.MINUTOS_BANQUILLO, ColumnsJugadorPartido.MINUTOS_DESCONVOCADO};
        String[] args ={String.valueOf(id_jugador),String.valueOf(id_partido)};
        String where = ColumnsJugadorPartido.ID_JUGADOR + "=? AND " + ColumnsJugadorPartido.ID_PARTIDO + "=?";
        return sqlDB.query(NOMBRE_TABLA,columns,where,args,null,null,null);
    }

}

package com.imovil.casta.tiempodejuegofinal.basedatos;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.json.JSONArray;

/**
 * Created by casta on 28/3/17.
 */

public class DBAdapter {

    //Clase encargada de gestionar la BBDD

    final static int VERSION = 1;
    public final static String DB_NOMBRE = "BBDDTiempoDeJuego25.db";

    private DBOpenHelper dbHelper;
    private SQLiteDatabase sqlDB;

    Equipo_Tabla tabla_equipo;
    Jugador_Tabla tabla_jugador;
    Partido_Tabla tabla_partido;
    Jugador_Partido_Tabla tabla_jugador_partido;

    public DBAdapter(Context context) {
        dbHelper = new DBOpenHelper(context);
    }

    //Conexion con la BBDD
    public void open() {
        sqlDB = dbHelper.getWritableDatabase();
        tabla_equipo = new Equipo_Tabla(sqlDB);
        tabla_jugador = new Jugador_Tabla(sqlDB);
        tabla_partido = new Partido_Tabla(sqlDB);
        tabla_jugador_partido = new Jugador_Partido_Tabla(sqlDB);
    }

    public void close() {
        sqlDB.close();
    }

    //Insertar equipo
    public boolean insertarEquipo(String nombre, byte[] escudo, String categoria){
        return tabla_equipo.crearEquipo(nombre,escudo,categoria);
    }

    //Coger escudo y nombre de equipos
    public Cursor getNombreEscudo(){
        return tabla_equipo.listaEquipo();
    }

    //Insertar jugador
    public boolean insertarJugador(String nombreJugador, String apellidos, String mote, String posicion, byte[] fotografia, int id_equipo){
        return  tabla_jugador.crearJugador(nombreJugador,apellidos,mote,posicion,fotografia,id_equipo);
    }

    //Insertar partido
    public boolean insertarPartido(int id_equipo, int numPartido,String date,String rival,String campo){
        return tabla_partido.crearPartido(id_equipo,numPartido,date,rival,campo);
    }

    //Insertar jugaodr_partido
    public boolean insertarJugadorPartido(int id_jugador,int id_partido, String nombre, int goles, int amarillas, int rojas, boolean convocado, long minutos_jugados, long minutos_banquillo, long minutos_desconvocado){
        return tabla_jugador_partido.crearJugadorPartido(id_jugador,id_partido,nombre,goles,amarillas,rojas,convocado,minutos_jugados,minutos_banquillo,minutos_desconvocado);
    }
    //Coger lista de jugadores
    public Cursor getJugadorLista(int equipo){
        return tabla_jugador.listaJugadores(equipo);
    }

    //Coger lista de partidos
    public Cursor getPartidosLista(int equipo){ return  tabla_partido.listaPartidos(equipo);}

    //Coger un partido determinado
    public Cursor getPartido(int equipo, int numero){ return  tabla_partido.getPartido(equipo,numero);}

    //Borrar jugador
    public int deleteJugador(int id){ return tabla_jugador.deleteJugador(id);}

    //Borrar partido
    public int deletePartido(int id){ return tabla_partido.deletePartido(id);}

    //Actualizar jugados
    public int actualizarJugador(int id, int jugados, int desconvocado, int goles,int amarillas,int rojas, long minutos, long banquillo, long min_desconvocado){
        return tabla_jugador.actualizarJugador(id,jugados,desconvocado,goles,amarillas,rojas,minutos,banquillo,min_desconvocado);
    }

    //Delete team
    public int deleteTeam(int id){
        return tabla_equipo.deleteTeam(id);
    }

    //Delete details
    public int deleteDetalles(int id){ return tabla_jugador_partido.deleteDetallesPartido(id);}

    //Detalles del partido
    public Cursor getDetallespartido(int jugador, int partido){
        return tabla_jugador_partido.getDetalles(jugador,partido);
    }

    public Cursor getDatosEquipo(int id){
        return tabla_equipo.datosEquipo(id);
    }

    public Cursor getDetallesP (int partido){
        return tabla_jugador_partido.getDetallesP(partido);
    }
    private class DBOpenHelper extends SQLiteOpenHelper {

        public DBOpenHelper(Context context) {
            super(context, DB_NOMBRE, null, VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(Equipo_Tabla.CREATE_TABLE);
            db.execSQL(Jugador_Tabla.CREATE_TABLE);
            db.execSQL(Partido_Tabla.CREATE_TABLE);
            db.execSQL(Jugador_Partido_Tabla.CREATE_TABLE);

        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

            db.execSQL(Equipo_Tabla.DELETE_TABLE);
            db.execSQL(Jugador_Tabla.DELETE_TABLE);
            db.execSQL(Partido_Tabla.DELETE_TABLE);
            db.execSQL(Jugador_Partido_Tabla.DELETE_TABLE);
            onCreate(db);
        }

        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onUpgrade(db, oldVersion, newVersion);
        }
    }
}


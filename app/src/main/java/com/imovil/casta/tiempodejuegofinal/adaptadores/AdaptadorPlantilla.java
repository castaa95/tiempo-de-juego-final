package com.imovil.casta.tiempodejuegofinal.adaptadores;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.imovil.casta.tiempodejuegofinal.R;
import com.imovil.casta.tiempodejuegofinal.modelo.Jugador;

import java.util.List;

/**
 * Created by casta on 20/4/17.
 */

public class AdaptadorPlantilla extends BaseAdapter {

    static class ViewHolder {
        public TextView nombreJugador;
        public TextView apellidosJugador;
        public TextView moteJugador;
        public ImageView imagenJugador;
        public TextView dorsalJugador;
    }

    private final List<Jugador> listaJugadores;
    public LayoutInflater inflater;

    //Constructor del adatador
    public AdaptadorPlantilla(Context context, List<Jugador> jugadores) {

        if (context == null) {
            throw new IllegalArgumentException();
        }

        this.listaJugadores=jugadores;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listaJugadores.size();
    }

    @Override
    public Object getItem(int position) {
        return listaJugadores.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        AdaptadorPlantilla.ViewHolder viewHolder;
        if (rowView == null) {

            rowView = inflater.inflate(R.layout.lista_jugadores,parent,false);
            viewHolder = new AdaptadorPlantilla.ViewHolder();
            viewHolder.nombreJugador = (TextView) rowView.findViewById(R.id.tvJugador);
            viewHolder.apellidosJugador = (TextView) rowView.findViewById(R.id.tvApellidos);
            viewHolder.imagenJugador = (ImageView) rowView.findViewById(R.id.ivFotografia);
            viewHolder.moteJugador = (TextView) rowView.findViewById(R.id.tvMote);
            viewHolder.dorsalJugador = (TextView) rowView.findViewById(R.id.tvDorsal);
            rowView.setTag(viewHolder);

        } else {
            viewHolder = (AdaptadorPlantilla.ViewHolder) rowView.getTag();
        }

        Jugador jugador = (Jugador) getItem(position);
        viewHolder.nombreJugador.setText(jugador.getNombre());
        viewHolder.apellidosJugador.setText(jugador.getApellidos());
        viewHolder.moteJugador.setText(jugador.getMote());
        viewHolder.imagenJugador.setImageBitmap(Bitmap.createScaledBitmap(jugador.getFotografia(),80,80,false));
        viewHolder.dorsalJugador.setText(jugador.getPosicion());

        return rowView;

    }

}

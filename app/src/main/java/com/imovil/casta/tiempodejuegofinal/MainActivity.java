package com.imovil.casta.tiempodejuegofinal;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Path;
import android.graphics.drawable.shapes.PathShape;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.imovil.casta.tiempodejuegofinal.basedatos.DBAdapter;
import com.imovil.casta.tiempodejuegofinal.vista.equipo.EquiposFragment;
import com.imovil.casta.tiempodejuegofinal.vista.estadisticas.EstadisticasFragment;
import com.imovil.casta.tiempodejuegofinal.vista.estadisticas.GraficasFragment;
import com.imovil.casta.tiempodejuegofinal.vista.partido.PartidoFragment;
import com.imovil.casta.tiempodejuegofinal.vista.plantilla.PlantillaFragment;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private TextView nombreEquipo;
    private ImageView escudoEquipo;
    private TextView categoriaEquipo;

    public static int id_equipo;
    public static String categoria;
    public static String nombre;
    public static Bitmap escudo;

    private DBAdapter dbAdapter;

    private String path ="";
    public static String vista;
    private String tagVista = "vista";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        Intent intent = getIntent();
        id_equipo = intent.getIntExtra("id",0);
        categoria = intent.getStringExtra("categoria");
        nombre = intent.getStringExtra("nombre");
        escudo = intent.getParcelableExtra("escudo");

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        Menu menuNav=navigationView.getMenu();
        MenuItem nav_plantilla = menuNav.findItem(R.id.nav_plantilla);
        MenuItem nav_partido = menuNav.findItem(R.id.nav_partido);
        MenuItem nav_estadisticas = menuNav.findItem(R.id.nav_estadisticas);
        MenuItem nav_out = menuNav.findItem(R.id.nav_log_out);
        MenuItem nav_graficas = menuNav.findItem(R.id.nav_grafica);
        MenuItem nav_eliminar = menuNav.findItem(R.id.nav_delete);


        View headerView = navigationView.getHeaderView(0);
        //Ponemos el escudo, nombre y categoria en el nav_header
        nombreEquipo = (TextView)headerView.findViewById(R.id.nombreEquipo);
        categoriaEquipo = (TextView)headerView.findViewById(R.id.categoriaEquipo);
        escudoEquipo = (ImageView)headerView.findViewById(R.id.circle_image);

        if (id_equipo == 0 || categoria.equals(null) || nombre.equals(null) || escudo.equals(null)){

            nav_plantilla.setEnabled(false);
            nav_estadisticas.setEnabled(false);
            nav_partido.setEnabled(false);
            nav_out.setEnabled(false);
            nav_graficas.setEnabled(false);
            nav_eliminar.setEnabled(false);


        }

        if (savedInstanceState == null) {


            if (id_equipo == 0 || categoria.equals(null) || nombre.equals(null) || escudo.equals(null)) {

                Bundle args = new Bundle();
                vista = "equipo";
                EquiposFragment equiposFragment = EquiposFragment.newInstance();
                FragmentManager fm = getSupportFragmentManager();
                fm.beginTransaction().replace(R.id.main_content, equiposFragment).commit();
                setTitle("Mis equipos");
            }

            else{
                Bundle args = new Bundle();
                vista = "plantilla";
                args.putInt(PlantillaFragment.ARGS_EQUIPO,id_equipo);
                PlantillaFragment fragment = PlantillaFragment.newInstance(id_equipo);
                fragment.setArguments(args);
                FragmentManager fm = getSupportFragmentManager();
                fm.beginTransaction().replace(R.id.main_content,fragment).commit();

                setTitle(R.string.plantilla_item);
            }
        }

        else{

            vista = savedInstanceState.getString(tagVista);

            if (vista.equals("plantilla")){

                Bundle args = new Bundle();
                args.putInt(PlantillaFragment.ARGS_EQUIPO,id_equipo);
                PlantillaFragment fragment = PlantillaFragment.newInstance(id_equipo);
                fragment.setArguments(args);
                FragmentManager fm = getSupportFragmentManager();
                fm.beginTransaction().replace(R.id.main_content,fragment).commit();
                setTitle(R.string.plantilla_item);

            }

            if (vista.equals("partido")){

                Bundle args = new Bundle();
                args.putInt(PlantillaFragment.ARGS_EQUIPO,id_equipo);
                PartidoFragment fragment = PartidoFragment.newInstance(id_equipo);
                fragment.setArguments(args);
                FragmentManager fm = getSupportFragmentManager();
                fm.beginTransaction().replace(R.id.main_content,fragment).commit();
                setTitle(R.string.partidos_item);

            }

            if (vista.equals("estadisticas")){

                Bundle args = new Bundle();
                args.putInt(PlantillaFragment.ARGS_EQUIPO,id_equipo);
                EstadisticasFragment fragment = EstadisticasFragment.newInstance(id_equipo);
                fragment.setArguments(args);
                FragmentManager fm = getSupportFragmentManager();
                fm.beginTransaction().replace(R.id.main_content,fragment).commit();
                setTitle(R.string.estadisticas_item);

            }

            if( vista.equals("equipo")){
                Bundle args = new Bundle();
                EquiposFragment equiposFragment = EquiposFragment.newInstance();
                FragmentManager fm = getSupportFragmentManager();
                fm.beginTransaction().replace(R.id.main_content, equiposFragment).commit();
                setTitle(getResources().getString(R.string.tituloEquipos));
            }

            if( vista.equals("grafica")){
                Bundle args = new Bundle();
                args.putInt(PlantillaFragment.ARGS_EQUIPO,id_equipo);
                GraficasFragment graficasFragment = GraficasFragment.newInstance(id_equipo);
                graficasFragment.setArguments(args);
                FragmentManager fm = getSupportFragmentManager();
                fm.beginTransaction().replace(R.id.main_content,graficasFragment).commit();
                setTitle(getResources().getString(R.string.grafTiempo));
            }
        }



        String nombre = intent.getStringExtra("nombre");
        String categoria = intent.getStringExtra("categoria");
        nombreEquipo.setText(nombre);
        categoriaEquipo.setText(categoria);
        escudoEquipo.setImageBitmap((Bitmap)intent.getParcelableExtra("escudo"));


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

   @Override
    public void onResume() {
        super.onResume();

        if (vista.equals("plantilla")){

            Bundle args = new Bundle();
            args.putInt(PlantillaFragment.ARGS_EQUIPO,id_equipo);
            PlantillaFragment fragment = PlantillaFragment.newInstance(id_equipo);
            fragment.setArguments(args);
            FragmentManager fm = getSupportFragmentManager();
            fm.beginTransaction().replace(R.id.main_content,fragment).commit();
            setTitle(R.string.plantilla_item);

        }

        if (vista.equals("partido")){

            Bundle args = new Bundle();
            args.putInt(PlantillaFragment.ARGS_EQUIPO,id_equipo);
            PartidoFragment fragment = PartidoFragment.newInstance(id_equipo);
            fragment.setArguments(args);
            FragmentManager fm = getSupportFragmentManager();
            fm.beginTransaction().replace(R.id.main_content,fragment).commit();
            setTitle(R.string.partidos_item);

        }

        if (vista.equals("estadisticas")){

            Bundle args = new Bundle();
            args.putInt(PlantillaFragment.ARGS_EQUIPO,id_equipo);
            EstadisticasFragment fragment = EstadisticasFragment.newInstance(id_equipo);
            fragment.setArguments(args);
            FragmentManager fm = getSupportFragmentManager();
            fm.beginTransaction().replace(R.id.main_content,fragment).commit();
            setTitle(R.string.estadisticas_item);

        }

        if (vista.equals("equipo")){
            Bundle args = new Bundle();
            vista = "equipo";
            EquiposFragment equiposFragment = EquiposFragment.newInstance();
            FragmentManager fm = getSupportFragmentManager();
            fm.beginTransaction().replace(R.id.main_content, equiposFragment).commit();
            setTitle(R.string.tituloEquipos);
        }

        if( vista.equals("grafica")){
            Bundle args = new Bundle();
            args.putInt(PlantillaFragment.ARGS_EQUIPO,id_equipo);
            GraficasFragment graficasFragment = GraficasFragment.newInstance(id_equipo);
            graficasFragment.setArguments(args);
            FragmentManager fm = getSupportFragmentManager();
            fm.beginTransaction().replace(R.id.main_content,graficasFragment).commit();
            setTitle(R.string.grafTiempo);
        }

    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        item.setChecked(true);
        int id = item.getItemId();

        if (id == R.id.nav_plantilla) {

            //Pasamos el id del equipo
            Bundle args = new Bundle();
            args.putInt(PlantillaFragment.ARGS_EQUIPO,id_equipo);
            PlantillaFragment fragment = PlantillaFragment.newInstance(id_equipo);
            fragment.setArguments(args);
            FragmentManager fm = getSupportFragmentManager();
            fm.beginTransaction().replace(R.id.main_content,fragment).commit();
            vista = "plantilla";
            setTitle(R.string.plantilla_item);


        } else if (id == R.id.nav_estadisticas) {

            Bundle args = new Bundle();
            args.putInt(PlantillaFragment.ARGS_EQUIPO,id_equipo);
            EstadisticasFragment fragment = EstadisticasFragment.newInstance(id_equipo);
            fragment.setArguments(args);
            FragmentManager fm = getSupportFragmentManager();
            fm.beginTransaction().replace(R.id.main_content,fragment).commit();
            vista = "estadisticas";
            setTitle(R.string.estadisticas_item);

        } else if (id == R.id.nav_partido) {
            Bundle args = new Bundle();
            args.putInt(PlantillaFragment.ARGS_EQUIPO,id_equipo);
            PartidoFragment fragment = PartidoFragment.newInstance(id_equipo);
            fragment.setArguments(args);
            FragmentManager fm = getSupportFragmentManager();
            fm.beginTransaction().replace(R.id.main_content,fragment).commit();
            vista = "partido";
            setTitle(R.string.partidos_item);

        } else if (id == R.id.nav_log_out){
            Bundle args = new Bundle();
            vista = "equipo";
            EquiposFragment equiposFragment = EquiposFragment.newInstance();
            FragmentManager fm = getSupportFragmentManager();
            fm.beginTransaction().replace(R.id.main_content, equiposFragment).commit();
            setTitle("Mis equipos");

        }

        else if(id == R.id.nav_grafica){
            Bundle args = new Bundle();
            args.putInt(PlantillaFragment.ARGS_EQUIPO,id_equipo);
            GraficasFragment graficasFragment = GraficasFragment.newInstance(id_equipo);
            graficasFragment.setArguments(args);
            FragmentManager fm = getSupportFragmentManager();
            fm.beginTransaction().replace(R.id.main_content,graficasFragment).commit();
            vista = "grafica";
            setTitle("Gráficas de tiempo jugado");
        }

        else if (id == R.id.nav_delete){
            //Conecta con BBDD
            dbAdapter = new DBAdapter(this.getApplicationContext());
            dbAdapter.open();
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(getResources().getString(R.string.tituloEliminar))
                    .setMessage(getResources().getString(R.string.mensajeEliminar))
                    .setPositiveButton(getResources().getString(R.string.si), new DialogInterface.OnClickListener()
                    {

                        public void onClick(DialogInterface dialog, int which) {
                            dbAdapter.deleteTeam(id_equipo);

                            Intent intent = new Intent(MainActivity.this, MainActivity.class);
                            intent.putExtra("id",0);
                            intent.putExtra("nombre"," ");
                            intent.putExtra("escudo"," ");
                            intent.putExtra("categoria"," ");
                            finish();
                            overridePendingTransition(0,0);
                            startActivity(intent);


                        }

                    })
                    .setNegativeButton("No", null)
                    .show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(tagVista,vista.toString());
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // Restore UI state from the savedInstanceState.
        // This bundle has also been passed to onCreate.

        String tagVista = savedInstanceState.getString("vista");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_export) {

            File directory = null;
            //Si no hay tarjeta SD crearemos un directorio
            if(Environment.getExternalStorageState() == null){

                directory = new File(Environment.getDataDirectory() + "/TiempoDeJuegoBackup.db/");
                path = directory.toString();
            }

            //En el caso de que tenga SD
            else if (Environment.getExternalStorageState() != null) {

                directory = new File(Environment.getExternalStorageDirectory() + "/TiempoDeJuegoBackup.db/");
                path = directory.toString();
            }


            String mensaje = getResources().getString(R.string.mensajeExportar) + path;
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(getResources().getString(R.string.tituloExportar))
                    .setMessage(mensaje)
                    .setPositiveButton(getResources().getString(R.string.aceptar), new DialogInterface.OnClickListener()
                    {

                        public void onClick(DialogInterface dialog, int which) {
                            exportDB();
                        }

                    })
                    .setNegativeButton(getResources().getString(R.string.cancelar), null)
                    .show();


        }

        else if(id == R.id.action_import) {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(getResources().getString(R.string.tituloImportar))
                    .setMessage(getResources().getString(R.string.mensajeImportar))
                    .setPositiveButton(getResources().getString(R.string.aceptar), new DialogInterface.OnClickListener()
                    {

                        public void onClick(DialogInterface dialog, int which) {
                            importDB();
                        }

                    })
                    .setNegativeButton(getResources().getString(R.string.cancelar), null)
                    .show();

        }

        return super.onOptionsItemSelected(item);
    }

    private void exportDB() {

        File directory = null;

        //Si no hay tarjeta SD crearemos un directorio
        if(Environment.getExternalStorageState() == null){

            directory = new File(Environment.getDataDirectory() + "/TiempoDeJuegoBackup.db/");
            path = directory.toString();

            //Comprobamos si existe el directorio, si no existe lo crearemos
            if(!directory.exists()){
              //  directory.mkdir();
            }

        }

        //En el caso de que tenga SD
        else if (Environment.getExternalStorageState() != null){

            directory = new File(Environment.getExternalStorageDirectory() + "/TiempoDeJuegoBackup.db/");
            path = directory.toString();

            //Si no existe el directorio lo creamos
            if(!directory.exists()){
                //directory.mkdir();
            }
        }


        try {

            //Almacenamos la base de datos en el directorio

            File currentDB = this.getDatabasePath(DBAdapter.DB_NOMBRE);

            isStoragePermissionGranted();

            FileChannel src = new FileInputStream(currentDB).getChannel();
            FileChannel dst = new FileOutputStream(directory).getChannel();
            dst.transferFrom(src, 0, src.size());
            src.close();
            dst.close();
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.guardadoCorrecto), Toast.LENGTH_SHORT).show();

        } catch (Exception e) {

            Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT)
                    .show();

        }
    }


    private void importDB() {
        try {

            File directory = null;

            if(Environment.getExternalStorageState() == null){

                directory = new File(Environment.getDataDirectory() + "/TiempoDeJuegoBackup.db/");
                path = directory.toString();
            }

            //En el caso de que tenga SD
            else if (Environment.getExternalStorageState() != null){

              directory = new File(Environment.getExternalStorageDirectory() + "/TiempoDeJuegoBackup.db/");
                path = directory.toString();

            }

            File currentDB = this.getDatabasePath(DBAdapter.DB_NOMBRE);

            isStoragePermissionGranted();

                FileChannel src = new FileInputStream(directory).getChannel();
                FileChannel dst = new FileOutputStream(currentDB).getChannel();
                dst.transferFrom(src, 0, src.size());
                src.close();
                dst.close();
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.cargadoCorreto), Toast.LENGTH_SHORT).show();


            //Recargamos
            Intent intent = new Intent(this,MainActivity.class);
            finish();
            overridePendingTransition(0,0);
            startActivity(intent);




        } catch (Exception e) {

            Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT)
                    .show();

        }
    }


    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("PERMISO","Permission is granted");
                return true;
            } else {

                Log.v("PERMISO","Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v("PERMISO","Permission is granted");
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
            Log.v("PERMISO","Permission: "+permissions[0]+ "was "+grantResults[0]);
            //resume tasks needing this permission
        }
    }

}
